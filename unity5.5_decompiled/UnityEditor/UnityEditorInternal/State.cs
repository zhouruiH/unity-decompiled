﻿namespace UnityEditorInternal
{
    using System;
    using UnityEngine;

    [Obsolete("State is obsolete. Use UnityEditor.Animations.AnimatorState instead (UnityUpgradable) -> UnityEditor.Animations.AnimatorState", true)]
    public class State : UnityEngine.Object
    {
        public UnityEditorInternal.BlendTree CreateBlendTree()
        {
            return null;
        }

        public UnityEditorInternal.BlendTree CreateBlendTree(UnityEditorInternal.AnimatorControllerLayer layer)
        {
            return null;
        }

        public Motion GetMotion()
        {
            return null;
        }

        public Motion GetMotion(UnityEditorInternal.AnimatorControllerLayer layer)
        {
            return null;
        }

        public bool iKOnFeet
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        public bool mirror
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        public float speed
        {
            get
            {
                return -1f;
            }
            set
            {
            }
        }

        public string tag
        {
            get
            {
                return string.Empty;
            }
            set
            {
            }
        }

        public string uniqueName
        {
            get
            {
                return string.Empty;
            }
        }

        public int uniqueNameHash
        {
            get
            {
                return -1;
            }
        }
    }
}

