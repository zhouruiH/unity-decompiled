﻿namespace UnityEditorInternal
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class AudioProfilerBackend
    {
        public UnityEditorInternal.AudioProfilerTreeViewState m_TreeViewState;
        public DataUpdateDelegate OnUpdate;

        public AudioProfilerBackend(UnityEditorInternal.AudioProfilerTreeViewState state)
        {
            this.m_TreeViewState = state;
            this.items = new List<UnityEditorInternal.AudioProfilerInfoWrapper>();
        }

        public void SetData(List<UnityEditorInternal.AudioProfilerInfoWrapper> data)
        {
            this.items = data;
            this.UpdateSorting();
        }

        public void UpdateSorting()
        {
            this.items.Sort(new UnityEditorInternal.AudioProfilerInfoHelper.AudioProfilerInfoComparer((UnityEditorInternal.AudioProfilerInfoHelper.ColumnIndices) this.m_TreeViewState.selectedColumn, (UnityEditorInternal.AudioProfilerInfoHelper.ColumnIndices) this.m_TreeViewState.prevSelectedColumn, this.m_TreeViewState.sortByDescendingOrder));
            if (this.OnUpdate != null)
            {
                this.OnUpdate();
            }
        }

        public List<UnityEditorInternal.AudioProfilerInfoWrapper> items { get; private set; }

        public delegate void DataUpdateDelegate();
    }
}

