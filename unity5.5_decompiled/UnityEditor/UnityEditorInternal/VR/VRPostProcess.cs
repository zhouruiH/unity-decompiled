﻿namespace UnityEditorInternal.VR
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using UnityEditor;
    using UnityEditor.Callbacks;
    using UnityEditorInternal;

    internal static class VRPostProcess
    {
        [UnityEditor.Callbacks.RegisterPlugins]
        private static IEnumerable<UnityEditorInternal.PluginDesc> RegisterPlugins(UnityEditor.BuildTarget target)
        {
            if ((target == UnityEditor.BuildTarget.Android) && UnityEditor.PlayerSettings.virtualRealitySupported)
            {
                UnityEditorInternal.PluginDesc desc = new UnityEditorInternal.PluginDesc();
                string str = UnityEditor.EditorApplication.applicationContentsPath + "/VR/oculus/" + UnityEditor.BuildPipeline.GetBuildTargetName(target);
                desc.pluginPath = Path.Combine(str, "ovrplugin.aar");
                return new UnityEditorInternal.PluginDesc[] { desc };
            }
            return new UnityEditorInternal.PluginDesc[0];
        }
    }
}

