﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class DDSImporter : AssetImporter
    {
        public bool isReadable { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

