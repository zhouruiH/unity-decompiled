﻿namespace UnityEditor
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    internal interface ITreeViewDragging
    {
        bool CanStartDrag(UnityEditor.TreeViewItem targetItem, List<int> draggedItemIDs, Vector2 mouseDownPosition);
        void DragCleanup(bool revertExpanded);
        bool DragElement(UnityEditor.TreeViewItem targetItem, Rect targetItemRect, bool firstItem);
        int GetDropTargetControlID();
        int GetRowMarkerControlID();
        void OnInitialize();
        void StartDrag(UnityEditor.TreeViewItem draggedItem, List<int> draggedItemIDs);

        bool drawRowMarkerAbove { get; set; }
    }
}

