﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Internal;
    using UnityEngine.Scripting;

    public sealed class AnimationUtility
    {
        private static int kBrokenMask = 1;
        private static int kLeftTangentMask = 30;
        private static int kRightTangentMask = 480;
        public static OnCurveWasModified onCurveWasModified;

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool AmbiguousBinding(string path, int classID, Transform root);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern string CalculateTransformPath(Transform targetTransform, Transform root);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void ConstrainToPolynomialCurve(AnimationCurve curve);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool CurveSupportsProcedural(AnimationCurve curve);
        [Obsolete("GetAllCurves is deprecated. Use GetCurveBindings and GetObjectReferenceCurveBindings instead."), ExcludeFromDocs]
        public static AnimationClipCurveData[] GetAllCurves(AnimationClip clip)
        {
            bool includeCurveData = true;
            return GetAllCurves(clip, includeCurveData);
        }

        [Obsolete("GetAllCurves is deprecated. Use GetCurveBindings and GetObjectReferenceCurveBindings instead.")]
        public static AnimationClipCurveData[] GetAllCurves(AnimationClip clip, [DefaultValue("true")] bool includeCurveData)
        {
            EditorCurveBinding[] curveBindings = GetCurveBindings(clip);
            AnimationClipCurveData[] dataArray = new AnimationClipCurveData[curveBindings.Length];
            for (int i = 0; i < dataArray.Length; i++)
            {
                dataArray[i] = new AnimationClipCurveData(curveBindings[i]);
                if (includeCurveData)
                {
                    dataArray[i].curve = GetEditorCurve(clip, curveBindings[i]);
                }
            }
            return dataArray;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern EditorCurveBinding[] GetAnimatableBindings(GameObject targetObject, GameObject root);
        public static UnityEngine.Object GetAnimatedObject(GameObject root, EditorCurveBinding binding)
        {
            return INTERNAL_CALL_GetAnimatedObject(root, ref binding);
        }

        [Obsolete("GetAnimationClips(Animation) is deprecated. Use GetAnimationClips(GameObject) instead.")]
        public static AnimationClip[] GetAnimationClips(Animation component)
        {
            return GetAnimationClips(component.gameObject);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern AnimationClip[] GetAnimationClips(GameObject gameObject);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern AnimationClipSettings GetAnimationClipSettings(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern AnimationClipStats GetAnimationClipStats(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern AnimationEvent[] GetAnimationEvents(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern EditorCurveBinding[] GetCurveBindings(AnimationClip clip);
        public static AnimationCurve GetEditorCurve(AnimationClip clip, EditorCurveBinding binding)
        {
            return INTERNAL_CALL_GetEditorCurve(clip, ref binding);
        }

        [Obsolete("This overload is deprecated. Use the one with EditorCurveBinding instead.")]
        public static AnimationCurve GetEditorCurve(AnimationClip clip, string relativePath, System.Type type, string propertyName)
        {
            return GetEditorCurve(clip, EditorCurveBinding.FloatCurve(relativePath, type, propertyName));
        }

        public static System.Type GetEditorCurveValueType(GameObject root, EditorCurveBinding binding)
        {
            return INTERNAL_CALL_GetEditorCurveValueType(root, ref binding);
        }

        public static bool GetFloatValue(GameObject root, EditorCurveBinding binding, out float data)
        {
            return INTERNAL_CALL_GetFloatValue(root, ref binding, out data);
        }

        [Obsolete("This overload is deprecated. Use the one with EditorCurveBinding instead.")]
        public static bool GetFloatValue(GameObject root, string relativePath, System.Type type, string propertyName, out float data)
        {
            return GetFloatValue(root, EditorCurveBinding.FloatCurve(relativePath, type, propertyName), out data);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool GetGenerateMotionCurves(AnimationClip clip);
        internal static bool GetKeyBroken(Keyframe key)
        {
            return ((key.tangentMode & kBrokenMask) != 0);
        }

        internal static TangentMode GetKeyLeftTangentMode(Keyframe key)
        {
            return (TangentMode) ((key.tangentMode & kLeftTangentMask) >> 1);
        }

        internal static TangentMode GetKeyRightTangentMode(Keyframe key)
        {
            return (TangentMode) ((key.tangentMode & kRightTangentMask) >> 5);
        }

        public static ObjectReferenceKeyframe[] GetObjectReferenceCurve(AnimationClip clip, EditorCurveBinding binding)
        {
            return INTERNAL_CALL_GetObjectReferenceCurve(clip, ref binding);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern EditorCurveBinding[] GetObjectReferenceCurveBindings(AnimationClip clip);
        public static bool GetObjectReferenceValue(GameObject root, EditorCurveBinding binding, out UnityEngine.Object targetObject)
        {
            return INTERNAL_CALL_GetObjectReferenceValue(root, ref binding, out targetObject);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern EditorCurveBinding[] GetScriptableObjectAnimatableBindings(ScriptableObject scriptableObject);
        internal static System.Type GetScriptableObjectEditorCurveValueType(ScriptableObject scriptableObject, EditorCurveBinding binding)
        {
            return INTERNAL_CALL_GetScriptableObjectEditorCurveValueType(scriptableObject, ref binding);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool HasGenericRootTransform(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool HasMotionCurves(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool HasMotionFloatCurves(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool HasRootCurves(AnimationClip clip);
        [Obsolete("Use AnimationMode.InAnimationMode instead")]
        public static bool InAnimationMode()
        {
            return UnityEditor.AnimationMode.InAnimationMode();
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void Internal_CalculateAutoTangent(AnimationCurve curve, int index);
        private static float Internal_CalculateLinearTangent(AnimationCurve curve, int index, int toIndex)
        {
            Keyframe keyframe = curve[index];
            Keyframe keyframe2 = curve[toIndex];
            float a = keyframe.time - keyframe2.time;
            if (Mathf.Approximately(a, 0f))
            {
                return 0f;
            }
            Keyframe keyframe3 = curve[index];
            Keyframe keyframe4 = curve[toIndex];
            return ((keyframe3.value - keyframe4.value) / a);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern UnityEngine.Object INTERNAL_CALL_GetAnimatedObject(GameObject root, ref EditorCurveBinding binding);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern AnimationCurve INTERNAL_CALL_GetEditorCurve(AnimationClip clip, ref EditorCurveBinding binding);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern System.Type INTERNAL_CALL_GetEditorCurveValueType(GameObject root, ref EditorCurveBinding binding);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool INTERNAL_CALL_GetFloatValue(GameObject root, ref EditorCurveBinding binding, out float data);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern ObjectReferenceKeyframe[] INTERNAL_CALL_GetObjectReferenceCurve(AnimationClip clip, ref EditorCurveBinding binding);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool INTERNAL_CALL_GetObjectReferenceValue(GameObject root, ref EditorCurveBinding binding, out UnityEngine.Object targetObject);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern System.Type INTERNAL_CALL_GetScriptableObjectEditorCurveValueType(ScriptableObject scriptableObject, ref EditorCurveBinding binding);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_Internal_SetEditorCurve(AnimationClip clip, ref EditorCurveBinding binding, AnimationCurve curve);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_Internal_SetObjectReferenceCurve(AnimationClip clip, ref EditorCurveBinding binding, ObjectReferenceKeyframe[] keyframes);
        [RequiredByNativeCode]
        private static void Internal_CallAnimationClipAwake(AnimationClip clip)
        {
            if (onCurveWasModified != null)
            {
                onCurveWasModified(clip, new EditorCurveBinding(), CurveModifiedType.ClipModified);
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void Internal_SetAnimationEvents(AnimationClip clip, AnimationEvent[] events);
        private static void Internal_SetEditorCurve(AnimationClip clip, EditorCurveBinding binding, AnimationCurve curve)
        {
            INTERNAL_CALL_Internal_SetEditorCurve(clip, ref binding, curve);
        }

        private static void Internal_SetObjectReferenceCurve(AnimationClip clip, EditorCurveBinding binding, ObjectReferenceKeyframe[] keyframes)
        {
            INTERNAL_CALL_Internal_SetObjectReferenceCurve(clip, ref binding, keyframes);
        }

        private static void Internal_UpdateTangents(AnimationCurve curve, int index)
        {
            if ((index >= 0) && (index < curve.length))
            {
                Keyframe key = curve[index];
                if ((GetKeyLeftTangentMode(key) == TangentMode.Linear) && (index >= 1))
                {
                    key.inTangent = Internal_CalculateLinearTangent(curve, index, index - 1);
                    curve.MoveKey(index, key);
                }
                if ((GetKeyRightTangentMode(key) == TangentMode.Linear) && ((index + 1) < curve.length))
                {
                    key.outTangent = Internal_CalculateLinearTangent(curve, index, index + 1);
                    curve.MoveKey(index, key);
                }
                if ((GetKeyLeftTangentMode(key) == TangentMode.ClampedAuto) || (GetKeyRightTangentMode(key) == TangentMode.ClampedAuto))
                {
                    Internal_CalculateAutoTangent(curve, index);
                }
                if ((GetKeyLeftTangentMode(key) == TangentMode.Auto) || (GetKeyRightTangentMode(key) == TangentMode.Auto))
                {
                    curve.SmoothTangents(index, 0f);
                }
                if (((GetKeyLeftTangentMode(key) == TangentMode.Free) && (GetKeyRightTangentMode(key) == TangentMode.Free)) && !GetKeyBroken(key))
                {
                    key.outTangent = key.inTangent;
                    curve.MoveKey(index, key);
                }
                if (GetKeyLeftTangentMode(key) == TangentMode.Constant)
                {
                    key.inTangent = float.PositiveInfinity;
                    curve.MoveKey(index, key);
                }
                if (GetKeyRightTangentMode(key) == TangentMode.Constant)
                {
                    key.outTangent = float.PositiveInfinity;
                    curve.MoveKey(index, key);
                }
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool IsValidPolynomialCurve(AnimationCurve curve);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern System.Type PropertyModificationToEditorCurveBinding(PropertyModification modification, GameObject gameObject, out EditorCurveBinding binding);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetAdditiveReferencePose(AnimationClip clip, AnimationClip referenceClip, float time);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetAnimationClips(Animation animation, AnimationClip[] clips);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetAnimationClipSettings(AnimationClip clip, AnimationClipSettings srcClipInfo);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void SetAnimationClipSettingsNoDirty(AnimationClip clip, AnimationClipSettings srcClipInfo);
        public static void SetAnimationEvents(AnimationClip clip, AnimationEvent[] events)
        {
            if (clip == null)
            {
                throw new ArgumentNullException("clip");
            }
            if (events == null)
            {
                throw new ArgumentNullException("events");
            }
            Internal_SetAnimationEvents(clip, events);
            if (onCurveWasModified != null)
            {
                onCurveWasModified(clip, new EditorCurveBinding(), CurveModifiedType.ClipModified);
            }
        }

        [Obsolete("SetAnimationType is no longer supported", true)]
        public static void SetAnimationType(AnimationClip clip, ModelImporterAnimationType type)
        {
        }

        public static void SetEditorCurve(AnimationClip clip, EditorCurveBinding binding, AnimationCurve curve)
        {
            Internal_SetEditorCurve(clip, binding, curve);
            if (onCurveWasModified != null)
            {
                onCurveWasModified(clip, binding, (curve == null) ? CurveModifiedType.CurveDeleted : CurveModifiedType.CurveModified);
            }
        }

        [Obsolete("This overload is deprecated. Use the one with EditorCurveBinding instead.")]
        public static void SetEditorCurve(AnimationClip clip, string relativePath, System.Type type, string propertyName, AnimationCurve curve)
        {
            SetEditorCurve(clip, EditorCurveBinding.FloatCurve(relativePath, type, propertyName), curve);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void SetGenerateMotionCurves(AnimationClip clip, bool value);
        internal static void SetKeyBroken(ref Keyframe key, bool broken)
        {
            if (broken)
            {
                key.tangentMode |= kBrokenMask;
            }
            else
            {
                key.tangentMode &= ~kBrokenMask;
            }
        }

        public static void SetKeyBroken(AnimationCurve curve, int index, bool broken)
        {
            if (curve == null)
            {
                throw new ArgumentNullException("curve");
            }
            if ((index < 0) || (index >= curve.length))
            {
                throw new ArgumentException("Index out of bounds.");
            }
            Keyframe key = curve[index];
            SetKeyBroken(ref key, broken);
            curve.MoveKey(index, key);
            UpdateTangentsFromModeSurrounding(curve, index);
        }

        internal static void SetKeyLeftTangentMode(ref Keyframe key, TangentMode tangentMode)
        {
            key.tangentMode &= ~kLeftTangentMask;
            key.tangentMode |= ((int) tangentMode) << 1;
        }

        public static void SetKeyLeftTangentMode(AnimationCurve curve, int index, TangentMode tangentMode)
        {
            if (curve == null)
            {
                throw new ArgumentNullException("curve");
            }
            if ((index < 0) || (index >= curve.length))
            {
                throw new ArgumentException("Index out of bounds.");
            }
            Keyframe key = curve[index];
            if (tangentMode != TangentMode.Free)
            {
                SetKeyBroken(ref key, true);
            }
            SetKeyLeftTangentMode(ref key, tangentMode);
            curve.MoveKey(index, key);
            UpdateTangentsFromModeSurrounding(curve, index);
        }

        internal static void SetKeyRightTangentMode(ref Keyframe key, TangentMode tangentMode)
        {
            key.tangentMode &= ~kRightTangentMask;
            key.tangentMode |= ((int) tangentMode) << 5;
        }

        public static void SetKeyRightTangentMode(AnimationCurve curve, int index, TangentMode tangentMode)
        {
            if (curve == null)
            {
                throw new ArgumentNullException("curve");
            }
            if ((index < 0) || (index >= curve.length))
            {
                throw new ArgumentException("Index out of bounds.");
            }
            Keyframe key = curve[index];
            if (tangentMode != TangentMode.Free)
            {
                SetKeyBroken(ref key, true);
            }
            SetKeyRightTangentMode(ref key, tangentMode);
            curve.MoveKey(index, key);
            UpdateTangentsFromModeSurrounding(curve, index);
        }

        public static void SetObjectReferenceCurve(AnimationClip clip, EditorCurveBinding binding, ObjectReferenceKeyframe[] keyframes)
        {
            Internal_SetObjectReferenceCurve(clip, binding, keyframes);
            if (onCurveWasModified != null)
            {
                onCurveWasModified(clip, binding, (keyframes == null) ? CurveModifiedType.CurveDeleted : CurveModifiedType.CurveModified);
            }
        }

        [Obsolete("Use AnimationMode.StartAnimationmode instead")]
        public static void StartAnimationMode(UnityEngine.Object[] objects)
        {
            Debug.LogWarning("AnimationUtility.StartAnimationMode is deprecated. Use AnimationMode.StartAnimationMode with the new APIs. The objects passed to this function will no longer be reverted automatically. See AnimationMode.AddPropertyModification");
            UnityEditor.AnimationMode.StartAnimationMode();
        }

        [Obsolete("Use AnimationMode.StopAnimationMode instead")]
        public static void StopAnimationMode()
        {
            UnityEditor.AnimationMode.StopAnimationMode();
        }

        internal static void UpdateTangentsFromMode(AnimationCurve curve)
        {
            for (int i = 0; i < curve.length; i++)
            {
                Internal_UpdateTangents(curve, i);
            }
        }

        internal static void UpdateTangentsFromModeSurrounding(AnimationCurve curve, int index)
        {
            Internal_UpdateTangents(curve, index - 2);
            Internal_UpdateTangents(curve, index - 1);
            Internal_UpdateTangents(curve, index);
            Internal_UpdateTangents(curve, index + 1);
            Internal_UpdateTangents(curve, index + 2);
        }

        public enum CurveModifiedType
        {
            CurveDeleted,
            CurveModified,
            ClipModified
        }

        public delegate void OnCurveWasModified(AnimationClip clip, EditorCurveBinding binding, AnimationUtility.CurveModifiedType deleted);

        public enum TangentMode
        {
            Free,
            Auto,
            Linear,
            Constant,
            ClampedAuto
        }
    }
}

