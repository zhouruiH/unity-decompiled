﻿namespace UnityEditor.VersionControl
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class Task
    {
        private IntPtr m_thisDummy;

        internal Task()
        {
        }

        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        public extern void Dispose();
        ~Task()
        {
            this.Dispose();
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern Asset[] Internal_GetAssetList();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern ChangeSet[] Internal_GetChangeSets();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern Message[] Internal_GetMessages();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetCompletionAction(CompletionAction action);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void Wait();

        public AssetList assetList
        {
            get
            {
                AssetList list = new AssetList();
                Asset[] assetArray = this.Internal_GetAssetList();
                foreach (Asset asset in assetArray)
                {
                    list.Add(asset);
                }
                return list;
            }
        }

        public ChangeSets changeSets
        {
            get
            {
                ChangeSets sets = new ChangeSets();
                ChangeSet[] setArray = this.Internal_GetChangeSets();
                foreach (ChangeSet set in setArray)
                {
                    sets.Add(set);
                }
                return sets;
            }
        }

        public string description { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public Message[] messages { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public string progressMessage { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public int progressPct { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public int resultCode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public int secondsSpent { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool success { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public string text { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public int userIdentifier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

