﻿namespace UnityEditor.VersionControl
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class Message
    {
        private IntPtr m_thisDummy;

        internal Message()
        {
        }

        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        public extern void Dispose();
        ~Message()
        {
            this.Dispose();
        }

        private static void Info(string message)
        {
            Debug.Log("Version control:\n" + message);
        }

        public void Show()
        {
            Info(this.message);
        }

        [ThreadAndSerializationSafe]
        public string message { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        [ThreadAndSerializationSafe]
        public Severity severity { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        [Flags]
        public enum Severity
        {
            Data,
            Verbose,
            Info,
            Warning,
            Error
        }
    }
}

