﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class Menu
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool GetChecked(string menuPath);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetChecked(string menuPath, bool isChecked);
    }
}

