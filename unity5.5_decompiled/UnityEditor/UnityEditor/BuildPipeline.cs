﻿namespace UnityEditor
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEditor.BuildReporting;
    using UnityEngine;
    using UnityEngine.Internal;

    public sealed class BuildPipeline
    {
        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundle(UnityEngine.Object mainAsset, UnityEngine.Object[] assets, string pathName)
        {
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundle(UnityEngine.Object mainAsset, UnityEngine.Object[] assets, string pathName, BuildAssetBundleOptions assetBundleOptions)
        {
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundle(UnityEngine.Object mainAsset, UnityEngine.Object[] assets, string pathName, out uint crc)
        {
            crc = 0;
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundle(UnityEngine.Object mainAsset, UnityEngine.Object[] assets, string pathName, out uint crc, BuildAssetBundleOptions assetBundleOptions)
        {
            crc = 0;
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static bool BuildAssetBundle(UnityEngine.Object mainAsset, UnityEngine.Object[] assets, string pathName, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            uint num;
            return BuildAssetBundle(mainAsset, assets, pathName, out num, assetBundleOptions, targetPlatform);
        }

        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static bool BuildAssetBundle(UnityEngine.Object mainAsset, UnityEngine.Object[] assets, string pathName, out uint crc, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            crc = 0;
            try
            {
                return BuildAssetBundleInternal(mainAsset, assets, null, pathName, assetBundleOptions, targetPlatform, out crc);
            }
            catch (Exception exception)
            {
                LogBuildExceptionAndExit("BuildPipeline.BuildAssetBundle", exception);
                return false;
            }
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundleExplicitAssetNames(UnityEngine.Object[] assets, string[] assetNames, string pathName)
        {
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundleExplicitAssetNames(UnityEngine.Object[] assets, string[] assetNames, string pathName, BuildAssetBundleOptions assetBundleOptions)
        {
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundleExplicitAssetNames(UnityEngine.Object[] assets, string[] assetNames, string pathName, out uint crc)
        {
            crc = 0;
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundleExplicitAssetNames(UnityEngine.Object[] assets, string[] assetNames, string pathName, out uint crc, BuildAssetBundleOptions assetBundleOptions)
        {
            crc = 0;
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static bool BuildAssetBundleExplicitAssetNames(UnityEngine.Object[] assets, string[] assetNames, string pathName, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            uint num;
            return BuildAssetBundleExplicitAssetNames(assets, assetNames, pathName, out num, assetBundleOptions, targetPlatform);
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static bool BuildAssetBundleExplicitAssetNames(UnityEngine.Object[] assets, string[] assetNames, string pathName, out uint crc, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            crc = 0;
            try
            {
                return BuildAssetBundleInternal(null, assets, assetNames, pathName, assetBundleOptions, targetPlatform, out crc);
            }
            catch (Exception exception)
            {
                LogBuildExceptionAndExit("BuildPipeline.BuildAssetBundleExplicitAssetNames", exception);
                return false;
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool BuildAssetBundleInternal(UnityEngine.Object mainAsset, UnityEngine.Object[] assets, string[] assetNames, string pathName, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform, out uint crc);
        [ExcludeFromDocs, Obsolete("BuildAssetBundles signature has changed. Please specify the targetPlatform parameter", true)]
        public static AssetBundleManifest BuildAssetBundles(string outputPath)
        {
            BuildAssetBundleOptions none = BuildAssetBundleOptions.None;
            return BuildAssetBundles(outputPath, none);
        }

        [Obsolete("BuildAssetBundles signature has changed. Please specify the targetPlatform parameter", true)]
        public static AssetBundleManifest BuildAssetBundles(string outputPath, [DefaultValue("BuildAssetBundleOptions.None")] BuildAssetBundleOptions assetBundleOptions)
        {
            WebPlayerAssetBundlesAreNoLongerSupported();
            return null;
        }

        [Obsolete("BuildAssetBundles signature has changed. Please specify the targetPlatform parameter", true), ExcludeFromDocs]
        public static AssetBundleManifest BuildAssetBundles(string outputPath, AssetBundleBuild[] builds)
        {
            BuildAssetBundleOptions none = BuildAssetBundleOptions.None;
            return BuildAssetBundles(outputPath, builds, none);
        }

        [Obsolete("BuildAssetBundles signature has changed. Please specify the targetPlatform parameter", true)]
        public static AssetBundleManifest BuildAssetBundles(string outputPath, AssetBundleBuild[] builds, [DefaultValue("BuildAssetBundleOptions.None")] BuildAssetBundleOptions assetBundleOptions)
        {
            WebPlayerAssetBundlesAreNoLongerSupported();
            return null;
        }

        public static AssetBundleManifest BuildAssetBundles(string outputPath, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            if (!Directory.Exists(outputPath))
            {
                throw new ArgumentException("The output path \"" + outputPath + "\" doesn't exist");
            }
            return BuildAssetBundlesInternal(outputPath, assetBundleOptions, targetPlatform);
        }

        public static AssetBundleManifest BuildAssetBundles(string outputPath, AssetBundleBuild[] builds, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            if (!Directory.Exists(outputPath))
            {
                throw new ArgumentException("The output path \"" + outputPath + "\" doesn't exist");
            }
            if (builds == null)
            {
                throw new ArgumentException("AssetBundleBuild cannot be null.");
            }
            return BuildAssetBundlesWithInfoInternal(outputPath, builds, assetBundleOptions, targetPlatform);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern AssetBundleManifest BuildAssetBundlesInternal(string outputPath, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern AssetBundleManifest BuildAssetBundlesWithInfoInternal(string outputPath, AssetBundleBuild[] builds, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform);
        public static string BuildPlayer(BuildPlayerOptions buildPlayerOptions)
        {
            return BuildPlayer(buildPlayerOptions.scenes, buildPlayerOptions.locationPathName, buildPlayerOptions.assetBundleManifestPath, buildPlayerOptions.target, buildPlayerOptions.options);
        }

        public static string BuildPlayer(string[] levels, string locationPathName, BuildTarget target, BuildOptions options)
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions {
                scenes = levels,
                locationPathName = locationPathName,
                target = target,
                options = options
            };
            return BuildPlayer(buildPlayerOptions);
        }

        public static string BuildPlayer(EditorBuildSettingsScene[] levels, string locationPathName, BuildTarget target, BuildOptions options)
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions {
                scenes = EditorBuildSettingsScene.GetActiveSceneList(levels),
                locationPathName = locationPathName,
                target = target,
                options = options
            };
            return BuildPlayer(buildPlayerOptions);
        }

        private static string BuildPlayer(string[] scenes, string locationPathName, string assetBundleManifestPath, BuildTarget target, BuildOptions options)
        {
            if (string.IsNullOrEmpty(locationPathName))
            {
                if (!(((options & BuildOptions.InstallInBuildFolder) != BuildOptions.CompressTextures) && PostprocessBuildPlayer.SupportsInstallInBuildFolder(target)))
                {
                    return "The 'locationPathName' parameter for BuildPipeline.BuildPlayer should not be null or empty.";
                }
            }
            else if (string.IsNullOrEmpty(Path.GetFileName(locationPathName)))
            {
                string extensionForBuildTarget = PostprocessBuildPlayer.GetExtensionForBuildTarget(target, options);
                if (!string.IsNullOrEmpty(extensionForBuildTarget))
                {
                    return string.Format("For the '{0}' target the 'locationPathName' parameter for BuildPipeline.BuildPlayer should not end with a directory separator.\nProvided path: '{1}', expected a path with the extension '.{2}'.", target, locationPathName, extensionForBuildTarget);
                }
            }
            try
            {
                return BuildPlayerInternal(scenes, locationPathName, assetBundleManifestPath, target, options).SummarizeErrors();
            }
            catch (Exception exception)
            {
                LogBuildExceptionAndExit("BuildPipeline.BuildPlayer", exception);
                return "";
            }
        }

        private static BuildReport BuildPlayerInternal(string[] levels, string locationPathName, string assetBundleManifestPath, BuildTarget target, BuildOptions options)
        {
            if (((BuildOptions.EnableHeadlessMode & options) != BuildOptions.CompressTextures) && ((BuildOptions.Development & options) != BuildOptions.CompressTextures))
            {
                throw new Exception("Unsupported build setting: cannot build headless development player");
            }
            return BuildPlayerInternalNoCheck(levels, locationPathName, assetBundleManifestPath, target, options, false);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern BuildReport BuildPlayerInternalNoCheck(string[] levels, string locationPathName, string assetBundleManifestPath, BuildTarget target, BuildOptions options, bool delayToAfterScriptReload);
        [Obsolete("BuildStreamedSceneAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static string BuildStreamedSceneAssetBundle(string[] levels, string locationPath, BuildTarget target)
        {
            return BuildPlayer(levels, locationPath, target, BuildOptions.BuildAdditionalStreamedScenes);
        }

        [Obsolete("BuildStreamedSceneAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static string BuildStreamedSceneAssetBundle(string[] levels, string locationPath, BuildTarget target, BuildOptions options)
        {
            return BuildPlayer(levels, locationPath, target, options | BuildOptions.BuildAdditionalStreamedScenes);
        }

        [Obsolete("BuildStreamedSceneAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static string BuildStreamedSceneAssetBundle(string[] levels, string locationPath, BuildTarget target, out uint crc)
        {
            return BuildStreamedSceneAssetBundle(levels, locationPath, target, out crc, BuildOptions.CompressTextures);
        }

        [Obsolete("BuildStreamedSceneAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static string BuildStreamedSceneAssetBundle(string[] levels, string locationPath, BuildTarget target, out uint crc, BuildOptions options)
        {
            crc = 0;
            try
            {
                BuildReport report = BuildPlayerInternal(levels, locationPath, null, target, (options | BuildOptions.BuildAdditionalStreamedScenes) | BuildOptions.ComputeCRC);
                crc = report.crc;
                string str = report.SummarizeErrors();
                UnityEngine.Object.DestroyImmediate(report, true);
                return str;
            }
            catch (Exception exception)
            {
                LogBuildExceptionAndExit("BuildPipeline.BuildStreamedSceneAssetBundle", exception);
                return "";
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern string GetBuildTargetAdvancedLicenseName(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern BuildTarget GetBuildTargetByName(string platform);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern BuildTargetGroup GetBuildTargetGroup(BuildTarget platform);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern BuildTargetGroup GetBuildTargetGroupByName(string platform);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern string GetBuildTargetGroupDisplayName(BuildTargetGroup targetPlatformGroup);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern string GetBuildTargetGroupName(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        internal static extern string GetBuildTargetName(BuildTarget targetPlatform);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern string GetBuildToolsDirectory(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool GetCRCForAssetBundle(string targetPath, out uint crc);
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        internal static extern string GetEditorTargetName();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool GetHashForAssetBundle(string targetPath, out Hash128 hash);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern string GetMonoBinDirectory(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern string GetMonoLibDirectory(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern string GetMonoProfileLibDirectory(BuildTarget target, string profile);
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        internal static extern string GetPlaybackEngineDirectory(BuildTarget target, BuildOptions options);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool IsBuildTargetCompatibleWithOS(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool IsBuildTargetSupported(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool IsUnityScriptEvalSupported(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        internal static extern bool LicenseCheck(BuildTarget target);
        private static void LogBuildExceptionAndExit(string buildFunctionName, Exception exception)
        {
            object[] args = new object[] { buildFunctionName };
            Debug.LogErrorFormat("Internal Error in {0}:", args);
            Debug.LogException(exception);
            EditorApplication.Exit(1);
        }

        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("PopAssetDependencies has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static extern void PopAssetDependencies();
        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("PushAssetDependencies has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static extern void PushAssetDependencies();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void SetPlaybackEngineDirectory(BuildTarget target, BuildOptions options, string playbackEngineDirectory);
        [Obsolete("WebPlayer has been removed in 5.4", true)]
        private static bool WebPlayerAssetBundlesAreNoLongerSupported()
        {
            throw new InvalidOperationException("WebPlayer asset bundles can no longer be built in 5.4+");
        }

        public static bool isBuildingPlayer { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

