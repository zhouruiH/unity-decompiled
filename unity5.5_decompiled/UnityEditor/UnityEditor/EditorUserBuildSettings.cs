﻿namespace UnityEditor
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public sealed class EditorUserBuildSettings
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static string <xboxOneAdditionalDebugPorts>k__BackingField;
        public static System.Action activeBuildTargetChanged;

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern string GetBuildLocation(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern string GetPlatformSettings(string platformName, string name);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool GetWSADotNetNative(WSABuildType config);
        internal static void Internal_ActiveBuildTargetChanged()
        {
            if (activeBuildTargetChanged != null)
            {
                activeBuildTargetChanged();
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetBuildLocation(BuildTarget target, string location);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetPlatformSettings(string platformName, string name, string value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetWSADotNetNative(WSABuildType config, bool enabled);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool SwitchActiveBuildTarget(BuildTarget target);

        public static BuildTarget activeBuildTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static string[] activeScriptCompilationDefines { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool allowDebugging { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static MobileTextureSubtarget androidBuildSubtarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static AndroidBuildSystem androidBuildSystem { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool buildScriptsOnly { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool compressFilesInPackage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool compressWithPsArc { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool connectProfiler { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool development { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool enableHeadlessMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool explicitDivideByZeroChecks { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool explicitNullChecks { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool exportAsGoogleAndroidProject { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool forceInstallation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool forceOptimizeScriptCompilation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool installInBuildFolder { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static iOSBuildType iOSBuildConfigType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool n3dsCreateCIAFile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool needSubmissionMaterials { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static PS4BuildSubtarget ps4BuildSubtarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static PS4HardwareTarget ps4HardwareTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static PSMBuildSubtarget psmBuildSubtarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static PSP2BuildSubtarget psp2BuildSubtarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static BuildTargetGroup selectedBuildTargetGroup { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static BuildTarget selectedStandaloneTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int streamingInstallLaunchRange { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool symlinkLibraries { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal static bool symlinkTrampoline { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static MobileTextureSubtarget tizenBuildSubtarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool webGLUsePreBuiltUnityEngine { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool webPlayerOfflineDeployment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool webPlayerStreamed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int wiiUBootMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static WiiUBuildDebugLevel wiiUBuildDebugLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static WiiUBuildOutput wiiuBuildOutput { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool wiiUEnableNetAPI { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static WSABuildAndRunDeployTarget wsaBuildAndRunDeployTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool wsaGenerateReferenceProjects { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static WSASDK wsaSDK { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static WSASubtarget wsaSubtarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static WSAUWPBuildType wsaUWPBuildType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static XboxBuildSubtarget xboxBuildSubtarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string xboxOneAdditionalDebugPorts
        {
            [CompilerGenerated]
            get
            {
                return <xboxOneAdditionalDebugPorts>k__BackingField;
            }
            [CompilerGenerated]
            set
            {
                <xboxOneAdditionalDebugPorts>k__BackingField = value;
            }
        }

        public static XboxOneDeployMethod xboxOneDeployMethod { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string xboxOneNetworkSharePath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string xboxOneUsername { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

