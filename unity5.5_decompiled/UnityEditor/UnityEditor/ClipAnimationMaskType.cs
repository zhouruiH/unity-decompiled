﻿namespace UnityEditor
{
    using System;

    public enum ClipAnimationMaskType
    {
        CopyFromOther = 1,
        CreateFromThisModel = 0,
        None = 3
    }
}

