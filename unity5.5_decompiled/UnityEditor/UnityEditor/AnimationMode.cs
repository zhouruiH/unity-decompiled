﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class AnimationMode
    {
        internal static AnimationModeChangedCallback animationModeChangedCallback;
        private static Color s_AnimatedPropertyColorDark = new Color(1f, 0.55f, 0.5f, 1f);
        private static Color s_AnimatedPropertyColorLight = new Color(1f, 0.65f, 0.6f, 1f);
        private static bool s_InAnimationPlaybackMode = false;

        public static void AddPropertyModification(EditorCurveBinding binding, PropertyModification modification, bool keepPrefabOverride)
        {
            INTERNAL_CALL_AddPropertyModification(ref binding, modification, keepPrefabOverride);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void BeginSampling();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void EndSampling();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool InAnimationMode();
        internal static bool InAnimationPlaybackMode()
        {
            return s_InAnimationPlaybackMode;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_AddPropertyModification(ref EditorCurveBinding binding, PropertyModification modification, bool keepPrefabOverride);
        private static void InternalAnimationModeChanged(bool newValue)
        {
            if (animationModeChangedCallback != null)
            {
                animationModeChangedCallback(newValue);
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool IsPropertyAnimated(UnityEngine.Object target, string propertyPath);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SampleAnimationClip(GameObject gameObject, AnimationClip clip, float time);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void StartAnimationMode();
        internal static void StartAnimationPlaybackMode()
        {
            s_InAnimationPlaybackMode = true;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void StopAnimationMode();
        internal static void StopAnimationPlaybackMode()
        {
            s_InAnimationPlaybackMode = false;
        }

        public static Color animatedPropertyColor
        {
            get
            {
                return (!EditorGUIUtility.isProSkin ? s_AnimatedPropertyColorLight : s_AnimatedPropertyColorDark);
            }
        }

        internal delegate void AnimationModeChangedCallback(bool newValue);
    }
}

