﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class LODUtility
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern float CalculateDistance(Camera camera, float relativeScreenHeight, LODGroup group);
        public static void CalculateLODGroupBoundingBox(LODGroup group)
        {
            if (group == null)
            {
                throw new ArgumentNullException("group");
            }
            group.RecalculateBounds();
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern LODVisualizationInformation CalculateVisualizationData(Camera camera, LODGroup group, int lodLevel);
        internal static Vector3 CalculateWorldReferencePoint(LODGroup group)
        {
            Vector3 vector;
            INTERNAL_CALL_CalculateWorldReferencePoint(group, out vector);
            return vector;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_CalculateWorldReferencePoint(LODGroup group, out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool NeedUpdateLODGroupBoundingBox(LODGroup group);
    }
}

