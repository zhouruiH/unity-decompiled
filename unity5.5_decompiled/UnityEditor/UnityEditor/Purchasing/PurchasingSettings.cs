﻿namespace UnityEditor.Purchasing
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class PurchasingSettings
    {
        [ThreadAndSerializationSafe]
        public static bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

