﻿namespace UnityEditor
{
    using System;

    [Obsolete("SplashScreenStyle deprecated, Use PlayerSettings.SplashScreen.UnityLogoStyle instead")]
    public enum SplashScreenStyle
    {
        Light,
        Dark
    }
}

