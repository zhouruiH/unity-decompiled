﻿namespace UnityEditor.CrashReporting
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class CrashReportingSettings
    {
        public static bool captureEditorExceptions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [ThreadAndSerializationSafe]
        public static bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

