﻿namespace UnityEditor.RestService
{
    using System;
    using System.Runtime.CompilerServices;

    internal delegate void WriteResponse(UnityEditor.RestService.HttpStatusCode code, string payload);
}

