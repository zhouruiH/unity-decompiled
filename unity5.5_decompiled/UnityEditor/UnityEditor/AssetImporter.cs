﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class AssetImporter : UnityEngine.Object
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern AssetImporter GetAtPath(string path);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern int LocalFileIDToClassID(long fileId);
        public void SaveAndReimport()
        {
            AssetDatabase.ImportAsset(this.assetPath);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetAssetBundleNameAndVariant(string assetBundleName, string assetBundleVariant);

        public string assetBundleName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string assetBundleVariant { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string assetPath { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public ulong assetTimeStamp { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public string userData { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

