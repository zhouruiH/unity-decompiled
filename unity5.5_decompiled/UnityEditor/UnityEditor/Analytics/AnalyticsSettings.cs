﻿namespace UnityEditor.Analytics
{
    using System;
    using System.Runtime.CompilerServices;

    public static class AnalyticsSettings
    {
        public static bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool testMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

