﻿namespace UnityEditor.Rendering
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    [StructLayout(LayoutKind.Sequential)]
    public struct TierSettings
    {
        public ShaderQuality standardShaderQuality;
        public bool cascadedShadowMaps;
        public bool reflectionProbeBoxProjection;
        public bool reflectionProbeBlending;
        public RenderingPath renderingPath;
    }
}

