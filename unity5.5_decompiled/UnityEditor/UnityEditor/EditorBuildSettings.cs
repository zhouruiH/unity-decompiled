﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class EditorBuildSettings
    {
        public static EditorBuildSettingsScene[] scenes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

