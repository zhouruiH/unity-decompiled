﻿namespace UnityEditor.TreeViewTests
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEditor;

    internal class LazyTestDataSource : UnityEditor.LazyTreeViewDataSource
    {
        private UnityEditor.TreeViewTests.BackendData m_Backend;

        public LazyTestDataSource(UnityEditor.TreeView treeView, UnityEditor.TreeViewTests.BackendData data) : base(treeView)
        {
            this.m_Backend = data;
            this.FetchData();
        }

        private void AddVisibleChildrenRecursive(UnityEditor.TreeViewTests.BackendData.Foo source, UnityEditor.TreeViewItem dest)
        {
            if (this.IsExpanded(source.id))
            {
                if ((source.children != null) && (source.children.Count > 0))
                {
                    dest.children = new List<UnityEditor.TreeViewItem>(source.children.Count);
                    for (int i = 0; i < source.children.Count; i++)
                    {
                        UnityEditor.TreeViewTests.BackendData.Foo foo = source.children[i];
                        dest.children.Add(new UnityEditor.TreeViewTests.FooTreeViewItem(foo.id, dest.depth + 1, dest, foo.name, foo));
                        this.itemCounter++;
                        this.AddVisibleChildrenRecursive(foo, dest.children[i]);
                    }
                }
            }
            else if (source.hasChildren)
            {
                dest.children = new List<UnityEditor.TreeViewItem> { new UnityEditor.TreeViewItem(-1, -1, null, string.Empty) };
            }
        }

        public override bool CanBeParent(UnityEditor.TreeViewItem item)
        {
            return item.hasChildren;
        }

        public override void FetchData()
        {
            this.itemCounter = 1;
            base.m_RootItem = new UnityEditor.TreeViewTests.FooTreeViewItem(this.m_Backend.root.id, 0, null, this.m_Backend.root.name, this.m_Backend.root);
            this.AddVisibleChildrenRecursive(this.m_Backend.root, base.m_RootItem);
            base.m_VisibleRows = new List<UnityEditor.TreeViewItem>();
            base.GetVisibleItemsRecursive(base.m_RootItem, base.m_VisibleRows);
            base.m_NeedRefreshVisibleFolders = false;
        }

        protected override HashSet<int> GetParentsAbove(int id)
        {
            HashSet<int> set = new HashSet<int>();
            for (UnityEditor.TreeViewTests.BackendData.Foo foo = UnityEditor.TreeViewTests.BackendData.FindNodeRecursive(this.m_Backend.root, id); foo != null; foo = foo.parent)
            {
                if (foo.parent != null)
                {
                    set.Add(foo.parent.id);
                }
            }
            return set;
        }

        protected override HashSet<int> GetParentsBelow(int id)
        {
            return this.m_Backend.GetParentsBelow(id);
        }

        public int itemCounter { get; private set; }
    }
}

