﻿namespace UnityEditor.TreeViewTests
{
    using System;
    using UnityEditor;
    using UnityEngine;

    internal class TreeViewTestWindow : UnityEditor.EditorWindow, UnityEditor.IHasCustomMenu
    {
        private UnityEditor.TreeViewTests.BackendData m_BackendData;
        private UnityEditor.TreeViewTests.BackendData m_BackendData2;
        private TestType m_TestType = TestType.TreeWithCustomItemHeight;
        private UnityEditor.TreeViewTests.TreeViewTest m_TreeViewTest;
        private UnityEditor.TreeViewTests.TreeViewTest m_TreeViewTest2;
        private UnityEditor.TreeViewTests.TreeViewTestWithCustomHeight m_TreeViewWithCustomHeight;

        public TreeViewTestWindow()
        {
            base.titleContent = new GUIContent("TreeView Test");
        }

        public virtual void AddItemsToMenu(UnityEditor.GenericMenu menu)
        {
            menu.AddItem(new GUIContent("Large TreeView"), this.m_TestType == TestType.LargeTreesWithStandardGUI, (UnityEditor.GenericMenu.MenuFunction) (() => (this.m_TestType = TestType.LargeTreesWithStandardGUI)));
            menu.AddItem(new GUIContent("Custom Item Height TreeView"), this.m_TestType == TestType.TreeWithCustomItemHeight, (UnityEditor.GenericMenu.MenuFunction) (() => (this.m_TestType = TestType.TreeWithCustomItemHeight)));
        }

        private void OnEnable()
        {
            base.position = new Rect(100f, 100f, 600f, 600f);
        }

        private void OnGUI()
        {
            switch (this.m_TestType)
            {
                case TestType.LargeTreesWithStandardGUI:
                    this.TestLargeTreesWithFixedItemHeightAndPingingAndFraming();
                    break;

                case TestType.TreeWithCustomItemHeight:
                    this.TestTreeWithCustomItemHeights();
                    break;
            }
        }

        private void TestLargeTreesWithFixedItemHeightAndPingingAndFraming()
        {
            Rect rect = new Rect(0f, 0f, base.position.width / 2f, base.position.height);
            Rect rect2 = new Rect(base.position.width / 2f, 0f, base.position.width / 2f, base.position.height);
            if (this.m_TreeViewTest == null)
            {
                this.m_BackendData = new UnityEditor.TreeViewTests.BackendData();
                this.m_BackendData.GenerateData(0xf4240);
                bool lazy = false;
                this.m_TreeViewTest = new UnityEditor.TreeViewTests.TreeViewTest(this, lazy);
                this.m_TreeViewTest.Init(rect, this.m_BackendData);
                lazy = true;
                this.m_TreeViewTest2 = new UnityEditor.TreeViewTests.TreeViewTest(this, lazy);
                this.m_TreeViewTest2.Init(rect2, this.m_BackendData);
            }
            this.m_TreeViewTest.OnGUI(rect);
            this.m_TreeViewTest2.OnGUI(rect2);
            UnityEditor.EditorGUI.DrawRect(new Rect(rect.xMax - 1f, 0f, 2f, base.position.height), new Color(0.4f, 0.4f, 0.4f, 0.8f));
        }

        private void TestTreeWithCustomItemHeights()
        {
            Rect rect = new Rect(0f, 0f, base.position.width, base.position.height);
            if (this.m_TreeViewWithCustomHeight == null)
            {
                this.m_BackendData2 = new UnityEditor.TreeViewTests.BackendData();
                this.m_BackendData2.GenerateData(300);
                this.m_TreeViewWithCustomHeight = new UnityEditor.TreeViewTests.TreeViewTestWithCustomHeight(this, this.m_BackendData2, rect);
            }
            this.m_TreeViewWithCustomHeight.OnGUI(rect);
        }

        private enum TestType
        {
            LargeTreesWithStandardGUI,
            TreeWithCustomItemHeight
        }
    }
}

