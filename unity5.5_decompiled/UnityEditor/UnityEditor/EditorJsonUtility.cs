﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;

    public static class EditorJsonUtility
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void FromJsonOverwrite(string json, object objectToOverwrite);
        public static string ToJson(object obj)
        {
            return ToJson(obj, false);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern string ToJson(object obj, bool prettyPrint);
    }
}

