﻿namespace UnityEditor
{
    using System;

    public enum StereoRenderingPath
    {
        MultiPass,
        SinglePass,
        Instancing
    }
}

