﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class LightingDataAsset : UnityEngine.Object
    {
        internal bool isValid { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal string validityErrorMessage { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

