﻿namespace UnityEditor
{
    using System;

    public enum MacFullscreenMode
    {
        [Obsolete("Capture Display mode is deprecated, Use FullscreenWindow instead")]
        CaptureDisplay = 0,
        FullscreenWindow = 1,
        FullscreenWindowWithDockAndMenuBar = 2
    }
}

