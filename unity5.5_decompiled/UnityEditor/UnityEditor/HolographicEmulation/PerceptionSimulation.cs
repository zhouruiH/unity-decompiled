﻿namespace UnityEditor.HolographicEmulation
{
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class PerceptionSimulation
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void Initialize();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void LoadRoom(string id);
        internal static void SetEmulationMode(EmulationMode mode)
        {
        }

        internal static void SetGestureHand(GestureHand hand)
        {
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void Shutdown();
    }
}

