﻿namespace UnityEditor
{
    using System;

    public enum WebGLCompressionFormat
    {
        Brotli,
        Gzip,
        Disabled
    }
}

