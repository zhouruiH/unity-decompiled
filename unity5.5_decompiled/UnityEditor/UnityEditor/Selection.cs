﻿namespace UnityEditor
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class Selection
    {
        public static System.Action selectionChanged;

        internal static void Add(int instanceID)
        {
            List<int> list = new List<int>(instanceIDs);
            if (list.IndexOf(instanceID) < 0)
            {
                list.Add(instanceID);
                instanceIDs = list.ToArray();
            }
        }

        internal static void Add(UnityEngine.Object obj)
        {
            if (obj != null)
            {
                Add(obj.GetInstanceID());
            }
        }

        public static bool Contains(int instanceID)
        {
            return (Array.IndexOf<int>(instanceIDs, instanceID) != -1);
        }

        public static bool Contains(UnityEngine.Object obj)
        {
            return Contains(obj.GetInstanceID());
        }

        public static UnityEngine.Object[] GetFiltered(System.Type type, UnityEditor.SelectionMode mode)
        {
            ArrayList list = new ArrayList();
            if ((type == typeof(Component)) || type.IsSubclassOf(typeof(Component)))
            {
                Transform[] transforms = GetTransforms(mode);
                foreach (Transform transform in transforms)
                {
                    Component component = transform.GetComponent(type);
                    if (component != null)
                    {
                        list.Add(component);
                    }
                }
            }
            else if ((type == typeof(GameObject)) || type.IsSubclassOf(typeof(GameObject)))
            {
                Transform[] transformArray3 = GetTransforms(mode);
                foreach (Transform transform2 in transformArray3)
                {
                    list.Add(transform2.gameObject);
                }
            }
            else
            {
                UnityEngine.Object[] objectsMode = GetObjectsMode(mode);
                foreach (UnityEngine.Object obj2 in objectsMode)
                {
                    if ((obj2 != null) && ((obj2.GetType() == type) || obj2.GetType().IsSubclassOf(type)))
                    {
                        list.Add(obj2);
                    }
                }
            }
            return (UnityEngine.Object[]) list.ToArray(typeof(UnityEngine.Object));
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern UnityEngine.Object[] GetObjectsMode(UnityEditor.SelectionMode mode);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern Transform[] GetTransforms(UnityEditor.SelectionMode mode);
        private static void Internal_CallSelectionChanged()
        {
            if (selectionChanged != null)
            {
                selectionChanged();
            }
        }

        internal static void Remove(int instanceID)
        {
            List<int> list = new List<int>(instanceIDs);
            list.Remove(instanceID);
            instanceIDs = list.ToArray();
        }

        internal static void Remove(UnityEngine.Object obj)
        {
            if (obj != null)
            {
                Remove(obj.GetInstanceID());
            }
        }

        public static GameObject activeGameObject { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int activeInstanceID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static UnityEngine.Object activeObject { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static Transform activeTransform { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string[] assetGUIDs { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal static string[] assetGUIDsDeepSelection { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static GameObject[] gameObjects { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static int[] instanceIDs { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static UnityEngine.Object[] objects { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static Transform[] transforms { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

