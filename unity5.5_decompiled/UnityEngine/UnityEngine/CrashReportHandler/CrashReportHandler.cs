﻿namespace UnityEngine.CrashReportHandler
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class CrashReportHandler
    {
        [ThreadAndSerializationSafe]
        public static bool enableCaptureExceptions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

