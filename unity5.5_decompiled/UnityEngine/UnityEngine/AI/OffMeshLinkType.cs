﻿namespace UnityEngine.AI
{
    using System;
    using UnityEngine.Scripting.APIUpdating;

    [MovedFrom("UnityEngine")]
    public enum OffMeshLinkType
    {
        LinkTypeManual,
        LinkTypeDropDown,
        LinkTypeJumpAcross
    }
}

