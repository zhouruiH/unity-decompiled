﻿namespace UnityEngine.AI
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Scripting.APIUpdating;

    [MovedFrom("UnityEngine")]
    public sealed class NavMeshAgent : Behaviour
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void ActivateCurrentOffMeshLink(bool activated);
        public bool CalculatePath(Vector3 targetPosition, NavMeshPath path)
        {
            path.ClearCorners();
            return this.CalculatePathInternal(targetPosition, path);
        }

        private bool CalculatePathInternal(Vector3 targetPosition, NavMeshPath path)
        {
            return INTERNAL_CALL_CalculatePathInternal(this, ref targetPosition, path);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void CompleteOffMeshLink();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void CopyPathTo(NavMeshPath path);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern bool FindClosestEdge(out NavMeshHit hit);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern float GetAreaCost(int areaIndex);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern OffMeshLinkData GetCurrentOffMeshLinkDataInternal();
        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("Use GetAreaCost instead.")]
        public extern float GetLayerCost(int layer);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern OffMeshLinkData GetNextOffMeshLinkDataInternal();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool INTERNAL_CALL_CalculatePathInternal(NavMeshAgent self, ref Vector3 targetPosition, NavMeshPath path);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_Move(NavMeshAgent self, ref Vector3 offset);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool INTERNAL_CALL_Raycast(NavMeshAgent self, ref Vector3 targetPosition, out NavMeshHit hit);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool INTERNAL_CALL_SetDestination(NavMeshAgent self, ref Vector3 target);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool INTERNAL_CALL_Warp(NavMeshAgent self, ref Vector3 newPosition);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_desiredVelocity(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_destination(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_nextPosition(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_pathEndPosition(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_steeringTarget(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_velocity(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_destination(ref Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_nextPosition(ref Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_velocity(ref Vector3 value);
        public void Move(Vector3 offset)
        {
            INTERNAL_CALL_Move(this, ref offset);
        }

        public bool Raycast(Vector3 targetPosition, out NavMeshHit hit)
        {
            return INTERNAL_CALL_Raycast(this, ref targetPosition, out hit);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void ResetPath();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void Resume();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern bool SamplePathPosition(int areaMask, float maxDistance, out NavMeshHit hit);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetAreaCost(int areaIndex, float areaCost);
        public bool SetDestination(Vector3 target)
        {
            return INTERNAL_CALL_SetDestination(this, ref target);
        }

        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("Use SetAreaCost instead.")]
        public extern void SetLayerCost(int layer, float cost);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern bool SetPath(NavMeshPath path);
        public void Stop()
        {
            this.StopInternal();
        }

        [Obsolete("Use Stop() instead")]
        public void Stop(bool stopUpdates)
        {
            this.StopInternal();
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void StopInternal();
        public bool Warp(Vector3 newPosition)
        {
            return INTERNAL_CALL_Warp(this, ref newPosition);
        }

        public float acceleration { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float angularSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int areaMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool autoBraking { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool autoRepath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool autoTraverseOffMeshLink { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int avoidancePriority { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float baseOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public OffMeshLinkData currentOffMeshLinkData
        {
            get
            {
                return this.GetCurrentOffMeshLinkDataInternal();
            }
        }

        public Vector3 desiredVelocity
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_desiredVelocity(out vector);
                return vector;
            }
        }

        public Vector3 destination
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_destination(out vector);
                return vector;
            }
            set
            {
                this.INTERNAL_set_destination(ref value);
            }
        }

        public bool hasPath { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public float height { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool isOnNavMesh { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isOnOffMeshLink { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isPathStale { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public OffMeshLinkData nextOffMeshLinkData
        {
            get
            {
                return this.GetNextOffMeshLinkDataInternal();
            }
        }

        public Vector3 nextPosition
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_nextPosition(out vector);
                return vector;
            }
            set
            {
                this.INTERNAL_set_nextPosition(ref value);
            }
        }

        public ObstacleAvoidanceType obstacleAvoidanceType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public NavMeshPath path
        {
            get
            {
                NavMeshPath path = new NavMeshPath();
                this.CopyPathTo(path);
                return path;
            }
            set
            {
                if (value == null)
                {
                    throw new NullReferenceException();
                }
                this.SetPath(value);
            }
        }

        public Vector3 pathEndPosition
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_pathEndPosition(out vector);
                return vector;
            }
        }

        public bool pathPending { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public NavMeshPathStatus pathStatus { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float remainingDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public float speed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Vector3 steeringTarget
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_steeringTarget(out vector);
                return vector;
            }
        }

        public float stoppingDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool updatePosition { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool updateRotation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Vector3 velocity
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_velocity(out vector);
                return vector;
            }
            set
            {
                this.INTERNAL_set_velocity(ref value);
            }
        }

        [Obsolete("Use areaMask instead.")]
        public int walkableMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

