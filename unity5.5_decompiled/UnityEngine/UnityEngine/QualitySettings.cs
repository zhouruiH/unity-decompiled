﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.Internal;

    public sealed class QualitySettings : UnityEngine.Object
    {
        [ExcludeFromDocs]
        public static void DecreaseLevel()
        {
            bool applyExpensiveChanges = false;
            DecreaseLevel(applyExpensiveChanges);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void DecreaseLevel([DefaultValue("false")] bool applyExpensiveChanges);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern int GetQualityLevel();
        [ExcludeFromDocs]
        public static void IncreaseLevel()
        {
            bool applyExpensiveChanges = false;
            IncreaseLevel(applyExpensiveChanges);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void IncreaseLevel([DefaultValue("false")] bool applyExpensiveChanges);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_get_shadowCascade4Split(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_set_shadowCascade4Split(ref Vector3 value);
        [ExcludeFromDocs]
        public static void SetQualityLevel(int index)
        {
            bool applyExpensiveChanges = true;
            SetQualityLevel(index, applyExpensiveChanges);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetQualityLevel(int index, [DefaultValue("true")] bool applyExpensiveChanges);

        public static ColorSpace activeColorSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static AnisotropicFiltering anisotropicFiltering { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int antiAliasing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int asyncUploadBufferSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int asyncUploadTimeSlice { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool billboardsFaceCameraPosition { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static BlendWeights blendWeights { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use GetQualityLevel and SetQualityLevel")]
        public static QualityLevel currentLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static ColorSpace desiredColorSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static float lodBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int masterTextureLimit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int maximumLODLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int maxQueuedFrames { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string[] names { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static int particleRaycastBudget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int pixelLightCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool realtimeReflectionProbes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float shadowCascade2Split { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static Vector3 shadowCascade4Split
        {
            get
            {
                Vector3 vector;
                INTERNAL_get_shadowCascade4Split(out vector);
                return vector;
            }
            set
            {
                INTERNAL_set_shadowCascade4Split(ref value);
            }
        }

        public static int shadowCascades { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float shadowDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float shadowNearPlaneOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static ShadowProjection shadowProjection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static ShadowResolution shadowResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static ShadowQuality shadows { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool softParticles { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool softVegetation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int vSyncCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

