﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class Ping
    {
        internal IntPtr m_Ptr;

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern Ping(string address);
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        public extern void DestroyPing();
        ~Ping()
        {
            this.DestroyPing();
        }

        public string ip { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isDone { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public int time { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

