﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class MeshRenderer : Renderer
    {
        public Mesh additionalVertexStreams { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

