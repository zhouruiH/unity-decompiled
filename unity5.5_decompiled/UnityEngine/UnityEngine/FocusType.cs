﻿namespace UnityEngine
{
    using System;

    public enum FocusType
    {
        Keyboard = 1,
        [Obsolete("FocusType.Native now behaves the same as FocusType.Passive in all OS cases. (UnityUpgradable) -> Passive", false)]
        Native = 0,
        Passive = 2
    }
}

