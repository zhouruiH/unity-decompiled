﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class LODGroup : Component
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void ForceLOD(int index);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern LOD[] GetLODs();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_localReferencePoint(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_localReferencePoint(ref Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void RecalculateBounds();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetLODs(LOD[] lods);
        [Obsolete("Use SetLODs instead.")]
        public void SetLODS(LOD[] lods)
        {
            this.SetLODs(lods);
        }

        public bool animateCrossFading { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float crossFadeAnimationDuration { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public LODFadeMode fadeMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Vector3 localReferencePoint
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_localReferencePoint(out vector);
                return vector;
            }
            set
            {
                this.INTERNAL_set_localReferencePoint(ref value);
            }
        }

        public int lodCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public float size { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

