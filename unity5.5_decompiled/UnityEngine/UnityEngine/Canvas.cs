﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using UnityEngine.Scripting;

    public sealed class Canvas : Behaviour
    {
        public static  event WillRenderCanvases willRenderCanvases;

        public static void ForceUpdateCanvases()
        {
            SendWillRenderCanvases();
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern Material GetDefaultCanvasMaterial();
        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("Shared default material now used for text and general UI elements, call Canvas.GetDefaultCanvasMaterial()")]
        public static extern Material GetDefaultCanvasTextMaterial();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern Material GetETC1SupportedCanvasMaterial();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_pixelRect(out Rect value);
        [RequiredByNativeCode]
        private static void SendWillRenderCanvases()
        {
            if (willRenderCanvases != null)
            {
                willRenderCanvases();
            }
        }

        public int cachedSortingLayerValue { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isRootCanvas { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public float normalizedSortingGridSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool overridePixelPerfect { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool overrideSorting { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool pixelPerfect { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Rect pixelRect
        {
            get
            {
                Rect rect;
                this.INTERNAL_get_pixelRect(out rect);
                return rect;
            }
        }

        public float planeDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float referencePixelsPerUnit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public RenderMode renderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int renderOrder { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public Canvas rootCanvas { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public float scaleFactor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Setting normalizedSize via a int is not supported. Please use normalizedSortingGridSize")]
        public int sortingGridNormalizedSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int sortingLayerID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string sortingLayerName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int sortingOrder { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int targetDisplay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Camera worldCamera { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public delegate void WillRenderCanvases();
    }
}

