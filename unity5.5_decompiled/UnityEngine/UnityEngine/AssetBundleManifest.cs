﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class AssetBundleManifest : UnityEngine.Object
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern string[] GetAllAssetBundles();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern string[] GetAllAssetBundlesWithVariant();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern string[] GetAllDependencies(string assetBundleName);
        public Hash128 GetAssetBundleHash(string assetBundleName)
        {
            Hash128 hash;
            INTERNAL_CALL_GetAssetBundleHash(this, assetBundleName, out hash);
            return hash;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern string[] GetDirectDependencies(string assetBundleName);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetAssetBundleHash(AssetBundleManifest self, string assetBundleName, out Hash128 value);
    }
}

