﻿namespace UnityEngine
{
    using System;

    public enum ParticleSystemNoiseQuality
    {
        Low,
        Medium,
        High
    }
}

