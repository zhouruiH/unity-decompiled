﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class BillboardRenderer : Renderer
    {
        public BillboardAsset billboard { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

