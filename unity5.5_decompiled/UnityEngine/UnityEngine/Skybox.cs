﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class Skybox : Behaviour
    {
        public Material material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

