﻿namespace UnityEngine
{
    using System;
    using UnityEngine.Scripting;

    [RequiredByNativeCode, AttributeUsage(AttributeTargets.Class)]
    public sealed class PreferBinarySerialization : Attribute
    {
    }
}

