﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.Scripting;

    [StructLayout(LayoutKind.Sequential), RequiredByNativeCode]
    public sealed class Gradient
    {
        internal IntPtr m_Ptr;
        [RequiredByNativeCode]
        public Gradient()
        {
            this.Init();
        }

        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        private extern void Init();
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        private extern void Cleanup();
        ~Gradient()
        {
            this.Cleanup();
        }

        public Color Evaluate(float time)
        {
            Color color;
            INTERNAL_CALL_Evaluate(this, time, out color);
            return color;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_Evaluate(Gradient self, float time, out Color value);
        public GradientColorKey[] colorKeys { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
        public GradientAlphaKey[] alphaKeys { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
        public GradientMode mode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
        internal Color constantColor
        {
            get
            {
                Color color;
                this.INTERNAL_get_constantColor(out color);
                return color;
            }
            set
            {
                this.INTERNAL_set_constantColor(ref value);
            }
        }
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_constantColor(out Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_constantColor(ref Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetKeys(GradientColorKey[] colorKeys, GradientAlphaKey[] alphaKeys);
    }
}

