﻿namespace UnityEngine.Windows.Speech
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public sealed class GrammarRecognizer : PhraseRecognizer
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <GrammarFilePath>k__BackingField;

        public GrammarRecognizer(string grammarFilePath) : this(grammarFilePath, ConfidenceLevel.Medium)
        {
        }

        public GrammarRecognizer(string grammarFilePath, ConfidenceLevel minimumConfidence)
        {
            if (grammarFilePath == null)
            {
                throw new ArgumentNullException("grammarFilePath");
            }
            if (grammarFilePath.Length == 0)
            {
                throw new ArgumentException("Grammar file path cannot be empty.");
            }
            this.GrammarFilePath = grammarFilePath;
            base.m_Recognizer = base.CreateFromGrammarFile(grammarFilePath, minimumConfidence);
        }

        public string GrammarFilePath { get; private set; }
    }
}

