﻿namespace UnityEngine.Profiling
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.Scripting.APIUpdating;

    [MovedFrom("UnityEngine")]
    public sealed class Profiler
    {
        [MethodImpl(MethodImplOptions.InternalCall), Conditional("UNITY_EDITOR")]
        public static extern void AddFramesFromFile(string file);
        [Conditional("ENABLE_PROFILER")]
        public static void BeginSample(string name)
        {
            BeginSampleOnly(name);
        }

        [MethodImpl(MethodImplOptions.InternalCall), Conditional("ENABLE_PROFILER")]
        public static extern void BeginSample(string name, UnityEngine.Object targetObject);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void BeginSampleOnly(string name);
        [MethodImpl(MethodImplOptions.InternalCall), Conditional("ENABLE_PROFILER")]
        public static extern void EndSample();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern uint GetMonoHeapSize();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern uint GetMonoUsedSize();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern int GetRuntimeMemorySize(UnityEngine.Object o);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern Sampler GetSampler(string name);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern string[] GetSamplerNames();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern uint GetTempAllocatorSize();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern uint GetTotalAllocatedMemory();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern uint GetTotalReservedMemory();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern uint GetTotalUnusedReservedMemory();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void SetSamplersEnabled(bool enabled);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool SetTempAllocatorRequestedSize(uint size);

        public static bool enableBinaryLog { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string logFile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("maxNumberOfSamplesPerFrame is no longer needed, as the profiler buffers auto expand as needed")]
        public static int maxNumberOfSamplesPerFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool supported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static uint usedHeapSize { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

