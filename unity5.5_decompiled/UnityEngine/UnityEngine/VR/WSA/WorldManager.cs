﻿namespace UnityEngine.VR.WSA
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using UnityEngine.Scripting;

    public sealed class WorldManager
    {
        public static  event OnPositionalLocatorStateChangedDelegate OnPositionalLocatorStateChanged;

        public static IntPtr GetNativeISpatialCoordinateSystemPtr()
        {
            IntPtr ptr;
            INTERNAL_CALL_GetNativeISpatialCoordinateSystemPtr(out ptr);
            return ptr;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetNativeISpatialCoordinateSystemPtr(out IntPtr value);
        [RequiredByNativeCode]
        private static void Internal_TriggerPositionalLocatorStateChanged(PositionalLocatorState oldState, PositionalLocatorState newState)
        {
            if (OnPositionalLocatorStateChanged != null)
            {
                OnPositionalLocatorStateChanged(oldState, newState);
            }
        }

        public static PositionalLocatorState state { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public delegate void OnPositionalLocatorStateChangedDelegate(PositionalLocatorState oldState, PositionalLocatorState newState);
    }
}

