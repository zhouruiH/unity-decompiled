﻿namespace UnityEngine.VR.WSA.WebCam
{
    using System;
    using System.Runtime.CompilerServices;

    public static class WebCam
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern int GetWebCamModeState_Internal();

        public static WebCamMode Mode
        {
            get
            {
                return (WebCamMode) GetWebCamModeState_Internal();
            }
        }
    }
}

