﻿namespace UnityEngine.VR.WSA.WebCam
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Scripting;

    public sealed class VideoCapture : IDisposable
    {
        private static readonly long HR_SUCCESS = 0L;
        private IntPtr m_NativePtr;
        private static Resolution[] s_SupportedResolutions;

        private VideoCapture(IntPtr nativeCaptureObject)
        {
            this.m_NativePtr = nativeCaptureObject;
        }

        public static void CreateAsync(bool showHolograms, OnVideoCaptureResourceCreatedCallback onCreatedCallback)
        {
            if (onCreatedCallback == null)
            {
                throw new ArgumentNullException("onCreatedCallback");
            }
            Instantiate_Internal(showHolograms, onCreatedCallback);
        }

        public void Dispose()
        {
            if (this.m_NativePtr != IntPtr.Zero)
            {
                Dispose_Internal(this.m_NativePtr);
                this.m_NativePtr = IntPtr.Zero;
            }
            GC.SuppressFinalize(this);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void Dispose_Internal(IntPtr videoCaptureObj);
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        private static extern void DisposeThreaded_Internal(IntPtr videoCaptureObj);
        ~VideoCapture()
        {
            if (this.m_NativePtr != IntPtr.Zero)
            {
                DisposeThreaded_Internal(this.m_NativePtr);
                this.m_NativePtr = IntPtr.Zero;
            }
        }

        public static IEnumerable<float> GetSupportedFrameRatesForResolution(Resolution resolution)
        {
            return new float[0];
        }

        public IntPtr GetUnsafePointerToVideoDeviceController()
        {
            return IntPtr.Zero;
        }

        [ThreadAndSerializationSafe]
        private static IntPtr GetUnsafePointerToVideoDeviceController_Internal(IntPtr videoCaptureObj)
        {
            IntPtr ptr;
            INTERNAL_CALL_GetUnsafePointerToVideoDeviceController_Internal(videoCaptureObj, out ptr);
            return ptr;
        }

        private static IntPtr Instantiate_Internal(bool showHolograms, OnVideoCaptureResourceCreatedCallback onCreatedCallback)
        {
            IntPtr ptr;
            INTERNAL_CALL_Instantiate_Internal(showHolograms, onCreatedCallback, out ptr);
            return ptr;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetUnsafePointerToVideoDeviceController_Internal(IntPtr videoCaptureObj, out IntPtr value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_Instantiate_Internal(bool showHolograms, OnVideoCaptureResourceCreatedCallback onCreatedCallback, out IntPtr value);
        [RequiredByNativeCode]
        private static void InvokeOnCreatedVideoCaptureResourceDelegate(OnVideoCaptureResourceCreatedCallback callback, IntPtr nativePtr)
        {
            if (nativePtr == IntPtr.Zero)
            {
                callback(null);
            }
            else
            {
                callback(new VideoCapture(nativePtr));
            }
        }

        [RequiredByNativeCode]
        private static void InvokeOnStartedRecordingVideoToDiskDelegate(OnStartedRecordingVideoCallback callback, long hResult)
        {
            callback(MakeCaptureResult(hResult));
        }

        [RequiredByNativeCode]
        private static void InvokeOnStoppedRecordingVideoToDiskDelegate(OnStoppedRecordingVideoCallback callback, long hResult)
        {
            callback(MakeCaptureResult(hResult));
        }

        [RequiredByNativeCode]
        private static void InvokeOnVideoModeStartedDelegate(OnVideoModeStartedCallback callback, long hResult)
        {
            callback(MakeCaptureResult(hResult));
        }

        [RequiredByNativeCode]
        private static void InvokeOnVideoModeStoppedDelegate(OnVideoModeStoppedCallback callback, long hResult)
        {
            callback(MakeCaptureResult(hResult));
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern bool IsRecording_Internal(IntPtr videoCaptureObj);
        private static VideoCaptureResult MakeCaptureResult(long hResult)
        {
            CaptureResultType success;
            VideoCaptureResult result = new VideoCaptureResult();
            if (hResult == HR_SUCCESS)
            {
                success = CaptureResultType.Success;
            }
            else
            {
                success = CaptureResultType.UnknownError;
            }
            result.resultType = success;
            result.hResult = hResult;
            return result;
        }

        private static VideoCaptureResult MakeCaptureResult(CaptureResultType resultType, long hResult)
        {
            return new VideoCaptureResult { resultType = resultType, hResult = hResult };
        }

        public void StartRecordingAsync(string filename, OnStartedRecordingVideoCallback onStartedRecordingVideoCallback)
        {
            if (this.m_NativePtr == IntPtr.Zero)
            {
                throw new InvalidOperationException("You must create a Video Capture Object before recording video.");
            }
            if (onStartedRecordingVideoCallback == null)
            {
                throw new ArgumentNullException("onStartedRecordingVideoCallback");
            }
            if (string.IsNullOrEmpty(filename))
            {
                throw new ArgumentNullException("filename");
            }
            filename = filename.Replace("/", @"\");
            string directoryName = Path.GetDirectoryName(filename);
            if (!string.IsNullOrEmpty(directoryName) && !Directory.Exists(directoryName))
            {
                throw new ArgumentException("The specified directory does not exist.", "filename");
            }
            this.StartRecordingVideoToDisk_Internal(this.m_NativePtr, filename, onStartedRecordingVideoCallback);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void StartRecordingVideoToDisk_Internal(IntPtr videoCaptureObj, string filename, OnStartedRecordingVideoCallback onStartedRecordingVideoCallback);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void StartVideoMode_Internal(IntPtr videoCaptureObj, int audioState, OnVideoModeStartedCallback onVideoModeStartedCallback, float hologramOpacity, float frameRate, int cameraResolutionWidth, int cameraResolutionHeight, int pixelFormat);
        public void StartVideoModeAsync(CameraParameters setupParams, AudioState audioState, OnVideoModeStartedCallback onVideoModeStartedCallback)
        {
            if (this.m_NativePtr == IntPtr.Zero)
            {
                throw new InvalidOperationException("You must create a Video Capture Object before starting its video mode.");
            }
            if (onVideoModeStartedCallback == null)
            {
                throw new ArgumentNullException("onVideoModeStartedCallback");
            }
            if ((setupParams.cameraResolutionWidth == 0) || (setupParams.cameraResolutionHeight == 0))
            {
                throw new ArgumentOutOfRangeException("setupParams", "The camera resolution must be set to a supported resolution.");
            }
            if (setupParams.frameRate == 0f)
            {
                throw new ArgumentOutOfRangeException("setupParams", "The camera frame rate must be set to a supported recording frame rate.");
            }
            this.StartVideoMode_Internal(this.m_NativePtr, (int) audioState, onVideoModeStartedCallback, setupParams.hologramOpacity, setupParams.frameRate, setupParams.cameraResolutionWidth, setupParams.cameraResolutionHeight, (int) setupParams.pixelFormat);
        }

        public void StopRecordingAsync(OnStoppedRecordingVideoCallback onStoppedRecordingVideoCallback)
        {
            if (this.m_NativePtr == IntPtr.Zero)
            {
                throw new InvalidOperationException("You must create a Video Capture Object before recording video.");
            }
            if (onStoppedRecordingVideoCallback == null)
            {
                throw new ArgumentNullException("onStoppedRecordingVideoCallback");
            }
            this.StopRecordingVideoToDisk_Internal(this.m_NativePtr, onStoppedRecordingVideoCallback);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void StopRecordingVideoToDisk_Internal(IntPtr videoCaptureObj, OnStoppedRecordingVideoCallback onStoppedRecordingVideoCallback);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void StopVideoMode_Internal(IntPtr videoCaptureObj, OnVideoModeStoppedCallback onVideoModeStoppedCallback);
        public void StopVideoModeAsync(OnVideoModeStoppedCallback onVideoModeStoppedCallback)
        {
            if (this.m_NativePtr == IntPtr.Zero)
            {
                throw new InvalidOperationException("You must create a Video Capture Object before stopping its video mode.");
            }
            if (onVideoModeStoppedCallback == null)
            {
                throw new ArgumentNullException("onVideoModeStoppedCallback");
            }
            this.StopVideoMode_Internal(this.m_NativePtr, onVideoModeStoppedCallback);
        }

        public bool IsRecording
        {
            get
            {
                return false;
            }
        }

        public static IEnumerable<Resolution> SupportedResolutions
        {
            get
            {
                if (s_SupportedResolutions == null)
                {
                    s_SupportedResolutions = new Resolution[0];
                }
                return s_SupportedResolutions;
            }
        }

        public enum AudioState
        {
            MicAudio,
            ApplicationAudio,
            ApplicationAndMicAudio,
            None
        }

        public enum CaptureResultType
        {
            Success,
            UnknownError
        }

        public delegate void OnStartedRecordingVideoCallback(VideoCapture.VideoCaptureResult result);

        public delegate void OnStoppedRecordingVideoCallback(VideoCapture.VideoCaptureResult result);

        public delegate void OnVideoCaptureResourceCreatedCallback(VideoCapture captureObject);

        public delegate void OnVideoModeStartedCallback(VideoCapture.VideoCaptureResult result);

        public delegate void OnVideoModeStoppedCallback(VideoCapture.VideoCaptureResult result);

        [StructLayout(LayoutKind.Sequential)]
        public struct VideoCaptureResult
        {
            public VideoCapture.CaptureResultType resultType;
            public long hResult;
            public bool success
            {
                get
                {
                    return (this.resultType == VideoCapture.CaptureResultType.Success);
                }
            }
        }
    }
}

