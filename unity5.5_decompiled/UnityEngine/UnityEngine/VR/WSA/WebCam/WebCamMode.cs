﻿namespace UnityEngine.VR.WSA.WebCam
{
    using System;

    public enum WebCamMode
    {
        None,
        PhotoMode,
        VideoMode
    }
}

