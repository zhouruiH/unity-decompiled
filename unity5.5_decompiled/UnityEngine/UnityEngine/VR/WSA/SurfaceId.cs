﻿namespace UnityEngine.VR.WSA
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct SurfaceId
    {
        public int handle;
    }
}

