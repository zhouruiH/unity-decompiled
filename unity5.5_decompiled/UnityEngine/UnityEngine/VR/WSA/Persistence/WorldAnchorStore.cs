﻿namespace UnityEngine.VR.WSA.Persistence
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.Scripting;
    using UnityEngine.VR.WSA;

    public sealed class WorldAnchorStore
    {
        private static WorldAnchorStore s_Instance = null;

        private WorldAnchorStore(IntPtr nativePtr)
        {
        }

        public void Clear()
        {
        }

        public bool Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("id must not be null or empty", "id");
            }
            return false;
        }

        public string[] GetAllIds()
        {
            return new string[0];
        }

        public int GetAllIds(string[] ids)
        {
            if (ids == null)
            {
                throw new ArgumentNullException("ids");
            }
            return 0;
        }

        public static void GetAsync(GetAsyncDelegate onCompleted)
        {
        }

        [RequiredByNativeCode]
        private static void InvokeGetAsyncDelegate(GetAsyncDelegate handler, IntPtr nativePtr)
        {
            s_Instance = new WorldAnchorStore(nativePtr);
            handler(s_Instance);
        }

        public WorldAnchor Load(string id, GameObject go)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("id must not be null or empty", "id");
            }
            if (go == null)
            {
                throw new ArgumentNullException("anchor");
            }
            return null;
        }

        public bool Save(string id, WorldAnchor anchor)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("id must not be null or empty", "id");
            }
            if (anchor == null)
            {
                throw new ArgumentNullException("anchor");
            }
            return false;
        }

        public int anchorCount
        {
            get
            {
                return 0;
            }
        }

        public delegate void GetAsyncDelegate(WorldAnchorStore store);
    }
}

