﻿namespace UnityEngine.VR.WSA.Sharing
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.Scripting;
    using UnityEngine.VR.WSA;

    public sealed class WorldAnchorTransferBatch : IDisposable
    {
        public WorldAnchorTransferBatch()
        {
        }

        private WorldAnchorTransferBatch(IntPtr nativePtr)
        {
        }

        public bool AddWorldAnchor(string id, WorldAnchor anchor)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("id is null or empty!", "id");
            }
            if (anchor == null)
            {
                throw new ArgumentNullException("anchor");
            }
            return false;
        }

        public void Dispose()
        {
        }

        public static void ExportAsync(WorldAnchorTransferBatch transferBatch, SerializationDataAvailableDelegate onDataAvailable, SerializationCompleteDelegate onCompleted)
        {
            if (transferBatch == null)
            {
                throw new ArgumentNullException("transferBatch");
            }
            if (onDataAvailable == null)
            {
                throw new ArgumentNullException("onDataAvailable");
            }
            if (onCompleted == null)
            {
                throw new ArgumentNullException("onCompleted");
            }
            onCompleted(SerializationCompletionReason.NotSupported);
        }

        ~WorldAnchorTransferBatch()
        {
        }

        public string[] GetAllIds()
        {
            return new string[0];
        }

        public int GetAllIds(string[] ids)
        {
            if (ids == null)
            {
                throw new ArgumentNullException("ids");
            }
            return 0;
        }

        public static void ImportAsync(byte[] serializedData, DeserializationCompleteDelegate onComplete)
        {
            ImportAsync(serializedData, 0, serializedData.Length, onComplete);
        }

        public static void ImportAsync(byte[] serializedData, int offset, int length, DeserializationCompleteDelegate onComplete)
        {
            if (serializedData == null)
            {
                throw new ArgumentNullException("serializedData");
            }
            if (serializedData.Length < 1)
            {
                throw new ArgumentException("serializedData is empty!", "serializedData");
            }
            if ((offset + length) > serializedData.Length)
            {
                throw new ArgumentException("offset + length is greater that serializedData.Length!");
            }
            if (onComplete == null)
            {
                throw new ArgumentNullException("onComplete");
            }
            onComplete(SerializationCompletionReason.NotSupported, new WorldAnchorTransferBatch());
        }

        [RequiredByNativeCode]
        private static void InvokeWorldAnchorDeserializationCompleteDelegate(DeserializationCompleteDelegate onDeserializationComplete, SerializationCompletionReason completionReason, IntPtr nativePtr)
        {
            WorldAnchorTransferBatch deserializedTransferBatch = new WorldAnchorTransferBatch(nativePtr);
            onDeserializationComplete(completionReason, deserializedTransferBatch);
        }

        [RequiredByNativeCode]
        private static void InvokeWorldAnchorSerializationCompleteDelegate(SerializationCompleteDelegate onSerializationComplete, SerializationCompletionReason completionReason)
        {
            onSerializationComplete(completionReason);
        }

        [RequiredByNativeCode]
        private static void InvokeWorldAnchorSerializationDataAvailableDelegate(SerializationDataAvailableDelegate onSerializationDataAvailable, byte[] data)
        {
            onSerializationDataAvailable(data);
        }

        public WorldAnchor LockObject(string id, GameObject go)
        {
            return null;
        }

        public int anchorCount
        {
            get
            {
                return 0;
            }
        }

        public delegate void DeserializationCompleteDelegate(SerializationCompletionReason completionReason, WorldAnchorTransferBatch deserializedTransferBatch);

        public delegate void SerializationCompleteDelegate(SerializationCompletionReason completionReason);

        public delegate void SerializationDataAvailableDelegate(byte[] data);
    }
}

