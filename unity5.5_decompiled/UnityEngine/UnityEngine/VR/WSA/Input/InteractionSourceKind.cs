﻿namespace UnityEngine.VR.WSA.Input
{
    using System;

    public enum InteractionSourceKind
    {
        Other,
        Hand,
        Voice,
        Controller
    }
}

