﻿namespace UnityEngine.VR.WSA.Input
{
    using System;

    public enum GestureSettings
    {
        DoubleTap = 2,
        Hold = 4,
        ManipulationTranslate = 8,
        NavigationRailsX = 0x80,
        NavigationRailsY = 0x100,
        NavigationRailsZ = 0x200,
        NavigationX = 0x10,
        NavigationY = 0x20,
        NavigationZ = 0x40,
        None = 0,
        Tap = 1
    }
}

