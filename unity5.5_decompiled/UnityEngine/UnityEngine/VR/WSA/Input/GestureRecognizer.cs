﻿namespace UnityEngine.VR.WSA.Input
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using UnityEngine;
    using UnityEngine.Scripting;

    public sealed class GestureRecognizer : IDisposable
    {
        private IntPtr m_Recognizer;

        public event GestureErrorDelegate GestureErrorEvent;

        public event HoldCanceledEventDelegate HoldCanceledEvent;

        public event HoldCompletedEventDelegate HoldCompletedEvent;

        public event HoldStartedEventDelegate HoldStartedEvent;

        public event ManipulationCanceledEventDelegate ManipulationCanceledEvent;

        public event ManipulationCompletedEventDelegate ManipulationCompletedEvent;

        public event ManipulationStartedEventDelegate ManipulationStartedEvent;

        public event ManipulationUpdatedEventDelegate ManipulationUpdatedEvent;

        public event NavigationCanceledEventDelegate NavigationCanceledEvent;

        public event NavigationCompletedEventDelegate NavigationCompletedEvent;

        public event NavigationStartedEventDelegate NavigationStartedEvent;

        public event NavigationUpdatedEventDelegate NavigationUpdatedEvent;

        public event RecognitionEndedEventDelegate RecognitionEndedEvent;

        public event RecognitionStartedEventDelegate RecognitionStartedEvent;

        public event TappedEventDelegate TappedEvent;

        public GestureRecognizer()
        {
            this.m_Recognizer = this.Internal_Create();
        }

        public void CancelGestures()
        {
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void Destroy(IntPtr recognizer);
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        private static extern void DestroyThreaded(IntPtr recognizer);
        public void Dispose()
        {
            if (this.m_Recognizer != IntPtr.Zero)
            {
                Destroy(this.m_Recognizer);
                this.m_Recognizer = IntPtr.Zero;
            }
            GC.SuppressFinalize(this);
        }

        ~GestureRecognizer()
        {
            if (this.m_Recognizer != IntPtr.Zero)
            {
                DestroyThreaded(this.m_Recognizer);
                this.m_Recognizer = IntPtr.Zero;
                GC.SuppressFinalize(this);
            }
        }

        public GestureSettings GetRecognizableGestures()
        {
            return GestureSettings.None;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_Internal_Create(GestureRecognizer self, out IntPtr value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void Internal_CancelGestures(IntPtr recognizer);
        private IntPtr Internal_Create()
        {
            IntPtr ptr;
            INTERNAL_CALL_Internal_Create(this, out ptr);
            return ptr;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern int Internal_GetRecognizableGestures(IntPtr recognizer);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern bool Internal_IsCapturingGestures(IntPtr recognizer);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern int Internal_SetRecognizableGestures(IntPtr recognizer, int newMaskValue);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void Internal_StartCapturingGestures(IntPtr recognizer);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void Internal_StopCapturingGestures(IntPtr recognizer);
        [RequiredByNativeCode]
        private void InvokeErrorEvent(string error, int hresult)
        {
            GestureErrorDelegate gestureErrorEvent = this.GestureErrorEvent;
            if (gestureErrorEvent != null)
            {
                gestureErrorEvent(error, hresult);
            }
        }

        [RequiredByNativeCode]
        private void InvokeHoldEvent(GestureEventType eventType, InteractionSourceKind source, Ray headRay)
        {
            switch (eventType)
            {
                case GestureEventType.HoldCanceled:
                {
                    HoldCanceledEventDelegate holdCanceledEvent = this.HoldCanceledEvent;
                    if (holdCanceledEvent != null)
                    {
                        holdCanceledEvent(source, headRay);
                    }
                    break;
                }
                case GestureEventType.HoldCompleted:
                {
                    HoldCompletedEventDelegate holdCompletedEvent = this.HoldCompletedEvent;
                    if (holdCompletedEvent != null)
                    {
                        holdCompletedEvent(source, headRay);
                    }
                    break;
                }
                case GestureEventType.HoldStarted:
                {
                    HoldStartedEventDelegate holdStartedEvent = this.HoldStartedEvent;
                    if (holdStartedEvent != null)
                    {
                        holdStartedEvent(source, headRay);
                    }
                    break;
                }
                default:
                    throw new ArgumentException("InvokeHoldEvent: Invalid GestureEventType");
            }
        }

        [RequiredByNativeCode]
        private void InvokeManipulationEvent(GestureEventType eventType, InteractionSourceKind source, Vector3 position, Ray headRay)
        {
            switch (eventType)
            {
                case GestureEventType.ManipulationCanceled:
                {
                    ManipulationCanceledEventDelegate manipulationCanceledEvent = this.ManipulationCanceledEvent;
                    if (manipulationCanceledEvent != null)
                    {
                        manipulationCanceledEvent(source, position, headRay);
                    }
                    break;
                }
                case GestureEventType.ManipulationCompleted:
                {
                    ManipulationCompletedEventDelegate manipulationCompletedEvent = this.ManipulationCompletedEvent;
                    if (manipulationCompletedEvent != null)
                    {
                        manipulationCompletedEvent(source, position, headRay);
                    }
                    break;
                }
                case GestureEventType.ManipulationStarted:
                {
                    ManipulationStartedEventDelegate manipulationStartedEvent = this.ManipulationStartedEvent;
                    if (manipulationStartedEvent != null)
                    {
                        manipulationStartedEvent(source, position, headRay);
                    }
                    break;
                }
                case GestureEventType.ManipulationUpdated:
                {
                    ManipulationUpdatedEventDelegate manipulationUpdatedEvent = this.ManipulationUpdatedEvent;
                    if (manipulationUpdatedEvent != null)
                    {
                        manipulationUpdatedEvent(source, position, headRay);
                    }
                    break;
                }
                default:
                    throw new ArgumentException("InvokeManipulationEvent: Invalid GestureEventType");
            }
        }

        [RequiredByNativeCode]
        private void InvokeNavigationEvent(GestureEventType eventType, InteractionSourceKind source, Vector3 relativePosition, Ray headRay)
        {
            switch (eventType)
            {
                case GestureEventType.NavigationCanceled:
                {
                    NavigationCanceledEventDelegate navigationCanceledEvent = this.NavigationCanceledEvent;
                    if (navigationCanceledEvent != null)
                    {
                        navigationCanceledEvent(source, relativePosition, headRay);
                    }
                    break;
                }
                case GestureEventType.NavigationCompleted:
                {
                    NavigationCompletedEventDelegate navigationCompletedEvent = this.NavigationCompletedEvent;
                    if (navigationCompletedEvent != null)
                    {
                        navigationCompletedEvent(source, relativePosition, headRay);
                    }
                    break;
                }
                case GestureEventType.NavigationStarted:
                {
                    NavigationStartedEventDelegate navigationStartedEvent = this.NavigationStartedEvent;
                    if (navigationStartedEvent != null)
                    {
                        navigationStartedEvent(source, relativePosition, headRay);
                    }
                    break;
                }
                case GestureEventType.NavigationUpdated:
                {
                    NavigationUpdatedEventDelegate navigationUpdatedEvent = this.NavigationUpdatedEvent;
                    if (navigationUpdatedEvent != null)
                    {
                        navigationUpdatedEvent(source, relativePosition, headRay);
                    }
                    break;
                }
                default:
                    throw new ArgumentException("InvokeNavigationEvent: Invalid GestureEventType");
            }
        }

        [RequiredByNativeCode]
        private void InvokeRecognitionEvent(GestureEventType eventType, InteractionSourceKind source, Ray headRay)
        {
            if (eventType != GestureEventType.RecognitionEnded)
            {
                if (eventType != GestureEventType.RecognitionStarted)
                {
                    throw new ArgumentException("InvokeRecognitionEvent: Invalid GestureEventType");
                }
            }
            else
            {
                RecognitionEndedEventDelegate recognitionEndedEvent = this.RecognitionEndedEvent;
                if (recognitionEndedEvent != null)
                {
                    recognitionEndedEvent(source, headRay);
                }
                return;
            }
            RecognitionStartedEventDelegate recognitionStartedEvent = this.RecognitionStartedEvent;
            if (recognitionStartedEvent != null)
            {
                recognitionStartedEvent(source, headRay);
            }
        }

        [RequiredByNativeCode]
        private void InvokeTapEvent(InteractionSourceKind source, Ray headRay, int tapCount)
        {
            TappedEventDelegate tappedEvent = this.TappedEvent;
            if (tappedEvent != null)
            {
                tappedEvent(source, tapCount, headRay);
            }
        }

        public bool IsCapturingGestures()
        {
            return false;
        }

        public GestureSettings SetRecognizableGestures(GestureSettings newMaskValue)
        {
            return GestureSettings.None;
        }

        public void StartCapturingGestures()
        {
        }

        public void StopCapturingGestures()
        {
        }

        public delegate void GestureErrorDelegate([MarshalAs(UnmanagedType.LPStr)] string error, int hresult);

        private enum GestureEventType
        {
            InteractionDetected,
            HoldCanceled,
            HoldCompleted,
            HoldStarted,
            TapDetected,
            ManipulationCanceled,
            ManipulationCompleted,
            ManipulationStarted,
            ManipulationUpdated,
            NavigationCanceled,
            NavigationCompleted,
            NavigationStarted,
            NavigationUpdated,
            RecognitionStarted,
            RecognitionEnded
        }

        public delegate void HoldCanceledEventDelegate(InteractionSourceKind source, Ray headRay);

        public delegate void HoldCompletedEventDelegate(InteractionSourceKind source, Ray headRay);

        public delegate void HoldStartedEventDelegate(InteractionSourceKind source, Ray headRay);

        public delegate void ManipulationCanceledEventDelegate(InteractionSourceKind source, Vector3 cumulativeDelta, Ray headRay);

        public delegate void ManipulationCompletedEventDelegate(InteractionSourceKind source, Vector3 cumulativeDelta, Ray headRay);

        public delegate void ManipulationStartedEventDelegate(InteractionSourceKind source, Vector3 cumulativeDelta, Ray headRay);

        public delegate void ManipulationUpdatedEventDelegate(InteractionSourceKind source, Vector3 cumulativeDelta, Ray headRay);

        public delegate void NavigationCanceledEventDelegate(InteractionSourceKind source, Vector3 normalizedOffset, Ray headRay);

        public delegate void NavigationCompletedEventDelegate(InteractionSourceKind source, Vector3 normalizedOffset, Ray headRay);

        public delegate void NavigationStartedEventDelegate(InteractionSourceKind source, Vector3 normalizedOffset, Ray headRay);

        public delegate void NavigationUpdatedEventDelegate(InteractionSourceKind source, Vector3 normalizedOffset, Ray headRay);

        public delegate void RecognitionEndedEventDelegate(InteractionSourceKind source, Ray headRay);

        public delegate void RecognitionStartedEventDelegate(InteractionSourceKind source, Ray headRay);

        public delegate void TappedEventDelegate(InteractionSourceKind source, int tapCount, Ray headRay);
    }
}

