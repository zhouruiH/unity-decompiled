﻿namespace UnityEngine.VR
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class VRDevice
    {
        public static IntPtr GetNativePtr()
        {
            IntPtr ptr;
            INTERNAL_CALL_GetNativePtr(out ptr);
            return ptr;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetNativePtr(out IntPtr value);

        [Obsolete("family is deprecated.  Use VRSettings.loadedDeviceName instead.")]
        public static string family { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool isPresent { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static string model { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static float refreshRate { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

