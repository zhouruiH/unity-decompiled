﻿namespace UnityEngine.VR
{
    using System;
    using System.Runtime.CompilerServices;

    public static class VRStats
    {
        public static float gpuTimeLastFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

