﻿namespace UnityEngine
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Security.Cryptography;
    using UnityEngine.Internal;
    using UnityEngine.Scripting;

    public sealed class Security
    {
        private static readonly string kSignatureExtension = ".signature";

        [Obsolete("This was an internal method which is no longer used", true)]
        public static Assembly LoadAndVerifyAssembly(byte[] assemblyData)
        {
            return null;
        }

        [Obsolete("This was an internal method which is no longer used", true)]
        public static Assembly LoadAndVerifyAssembly(byte[] assemblyData, string authorizationKey)
        {
            return null;
        }

        [ExcludeFromDocs, Obsolete("Security.PrefetchSocketPolicy is no longer supported, since the Unity Web Player is no longer supported by Unity.")]
        public static bool PrefetchSocketPolicy(string ip, int atPort)
        {
            int timeout = 0xbb8;
            return PrefetchSocketPolicy(ip, atPort, timeout);
        }

        [Obsolete("Security.PrefetchSocketPolicy is no longer supported, since the Unity Web Player is no longer supported by Unity.")]
        public static bool PrefetchSocketPolicy(string ip, int atPort, [DefaultValue("3000")] int timeout)
        {
            return false;
        }

        [RequiredByNativeCode]
        internal static bool VerifySignature(string file, byte[] publicKey)
        {
            try
            {
                string path = file + kSignatureExtension;
                if (!File.Exists(path))
                {
                    return false;
                }
                using (RSACryptoServiceProvider provider = new RSACryptoServiceProvider())
                {
                    provider.ImportCspBlob(publicKey);
                    using (SHA1CryptoServiceProvider provider2 = new SHA1CryptoServiceProvider())
                    {
                        return provider.VerifyData(File.ReadAllBytes(file), provider2, File.ReadAllBytes(path));
                    }
                }
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
            }
            return false;
        }
    }
}

