﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class ParticleSystemRenderer : Renderer
    {
        public bool AreVertexStreamsEnabled(ParticleSystemVertexStreams streams)
        {
            return (this.Internal_GetEnabledVertexStreams(streams) == streams);
        }

        public void DisableVertexStreams(ParticleSystemVertexStreams streams)
        {
            this.Internal_SetVertexStreams(streams, false);
        }

        public void EnableVertexStreams(ParticleSystemVertexStreams streams)
        {
            this.Internal_SetVertexStreams(streams, true);
        }

        public ParticleSystemVertexStreams GetEnabledVertexStreams(ParticleSystemVertexStreams streams)
        {
            return this.Internal_GetEnabledVertexStreams(streams);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern int GetMeshes(Mesh[] meshes);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_pivot(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern ParticleSystemVertexStreams Internal_GetEnabledVertexStreams(ParticleSystemVertexStreams streams);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern int Internal_GetMeshCount();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_pivot(ref Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void Internal_SetVertexStreams(ParticleSystemVertexStreams streams, bool enabled);
        public void SetMeshes(Mesh[] meshes)
        {
            this.SetMeshes(meshes, meshes.Length);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetMeshes(Mesh[] meshes, int size);

        public ParticleSystemRenderSpace alignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float cameraVelocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal bool editorEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float lengthScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float maxParticleSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Mesh mesh { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int meshCount
        {
            get
            {
                return this.Internal_GetMeshCount();
            }
        }

        public float minParticleSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float normalDirection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Vector3 pivot
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_pivot(out vector);
                return vector;
            }
            set
            {
                this.INTERNAL_set_pivot(ref value);
            }
        }

        public ParticleSystemRenderMode renderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float sortingFudge { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ParticleSystemSortMode sortMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Material trailMaterial { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float velocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

