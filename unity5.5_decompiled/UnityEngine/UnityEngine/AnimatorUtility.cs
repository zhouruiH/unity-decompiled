﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class AnimatorUtility
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void DeoptimizeTransformHierarchy(GameObject go);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void OptimizeTransformHierarchy(GameObject go, string[] exposedTransforms);
    }
}

