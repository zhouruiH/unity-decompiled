﻿namespace UnityEngine.iOS
{
    using System;
    using System.Collections;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.Scripting;

    [RequiredByNativeCode]
    public sealed class RemoteNotification
    {
        private IntPtr notificationWrapper;

        private RemoteNotification()
        {
        }

        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        private extern void Destroy();
        ~RemoteNotification()
        {
            this.Destroy();
        }

        public string alertBody { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public int applicationIconBadgeNumber { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool hasAction { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public string soundName { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public IDictionary userInfo { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

