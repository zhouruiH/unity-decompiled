﻿namespace UnityEngine
{
    using System;

    public enum LineTextureMode
    {
        Stretch,
        Tile
    }
}

