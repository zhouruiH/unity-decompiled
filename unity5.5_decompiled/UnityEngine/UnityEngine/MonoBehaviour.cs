﻿namespace UnityEngine
{
    using System;
    using System.Collections;
    using System.Runtime.CompilerServices;
    using UnityEngine.Internal;
    using UnityEngine.Scripting;

    [RequiredByNativeCode]
    public class MonoBehaviour : Behaviour
    {
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        public extern MonoBehaviour();
        public void CancelInvoke()
        {
            this.Internal_CancelInvokeAll();
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void CancelInvoke(string methodName);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void Internal_CancelInvokeAll();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern bool Internal_IsInvokingAll();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void Invoke(string methodName, float time);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void InvokeRepeating(string methodName, float time, float repeatRate);
        public bool IsInvoking()
        {
            return this.Internal_IsInvokingAll();
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern bool IsInvoking(string methodName);
        public static void print(object message)
        {
            Debug.Log(message);
        }

        public Coroutine StartCoroutine(IEnumerator routine)
        {
            return this.StartCoroutine_Auto_Internal(routine);
        }

        [ExcludeFromDocs]
        public Coroutine StartCoroutine(string methodName)
        {
            object obj2 = null;
            return this.StartCoroutine(methodName, obj2);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern Coroutine StartCoroutine(string methodName, [DefaultValue("null")] object value);
        [Obsolete("StartCoroutine_Auto has been deprecated. Use StartCoroutine instead (UnityUpgradable) -> StartCoroutine([mscorlib] System.Collections.IEnumerator)", false)]
        public Coroutine StartCoroutine_Auto(IEnumerator routine)
        {
            return this.StartCoroutine(routine);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern Coroutine StartCoroutine_Auto_Internal(IEnumerator routine);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void StopAllCoroutines();
        public void StopCoroutine(IEnumerator routine)
        {
            this.StopCoroutineViaEnumerator_Auto(routine);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void StopCoroutine(string methodName);
        public void StopCoroutine(Coroutine routine)
        {
            this.StopCoroutine_Auto(routine);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void StopCoroutine_Auto(Coroutine routine);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void StopCoroutineViaEnumerator_Auto(IEnumerator routine);

        public bool runInEditMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool useGUILayout { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

