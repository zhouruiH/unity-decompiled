﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class LineRenderer : Renderer
    {
        public Vector3 GetPosition(int index)
        {
            Vector3 vector;
            INTERNAL_CALL_GetPosition(this, index, out vector);
            return vector;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern int GetPositions(Vector3[] positions);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetPosition(LineRenderer self, int index, out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_SetPosition(LineRenderer self, int index, ref Vector3 position);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_endColor(out Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_startColor(out Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_endColor(ref Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_startColor(ref Color value);
        [Obsolete("SetColors has been deprecated. Please use the startColor, endColor, or colorGradient properties instead.")]
        public void SetColors(Color start, Color end)
        {
            this.startColor = start;
            this.endColor = end;
        }

        public void SetPosition(int index, Vector3 position)
        {
            INTERNAL_CALL_SetPosition(this, index, ref position);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetPositions(Vector3[] positions);
        [Obsolete("SetVertexCount has been deprecated. Please use the numPositions property instead.")]
        public void SetVertexCount(int count)
        {
            this.numPositions = count;
        }

        [Obsolete("SetWidth has been deprecated. Please use the startWidth, endWidth, or widthCurve properties instead.")]
        public void SetWidth(float start, float end)
        {
            this.startWidth = start;
            this.endWidth = end;
        }

        public Gradient colorGradient { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Color endColor
        {
            get
            {
                Color color;
                this.INTERNAL_get_endColor(out color);
                return color;
            }
            set
            {
                this.INTERNAL_set_endColor(ref value);
            }
        }

        public float endWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int numCapVertices { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int numCornerVertices { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int numPositions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Color startColor
        {
            get
            {
                Color color;
                this.INTERNAL_get_startColor(out color);
                return color;
            }
            set
            {
                this.INTERNAL_set_startColor(ref value);
            }
        }

        public float startWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public LineTextureMode textureMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool useWorldSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public AnimationCurve widthCurve { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float widthMultiplier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

