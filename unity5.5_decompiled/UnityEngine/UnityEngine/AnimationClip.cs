﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class AnimationClip : Motion
    {
        public AnimationClip()
        {
            Internal_CreateAnimationClip(this);
        }

        public void AddEvent(AnimationEvent evt)
        {
            if (evt == null)
            {
                throw new ArgumentNullException("evt");
            }
            this.AddEventInternal(evt);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void AddEventInternal(object evt);
        public void ClearCurves()
        {
            INTERNAL_CALL_ClearCurves(this);
        }

        public void EnsureQuaternionContinuity()
        {
            INTERNAL_CALL_EnsureQuaternionContinuity(this);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern Array GetEventsInternal();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_ClearCurves(AnimationClip self);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_EnsureQuaternionContinuity(AnimationClip self);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void Internal_CreateAnimationClip([Writable] AnimationClip self);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_localBounds(out Bounds value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_localBounds(ref Bounds value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SampleAnimation(GameObject go, float time);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetCurve(string relativePath, System.Type type, string propertyName, AnimationCurve curve);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void SetEventsInternal(Array value);

        public AnimationEvent[] events
        {
            get
            {
                return (AnimationEvent[]) this.GetEventsInternal();
            }
            set
            {
                this.SetEventsInternal(value);
            }
        }

        public float frameRate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool humanMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool legacy { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float length { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public Bounds localBounds
        {
            get
            {
                Bounds bounds;
                this.INTERNAL_get_localBounds(out bounds);
                return bounds;
            }
            set
            {
                this.INTERNAL_set_localBounds(ref value);
            }
        }

        internal float startTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal float stopTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public WrapMode wrapMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

