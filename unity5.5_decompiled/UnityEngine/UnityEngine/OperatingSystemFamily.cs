﻿namespace UnityEngine
{
    using System;

    public enum OperatingSystemFamily
    {
        Other,
        MacOSX,
        Windows,
        Linux
    }
}

