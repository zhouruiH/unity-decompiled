﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class TerrainCollider : Collider
    {
        public TerrainData terrainData { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

