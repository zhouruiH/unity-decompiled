﻿namespace UnityEngine
{
    using System;

    public enum MotionVectorGenerationMode
    {
        Camera,
        Object,
        ForceNoMotion
    }
}

