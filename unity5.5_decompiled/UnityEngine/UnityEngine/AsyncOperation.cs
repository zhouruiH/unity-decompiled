﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.Scripting;

    [StructLayout(LayoutKind.Sequential), RequiredByNativeCode]
    public class AsyncOperation : YieldInstruction
    {
        internal IntPtr m_Ptr;
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        private extern void InternalDestroy();
        ~AsyncOperation()
        {
            this.InternalDestroy();
        }

        public bool isDone { [MethodImpl(MethodImplOptions.InternalCall)] get; }
        public float progress { [MethodImpl(MethodImplOptions.InternalCall)] get; }
        public int priority { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
        public bool allowSceneActivation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

