﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class Time
    {
        public static int captureFramerate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float deltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static float fixedDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float fixedTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static int frameCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static float maximumDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float maximumParticleDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float realtimeSinceStartup { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static int renderedFrameCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static float smoothDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static float time { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static float timeScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float timeSinceLevelLoad { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static float unscaledDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static float unscaledTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

