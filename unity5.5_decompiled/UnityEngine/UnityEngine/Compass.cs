﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class Compass
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_rawVector(out Vector3 value);

        public bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float headingAccuracy { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public float magneticHeading { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public Vector3 rawVector
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_rawVector(out vector);
                return vector;
            }
        }

        public double timestamp { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public float trueHeading { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

