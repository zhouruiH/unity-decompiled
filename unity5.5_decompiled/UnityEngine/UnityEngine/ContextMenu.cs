﻿namespace UnityEngine
{
    using System;

    [AttributeUsage(AttributeTargets.Method, AllowMultiple=true)]
    public sealed class ContextMenu : Attribute
    {
        public readonly string menuItem;
        public readonly int priority;
        public readonly bool validate;

        public ContextMenu(string itemName) : this(itemName, false)
        {
        }

        public ContextMenu(string itemName, bool isValidateFunction) : this(itemName, isValidateFunction, 0xf4240)
        {
        }

        public ContextMenu(string itemName, bool isValidateFunction, int priority)
        {
            this.menuItem = itemName;
            this.validate = isValidateFunction;
            this.priority = priority;
        }
    }
}

