﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using UnityEngine.Internal;
    using UnityEngine.Scripting;

    public sealed class RemoteSettings
    {
        public static  event UpdatedEventHandler Updated;

        [RequiredByNativeCode]
        public static void CallOnUpdate()
        {
            UpdatedEventHandler updated = Updated;
            if (updated != null)
            {
                updated();
            }
        }

        [ExcludeFromDocs]
        public static bool GetBool(string key)
        {
            bool defaultValue = false;
            return GetBool(key, defaultValue);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool GetBool(string key, [DefaultValue("false")] bool defaultValue);
        [ExcludeFromDocs]
        public static float GetFloat(string key)
        {
            float defaultValue = 0f;
            return GetFloat(key, defaultValue);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern float GetFloat(string key, [DefaultValue("0.0F")] float defaultValue);
        [ExcludeFromDocs]
        public static int GetInt(string key)
        {
            int defaultValue = 0;
            return GetInt(key, defaultValue);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern int GetInt(string key, [DefaultValue("0")] int defaultValue);
        [ExcludeFromDocs]
        public static string GetString(string key)
        {
            string defaultValue = "";
            return GetString(key, defaultValue);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern string GetString(string key, [DefaultValue("\"\"")] string defaultValue);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool HasKey(string key);

        public delegate void UpdatedEventHandler();
    }
}

