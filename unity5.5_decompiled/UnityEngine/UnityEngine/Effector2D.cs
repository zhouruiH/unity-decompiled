﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public class Effector2D : Behaviour
    {
        public int colliderMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal bool designedForNonTrigger { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal bool designedForTrigger { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal bool requiresCollider { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool useColliderMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

