﻿namespace UnityEngine
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine.Scripting;

    [StructLayout(LayoutKind.Sequential), RequiredByNativeCode]
    public struct SkeletonBone
    {
        public string name;
        internal string parentName;
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;
        [Obsolete("transformModified is no longer used and has been deprecated.", true)]
        public int transformModified
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
    }
}

