﻿namespace UnityEngine
{
    using System;

    public enum GradientMode
    {
        Blend,
        Fixed
    }
}

