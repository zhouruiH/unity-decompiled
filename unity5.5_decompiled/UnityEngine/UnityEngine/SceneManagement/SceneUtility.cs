﻿namespace UnityEngine.SceneManagement
{
    using System;
    using System.Runtime.CompilerServices;

    public static class SceneUtility
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern int GetBuildIndexByScenePath(string scenePath);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern string GetScenePathByBuildIndex(int buildIndex);
    }
}

