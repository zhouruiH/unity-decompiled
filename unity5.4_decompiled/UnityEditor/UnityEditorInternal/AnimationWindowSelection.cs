﻿namespace UnityEditorInternal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    internal class AnimationWindowSelection
    {
        [CompilerGenerated]
        private static Action <>f__am$cache6;
        private bool m_BatchOperations;
        private List<AnimationWindowCurve> m_CurvesCache;
        [SerializeField]
        private bool m_Locked;
        [SerializeField]
        private List<AnimationWindowSelectionItem> m_Selection = new List<AnimationWindowSelectionItem>();
        private bool m_SelectionChanged;
        [NonSerialized]
        public Action onSelectionChanged;

        public AnimationWindowSelection()
        {
            if (<>f__am$cache6 == null)
            {
                <>f__am$cache6 = new Action(AnimationWindowSelection.<AnimationWindowSelection>m__8C);
            }
            this.onSelectionChanged = (Action) Delegate.Combine(this.onSelectionChanged, <>f__am$cache6);
        }

        [CompilerGenerated]
        private static void <AnimationWindowSelection>m__8C()
        {
        }

        public void Add(AnimationWindowSelectionItem newItem)
        {
            if (!this.locked && !this.m_Selection.Contains(newItem))
            {
                this.m_Selection.Add(newItem);
                this.Notify();
            }
        }

        public void BeginOperations()
        {
            if (this.m_BatchOperations)
            {
                Debug.LogWarning("AnimationWindowSelection: Already inside a BeginOperations/EndOperations block");
            }
            else
            {
                this.m_BatchOperations = true;
                this.m_SelectionChanged = false;
            }
        }

        public void Clear()
        {
            if (!this.locked && (this.m_Selection.Count > 0))
            {
                foreach (AnimationWindowSelectionItem item in this.m_Selection)
                {
                    Object.DestroyImmediate(item);
                }
                this.m_Selection.Clear();
                this.Notify();
            }
        }

        public void ClearCache()
        {
            this.m_CurvesCache = null;
        }

        public void EndOperations()
        {
            if (this.m_BatchOperations)
            {
                if (this.m_SelectionChanged)
                {
                    this.onSelectionChanged();
                }
                this.m_SelectionChanged = false;
                this.m_BatchOperations = false;
            }
        }

        public bool Exists(Predicate<AnimationWindowSelectionItem> predicate)
        {
            return this.m_Selection.Exists(predicate);
        }

        public bool Exists(AnimationWindowSelectionItem itemToFind)
        {
            return this.m_Selection.Contains(itemToFind);
        }

        public AnimationWindowSelectionItem Find(Predicate<AnimationWindowSelectionItem> predicate)
        {
            return this.m_Selection.Find(predicate);
        }

        public AnimationWindowSelectionItem First()
        {
            return this.m_Selection.First<AnimationWindowSelectionItem>();
        }

        public int GetRefreshHash()
        {
            int num = 0;
            foreach (AnimationWindowSelectionItem item in this.m_Selection)
            {
                num ^= item.GetRefreshHash();
            }
            return num;
        }

        public void Notify()
        {
            if (this.m_BatchOperations)
            {
                this.m_SelectionChanged = true;
            }
            else
            {
                this.onSelectionChanged();
            }
        }

        public void RangeAdd(AnimationWindowSelectionItem[] newItemArray)
        {
            if (!this.locked)
            {
                bool flag = false;
                foreach (AnimationWindowSelectionItem item in newItemArray)
                {
                    if (!this.m_Selection.Contains(item))
                    {
                        this.m_Selection.Add(item);
                        flag = true;
                    }
                }
                if (flag)
                {
                    this.Notify();
                }
            }
        }

        public void Refresh()
        {
            this.ClearCache();
            foreach (AnimationWindowSelectionItem item in this.m_Selection)
            {
                item.ClearCache();
            }
        }

        public void Set(AnimationWindowSelectionItem newItem)
        {
            if (!this.locked)
            {
                this.BeginOperations();
                this.Clear();
                this.Add(newItem);
                this.EndOperations();
            }
        }

        public AnimationWindowSelectionItem[] ToArray()
        {
            return this.m_Selection.ToArray();
        }

        public void UpdateClip(AnimationWindowSelectionItem itemToUpdate, AnimationClip newClip)
        {
            if (this.m_Selection.Contains(itemToUpdate))
            {
                itemToUpdate.animationClip = newClip;
                this.Notify();
            }
        }

        public void UpdateTimeOffset(AnimationWindowSelectionItem itemToUpdate, float timeOffset)
        {
            if (this.m_Selection.Contains(itemToUpdate))
            {
                itemToUpdate.timeOffset = timeOffset;
            }
        }

        public int count
        {
            get
            {
                return this.m_Selection.Count;
            }
        }

        public List<AnimationWindowCurve> curves
        {
            get
            {
                if (this.m_CurvesCache == null)
                {
                    this.m_CurvesCache = new List<AnimationWindowCurve>();
                    foreach (AnimationWindowSelectionItem item in this.m_Selection)
                    {
                        this.m_CurvesCache.AddRange(item.curves);
                    }
                }
                return this.m_CurvesCache;
            }
        }

        public bool disabled
        {
            get
            {
                if (this.m_Selection.Count > 0)
                {
                    foreach (AnimationWindowSelectionItem item in this.m_Selection)
                    {
                        if (item.animationClip != null)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        public bool locked
        {
            get
            {
                return this.m_Locked;
            }
            set
            {
                if (!this.disabled)
                {
                    this.m_Locked = value;
                }
            }
        }
    }
}

