﻿namespace UnityEditorInternal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEngine;

    [Serializable]
    internal class AnimationWindowState : ScriptableObject, IPlayHead, IAnimationRecordingState
    {
        [SerializeField]
        public AnimEditor animEditor;
        [NonSerialized]
        public AnimationWindowHierarchyDataSource hierarchyData;
        [SerializeField]
        public AnimationWindowHierarchyState hierarchyState;
        public const float kDefaultFrameRate = 60f;
        private List<AnimationWindowCurve> m_ActiveCurvesCache;
        private List<CurveWrapper> m_ActiveCurveWrappersCache;
        private AnimationWindowKeyframe m_ActiveKeyframeCache;
        [SerializeField]
        private int m_ActiveKeyframeHash;
        [SerializeField]
        private int m_CurrentFrame;
        [SerializeField]
        private float m_CurrentTime;
        private List<DopeLine> m_dopelinesCache;
        [SerializeField]
        private float m_FrameRate = 60f;
        private EditorCurveBinding? m_lastAddedCurveBinding;
        private HashSet<int> m_ModifiedCurves = new HashSet<int>();
        [SerializeField]
        private AnimationWindowPolicy m_Policy;
        private int m_PreviousRefreshHash;
        private AnimationRecordMode m_Recording;
        private RefreshType m_Refresh;
        [SerializeField]
        private HashSet<int> m_SelectedKeyHashes;
        private List<AnimationWindowKeyframe> m_SelectedKeysCache;
        [SerializeField]
        private AnimationWindowSelection m_Selection;
        [SerializeField]
        private TimeArea m_TimeArea;
        public Action<float> onFrameRateChange;
        private static List<AnimationWindowKeyframe> s_KeyframeClipboard;
        [SerializeField]
        public bool showCurveEditor;
        [SerializeField]
        public bool timeInFrames = true;

        public bool AnyKeyIsSelected(DopeLine dopeline)
        {
            foreach (AnimationWindowKeyframe keyframe in dopeline.keys)
            {
                if (this.KeyIsSelected(keyframe))
                {
                    return true;
                }
            }
            return false;
        }

        public void ClearHierarchySelection()
        {
            this.hierarchyState.selectedIDs.Clear();
            this.m_ActiveCurvesCache = null;
        }

        public void ClearKeySelections()
        {
            this.selectedKeyHashes.Clear();
            this.m_SelectedKeysCache = null;
        }

        public void ClearSelections()
        {
            this.ClearKeySelections();
            this.ClearHierarchySelection();
        }

        public void CopyAllActiveCurves()
        {
            foreach (AnimationWindowCurve curve in this.activeCurves)
            {
                foreach (AnimationWindowKeyframe keyframe in curve.m_Keyframes)
                {
                    s_KeyframeClipboard.Add(new AnimationWindowKeyframe(keyframe));
                }
            }
        }

        public void CopyKeys()
        {
            if (s_KeyframeClipboard == null)
            {
                s_KeyframeClipboard = new List<AnimationWindowKeyframe>();
            }
            float maxValue = float.MaxValue;
            s_KeyframeClipboard.Clear();
            foreach (AnimationWindowKeyframe keyframe in this.selectedKeys)
            {
                s_KeyframeClipboard.Add(new AnimationWindowKeyframe(keyframe));
                float num2 = keyframe.time + keyframe.curve.timeOffset;
                if (num2 < maxValue)
                {
                    maxValue = num2;
                }
            }
            if (s_KeyframeClipboard.Count > 0)
            {
                foreach (AnimationWindowKeyframe keyframe2 in s_KeyframeClipboard)
                {
                    keyframe2.time -= maxValue - keyframe2.curve.timeOffset;
                }
            }
            else
            {
                this.CopyAllActiveCurves();
            }
        }

        private void CurveWasModified(AnimationClip clip, EditorCurveBinding binding, AnimationUtility.CurveModifiedType type)
        {
            <CurveWasModified>c__AnonStorey4A storeya = new <CurveWasModified>c__AnonStorey4A {
                clip = clip
            };
            if (this.m_Selection.Exists(new Predicate<AnimationWindowSelectionItem>(storeya.<>m__8D)))
            {
                if (type == AnimationUtility.CurveModifiedType.CurveModified)
                {
                    bool flag = false;
                    int hashCode = binding.GetHashCode();
                    foreach (AnimationWindowCurve curve in this.allCurves)
                    {
                        int item = curve.binding.GetHashCode();
                        if (item == hashCode)
                        {
                            this.m_ModifiedCurves.Add(item);
                            flag = true;
                        }
                    }
                    if (flag)
                    {
                        this.refresh = RefreshType.CurvesOnly;
                    }
                    else
                    {
                        this.m_lastAddedCurveBinding = new EditorCurveBinding?(binding);
                        this.refresh = RefreshType.Everything;
                    }
                }
                else
                {
                    this.refresh = RefreshType.Everything;
                }
            }
        }

        public void DeleteSelectedKeys()
        {
            if (this.selectedKeys.Count != 0)
            {
                foreach (AnimationWindowKeyframe keyframe in this.selectedKeys)
                {
                    if (keyframe.curve.animationIsEditable)
                    {
                        this.UnselectKey(keyframe);
                        keyframe.curve.m_Keyframes.Remove(keyframe);
                        this.SaveCurve(keyframe.curve);
                    }
                }
                this.ResampleAnimation();
            }
        }

        public void ForceRefresh()
        {
            this.refresh = RefreshType.Everything;
        }

        public string FormatFrame(int frame, int frameDigits)
        {
            int num = frame / ((int) this.frameRate);
            float num2 = ((float) frame) % this.frameRate;
            return (num.ToString() + ":" + num2.ToString().PadLeft(frameDigits, '0'));
        }

        public float FrameDeltaToPixel(Rect rect)
        {
            return (rect.width / this.visibleFrameSpan);
        }

        public float FrameToPixel(float i, Rect rect)
        {
            return (((i - this.minVisibleFrame) * rect.width) / this.visibleFrameSpan);
        }

        public float FrameToTime(float frame)
        {
            return (frame / this.frameRate);
        }

        public List<AnimationWindowCurve> GetAffectedCurves(List<AnimationWindowKeyframe> keyframes)
        {
            List<AnimationWindowCurve> list = new List<AnimationWindowCurve>();
            foreach (AnimationWindowKeyframe keyframe in keyframes)
            {
                if (!list.Contains(keyframe.curve))
                {
                    list.Add(keyframe.curve);
                }
            }
            return list;
        }

        public List<DopeLine> GetAffectedDopelines(List<AnimationWindowKeyframe> keyframes)
        {
            List<DopeLine> list = new List<DopeLine>();
            foreach (AnimationWindowCurve curve in this.GetAffectedCurves(keyframes))
            {
                foreach (DopeLine line in this.dopelines)
                {
                    if (!list.Contains(line) && line.m_Curves.Contains<AnimationWindowCurve>(curve))
                    {
                        list.Add(line);
                    }
                }
            }
            return list;
        }

        public List<int> GetAffectedHierarchyIDs(List<AnimationWindowKeyframe> keyframes)
        {
            List<int> list = new List<int>();
            foreach (DopeLine line in this.GetAffectedDopelines(keyframes))
            {
                if (!list.Contains(line.m_HierarchyNodeID))
                {
                    list.Add(line.m_HierarchyNodeID);
                }
            }
            return list;
        }

        public List<AnimationWindowKeyframe> GetAggregateKeys(AnimationWindowHierarchyNode hierarchyNode)
        {
            <GetAggregateKeys>c__AnonStorey4B storeyb = new <GetAggregateKeys>c__AnonStorey4B {
                hierarchyNode = hierarchyNode
            };
            DopeLine line = this.dopelines.FirstOrDefault<DopeLine>(new Func<DopeLine, bool>(storeyb.<>m__8E));
            if (line == null)
            {
                return null;
            }
            return line.keys;
        }

        public DopeLine GetDopeline(int selectedInstanceID)
        {
            foreach (DopeLine line in this.dopelines)
            {
                if (line.m_HierarchyNodeID == selectedInstanceID)
                {
                    return line;
                }
            }
            return null;
        }

        private int GetRefreshHash()
        {
            return (((((this.m_Selection == null) ? 0 : this.m_Selection.GetRefreshHash()) ^ ((this.hierarchyState == null) ? 0 : this.hierarchyState.expandedIDs.Count)) ^ ((this.hierarchyState == null) ? 0 : this.hierarchyState.GetTallInstancesCount())) ^ (!this.showCurveEditor ? 0 : 1));
        }

        public void HandleHierarchySelectionChanged(int[] selectedInstanceIDs, bool triggerSceneSelectionSync)
        {
            this.m_ActiveCurvesCache = null;
            if (triggerSceneSelectionSync)
            {
                this.SyncSceneSelection(selectedInstanceIDs);
            }
        }

        public bool KeyIsSelected(AnimationWindowKeyframe keyframe)
        {
            return this.selectedKeyHashes.Contains(keyframe.GetHash());
        }

        public void MoveSelectedKeys(float deltaTime)
        {
            this.MoveSelectedKeys(deltaTime, false);
        }

        public void MoveSelectedKeys(float deltaTime, bool snapToFrame)
        {
            this.MoveSelectedKeys(deltaTime, snapToFrame, true);
        }

        public void MoveSelectedKeys(float deltaTime, bool snapToFrame, bool saveToClip)
        {
            List<AnimationWindowKeyframe> list = new List<AnimationWindowKeyframe>(this.selectedKeys);
            List<AnimationWindowKeyframe> currentSelectedKeys = new List<AnimationWindowKeyframe>();
            foreach (AnimationWindowKeyframe keyframe in list)
            {
                if (keyframe.curve.animationIsEditable)
                {
                    keyframe.time += deltaTime;
                    if (snapToFrame)
                    {
                        keyframe.time = this.SnapToFrame(keyframe.time, keyframe.curve.clip.frameRate, !saveToClip);
                    }
                    currentSelectedKeys.Add(keyframe);
                }
            }
            this.ClearKeySelections();
            foreach (AnimationWindowKeyframe keyframe2 in list)
            {
                this.SelectKey(keyframe2);
            }
            if (saveToClip)
            {
                this.SaveSelectedKeys(currentSelectedKeys);
                this.ResampleAnimation();
            }
        }

        public void OnDestroy()
        {
            if (this.m_Selection != null)
            {
                this.m_Selection.Clear();
            }
        }

        public void OnDisable()
        {
            CurveBindingUtility.Cleanup();
            this.recording = false;
            this.playing = false;
            AnimationUtility.onCurveWasModified = (AnimationUtility.OnCurveWasModified) Delegate.Remove(AnimationUtility.onCurveWasModified, new AnimationUtility.OnCurveWasModified(this.CurveWasModified));
            Undo.undoRedoPerformed = (Undo.UndoRedoCallback) Delegate.Remove(Undo.undoRedoPerformed, new Undo.UndoRedoCallback(this.UndoRedoPerformed));
            if (this.m_Recording != null)
            {
                this.m_Recording.Dispose();
                this.m_Recording = null;
            }
        }

        public void OnEnable()
        {
            base.hideFlags = HideFlags.HideAndDontSave;
            AnimationUtility.onCurveWasModified = (AnimationUtility.OnCurveWasModified) Delegate.Combine(AnimationUtility.onCurveWasModified, new AnimationUtility.OnCurveWasModified(this.CurveWasModified));
            Undo.undoRedoPerformed = (Undo.UndoRedoCallback) Delegate.Combine(Undo.undoRedoPerformed, new Undo.UndoRedoCallback(this.UndoRedoPerformed));
            this.m_Recording = new AnimationRecordMode();
        }

        public void OnGUI()
        {
            this.RefreshHashCheck();
            this.Refresh();
        }

        public void OnHierarchySelectionChanged(int[] selectedInstanceIDs)
        {
            this.HandleHierarchySelectionChanged(selectedInstanceIDs, true);
            foreach (DopeLine line in this.dopelines)
            {
                if (selectedInstanceIDs.Contains<int>(line.m_HierarchyNodeID))
                {
                    this.SelectKeysFromDopeline(line);
                }
                else
                {
                    this.UnselectKeysFromDopeline(line);
                }
            }
        }

        private void OnNewCurveAdded(EditorCurveBinding newCurve)
        {
            string propertyGroupName = AnimationWindowUtility.GetPropertyGroupName(newCurve.propertyName);
            this.ClearHierarchySelection();
            foreach (AnimationWindowHierarchyNode node in this.hierarchyData.GetRows())
            {
                if (((node.path == newCurve.path) && (node.animatableObjectType == newCurve.type)) && (node.propertyName == propertyGroupName))
                {
                    this.SelectHierarchyItem(node.id, true, false);
                    if (newCurve.isPPtrCurve)
                    {
                        this.hierarchyState.AddTallInstance(node.id);
                    }
                }
            }
            if (this.recording)
            {
                this.ResampleAnimation();
                InspectorWindow.RepaintAllInspectors();
            }
            this.m_lastAddedCurveBinding = null;
        }

        public void OnSelectionChanged()
        {
            CurveBindingUtility.Cleanup();
            if (this.onFrameRateChange != null)
            {
                this.onFrameRateChange(this.frameRate);
            }
            if (this.recording)
            {
                this.ResampleAnimation();
            }
        }

        public void PasteKeys()
        {
            if (s_KeyframeClipboard == null)
            {
                s_KeyframeClipboard = new List<AnimationWindowKeyframe>();
            }
            HashSet<int> set = new HashSet<int>(this.m_SelectedKeyHashes);
            this.ClearKeySelections();
            AnimationWindowCurve curve = null;
            AnimationWindowCurve curve2 = null;
            float startTime = 0f;
            List<AnimationWindowCurve> source = new List<AnimationWindowCurve>();
            foreach (AnimationWindowKeyframe keyframe in s_KeyframeClipboard)
            {
                if (!source.Any<AnimationWindowCurve>() || (source.Last<AnimationWindowCurve>() != keyframe.curve))
                {
                    source.Add(keyframe.curve);
                }
            }
            bool flag = source.Count<AnimationWindowCurve>() == this.activeCurves.Count<AnimationWindowCurve>();
            int num2 = 0;
            foreach (AnimationWindowKeyframe keyframe2 in s_KeyframeClipboard)
            {
                if ((curve2 != null) && (keyframe2.curve != curve2))
                {
                    num2++;
                }
                AnimationWindowKeyframe keyframe3 = new AnimationWindowKeyframe(keyframe2);
                if (flag)
                {
                    keyframe3.curve = this.activeCurves[num2];
                }
                else
                {
                    keyframe3.curve = AnimationWindowUtility.BestMatchForPaste(keyframe3.curve.binding, source, this.activeCurves);
                }
                if (keyframe3.curve == null)
                {
                    if (this.activeCurves.Count > 0)
                    {
                        AnimationWindowCurve curve3 = this.activeCurves[0];
                        if (curve3.animationIsEditable)
                        {
                            keyframe3.curve = new AnimationWindowCurve(curve3.clip, keyframe2.curve.binding, keyframe2.curve.type);
                            keyframe3.curve.selectionBindingInterface = curve3.selectionBindingInterface;
                            keyframe3.time = keyframe2.time;
                        }
                    }
                    else
                    {
                        AnimationWindowSelectionItem item = this.m_Selection.First();
                        if (item.animationIsEditable)
                        {
                            keyframe3.curve = new AnimationWindowCurve(item.animationClip, keyframe2.curve.binding, keyframe2.curve.type);
                            keyframe3.curve.selectionBindingInterface = item;
                            keyframe3.time = keyframe2.time;
                        }
                    }
                }
                if ((keyframe3.curve != null) && keyframe3.curve.animationIsEditable)
                {
                    keyframe3.time += this.time.time - keyframe3.curve.timeOffset;
                    if (((keyframe3.time >= 0f) && (keyframe3.curve != null)) && (keyframe3.curve.isPPtrCurve == keyframe2.curve.isPPtrCurve))
                    {
                        if (keyframe3.curve.HasKeyframe(AnimationKeyTime.Time(keyframe3.time, keyframe3.curve.clip.frameRate)))
                        {
                            keyframe3.curve.RemoveKeyframe(AnimationKeyTime.Time(keyframe3.time, keyframe3.curve.clip.frameRate));
                        }
                        if (curve == keyframe3.curve)
                        {
                            keyframe3.curve.RemoveKeysAtRange(startTime, keyframe3.time);
                        }
                        keyframe3.curve.m_Keyframes.Add(keyframe3);
                        this.SelectKey(keyframe3);
                        this.SaveCurve(keyframe3.curve);
                        curve = keyframe3.curve;
                        startTime = keyframe3.time;
                    }
                    curve2 = keyframe2.curve;
                }
            }
            if (this.m_SelectedKeyHashes.Count == 0)
            {
                this.m_SelectedKeyHashes = set;
            }
            else
            {
                this.ResampleAnimation();
            }
        }

        public float PixelDeltaToTime(Rect rect)
        {
            return (this.visibleTimeSpan / rect.width);
        }

        public float PixelToTime(float pixel)
        {
            return this.PixelToTime(pixel, SnapMode.Disabled);
        }

        public float PixelToTime(float pixel, SnapMode snap)
        {
            float num = pixel - this.zeroTimePixel;
            return this.SnapToFrame(num / this.pixelPerSecond, snap);
        }

        public float PixelToTime(float pixelX, Rect rect)
        {
            return (((pixelX * this.visibleTimeSpan) / rect.width) + this.minVisibleTime);
        }

        private UndoPropertyModification[] PostprocessAnimationRecordingModifications(UndoPropertyModification[] modifications)
        {
            if (!AnimationMode.InAnimationMode())
            {
                Undo.postprocessModifications = (Undo.PostprocessModifications) Delegate.Remove(Undo.postprocessModifications, new Undo.PostprocessModifications(this.PostprocessAnimationRecordingModifications));
                return modifications;
            }
            return AnimationRecording.Process(this, modifications);
        }

        private void Refresh()
        {
            if (this.refresh == RefreshType.Everything)
            {
                this.m_Selection.Refresh();
                this.m_ActiveKeyframeCache = null;
                this.m_ActiveCurvesCache = null;
                this.m_dopelinesCache = null;
                this.m_SelectedKeysCache = null;
                this.m_ActiveCurveWrappersCache = null;
                if (this.hierarchyData != null)
                {
                    this.hierarchyData.UpdateData();
                }
                if (this.m_lastAddedCurveBinding.HasValue)
                {
                    this.OnNewCurveAdded(this.m_lastAddedCurveBinding.Value);
                }
                if ((this.activeCurves.Count == 0) && (this.dopelines.Count > 0))
                {
                    this.SelectHierarchyItem(this.dopelines[0], false, false);
                }
                this.m_Refresh = RefreshType.None;
            }
            else if (this.refresh == RefreshType.CurvesOnly)
            {
                this.m_ActiveKeyframeCache = null;
                this.m_ActiveCurvesCache = null;
                this.m_ActiveCurveWrappersCache = null;
                this.m_SelectedKeysCache = null;
                this.ReloadModifiedAnimationCurveCache();
                this.ReloadModifiedDopelineCache();
                this.m_Refresh = RefreshType.None;
                this.m_ModifiedCurves.Clear();
            }
            if (this.selection.disabled && this.recording)
            {
                this.recording = false;
            }
        }

        private void RefreshHashCheck()
        {
            int refreshHash = this.GetRefreshHash();
            if (this.m_PreviousRefreshHash != refreshHash)
            {
                this.refresh = RefreshType.Everything;
                this.m_PreviousRefreshHash = refreshHash;
            }
        }

        private void ReloadModifiedAnimationCurveCache()
        {
            foreach (AnimationWindowCurve curve in this.allCurves)
            {
                if (this.m_ModifiedCurves.Contains(curve.binding.GetHashCode()))
                {
                    curve.LoadKeyframes(curve.clip);
                }
            }
        }

        private void ReloadModifiedDopelineCache()
        {
            if (this.m_dopelinesCache != null)
            {
                foreach (DopeLine line in this.m_dopelinesCache)
                {
                    foreach (AnimationWindowCurve curve in line.m_Curves)
                    {
                        if (this.m_ModifiedCurves.Contains(curve.binding.GetHashCode()))
                        {
                            line.LoadKeyframes();
                        }
                    }
                }
            }
        }

        public void RemoveCurve(AnimationWindowCurve curve)
        {
            if (curve.animationIsEditable)
            {
                Undo.RegisterCompleteObjectUndo(curve.clip, "Remove Curve");
                if (curve.isPPtrCurve)
                {
                    AnimationUtility.SetObjectReferenceCurve(curve.clip, curve.binding, null);
                }
                else
                {
                    AnimationUtility.SetEditorCurve(curve.clip, curve.binding, null);
                }
            }
        }

        public void Repaint()
        {
            if (this.animEditor != null)
            {
                this.animEditor.Repaint();
            }
        }

        public void ResampleAnimation()
        {
            if ((((!this.disabled && ((this.policy == null) || this.policy.allowRecording)) && !this.animatorIsOptimized) && ((this.policy == null) || this.policy.allowRecording)) && this.canRecord)
            {
                bool flag = false;
                foreach (AnimationWindowSelectionItem item in this.selection.ToArray())
                {
                    if (item.animationClip != null)
                    {
                        if (!this.recording)
                        {
                            this.recording = true;
                        }
                        Undo.FlushUndoRecordObjects();
                        AnimationMode.BeginSampling();
                        CurveBindingUtility.SampleAnimationClip(item.rootGameObject, item.animationClip, this.currentTime - item.timeOffset);
                        AnimationMode.EndSampling();
                        flag = true;
                    }
                }
                if (flag)
                {
                    SceneView.RepaintAll();
                    ParticleSystemWindow instance = ParticleSystemWindow.GetInstance();
                    if (instance != null)
                    {
                        instance.Repaint();
                    }
                }
            }
        }

        public void SaveCurve(AnimationWindowCurve curve)
        {
            if (!curve.animationIsEditable)
            {
                Debug.LogError("Curve is not editable and shouldn't be saved.");
            }
            Undo.RegisterCompleteObjectUndo(curve.clip, "Edit Curve");
            AnimationRecording.SaveModifiedCurve(curve, curve.clip);
            this.Repaint();
        }

        public void SaveSelectedKeys(List<AnimationWindowKeyframe> currentSelectedKeys)
        {
            List<AnimationWindowCurve> list = new List<AnimationWindowCurve>();
            foreach (AnimationWindowKeyframe keyframe in currentSelectedKeys)
            {
                if (!list.Contains(keyframe.curve))
                {
                    list.Add(keyframe.curve);
                }
                List<AnimationWindowKeyframe> list2 = new List<AnimationWindowKeyframe>();
                foreach (AnimationWindowKeyframe keyframe2 in keyframe.curve.m_Keyframes)
                {
                    if (!currentSelectedKeys.Contains(keyframe2) && (AnimationKeyTime.Time(keyframe.time, this.frameRate).frame == AnimationKeyTime.Time(keyframe2.time, this.frameRate).frame))
                    {
                        list2.Add(keyframe2);
                    }
                }
                foreach (AnimationWindowKeyframe keyframe3 in list2)
                {
                    keyframe.curve.m_Keyframes.Remove(keyframe3);
                }
            }
            foreach (AnimationWindowCurve curve in list)
            {
                this.SaveCurve(curve);
            }
        }

        public void SelectHierarchyItem(DopeLine dopeline, bool additive)
        {
            this.SelectHierarchyItem(dopeline.m_HierarchyNodeID, additive, true);
        }

        public void SelectHierarchyItem(int hierarchyNodeID, bool additive, bool triggerSceneSelectionSync)
        {
            if (!additive)
            {
                this.ClearHierarchySelection();
            }
            this.hierarchyState.selectedIDs.Add(hierarchyNodeID);
            int[] selectedInstanceIDs = this.hierarchyState.selectedIDs.ToArray();
            this.HandleHierarchySelectionChanged(selectedInstanceIDs, triggerSceneSelectionSync);
        }

        public void SelectHierarchyItem(DopeLine dopeline, bool additive, bool triggerSceneSelectionSync)
        {
            this.SelectHierarchyItem(dopeline.m_HierarchyNodeID, additive, triggerSceneSelectionSync);
        }

        public void SelectKey(AnimationWindowKeyframe keyframe)
        {
            int hash = keyframe.GetHash();
            if (!this.selectedKeyHashes.Contains(hash))
            {
                this.selectedKeyHashes.Add(hash);
            }
            this.m_SelectedKeysCache = null;
        }

        public void SelectKeysFromDopeline(DopeLine dopeline)
        {
            if (dopeline != null)
            {
                foreach (AnimationWindowKeyframe keyframe in dopeline.keys)
                {
                    this.SelectKey(keyframe);
                }
            }
        }

        public float SnapToFrame(float time, SnapMode snap)
        {
            return this.SnapToFrame(time, snap, false);
        }

        public float SnapToFrame(float time, float fps, bool preventHashCollision)
        {
            float num = Mathf.Round(time * fps) / fps;
            if (preventHashCollision)
            {
                num += 0.01f / fps;
            }
            return num;
        }

        public float SnapToFrame(float time, SnapMode snap, bool preventHashCollision)
        {
            if (snap == SnapMode.Disabled)
            {
                return time;
            }
            float fps = (snap != SnapMode.SnapToFrame) ? this.clipFrameRate : this.frameRate;
            return this.SnapToFrame(time, fps, preventHashCollision);
        }

        private void SyncSceneSelection(int[] selectedNodeIDs)
        {
            List<int> list = new List<int>();
            foreach (int num in selectedNodeIDs)
            {
                AnimationWindowHierarchyNode node = this.hierarchyData.FindItem(num) as AnimationWindowHierarchyNode;
                if (((this.activeRootGameObject != null) && (node != null)) && !(node is AnimationWindowHierarchyMasterNode))
                {
                    Transform tr = this.activeRootGameObject.transform.Find(node.path);
                    if (((tr != null) && (this.activeRootGameObject != null)) && (this.activeAnimationPlayer == AnimationWindowUtility.GetClosestAnimationPlayerComponentInParents(tr)))
                    {
                        list.Add(tr.gameObject.GetInstanceID());
                    }
                }
            }
            Selection.instanceIDs = list.ToArray();
        }

        public float TimeToFrame(float time)
        {
            return (time * this.frameRate);
        }

        public int TimeToFrameFloor(float time)
        {
            return Mathf.FloorToInt(this.TimeToFrame(time));
        }

        public int TimeToFrameRound(float time)
        {
            return Mathf.RoundToInt(this.TimeToFrame(time));
        }

        public float TimeToPixel(float time)
        {
            return this.TimeToPixel(time, SnapMode.Disabled);
        }

        public float TimeToPixel(float time, SnapMode snap)
        {
            return ((this.SnapToFrame(time, snap) * this.pixelPerSecond) + this.zeroTimePixel);
        }

        public float TimeToPixel(float time, Rect rect)
        {
            return this.FrameToPixel(time * this.frameRate, rect);
        }

        public void UndoRedoPerformed()
        {
            this.refresh = RefreshType.Everything;
            if (this.recording)
            {
                this.ResampleAnimation();
            }
        }

        public void UnSelectHierarchyItem(int hierarchyNodeID)
        {
            this.hierarchyState.selectedIDs.Remove(hierarchyNodeID);
        }

        public void UnSelectHierarchyItem(DopeLine dopeline)
        {
            this.UnSelectHierarchyItem(dopeline.m_HierarchyNodeID);
        }

        public void UnselectKey(AnimationWindowKeyframe keyframe)
        {
            int hash = keyframe.GetHash();
            if (this.selectedKeyHashes.Contains(hash))
            {
                this.selectedKeyHashes.Remove(hash);
            }
            this.m_SelectedKeysCache = null;
        }

        public void UnselectKeysFromDopeline(DopeLine dopeline)
        {
            if (dopeline != null)
            {
                foreach (AnimationWindowKeyframe keyframe in dopeline.keys)
                {
                    this.UnselectKey(keyframe);
                }
            }
        }

        public AnimationClip activeAnimationClip
        {
            get
            {
                if (this.selectedItem != null)
                {
                    return this.selectedItem.animationClip;
                }
                return null;
            }
        }

        public Component activeAnimationPlayer
        {
            get
            {
                if (this.selectedItem != null)
                {
                    return this.selectedItem.animationPlayer;
                }
                return null;
            }
        }

        public List<AnimationWindowCurve> activeCurves
        {
            get
            {
                if (this.m_ActiveCurvesCache == null)
                {
                    this.m_ActiveCurvesCache = new List<AnimationWindowCurve>();
                    if ((this.hierarchyState != null) && (this.hierarchyData != null))
                    {
                        foreach (int num in this.hierarchyState.selectedIDs)
                        {
                            AnimationWindowHierarchyNode node = this.hierarchyData.FindItem(num) as AnimationWindowHierarchyNode;
                            if (node != null)
                            {
                                AnimationWindowCurve[] curves = node.curves;
                                if (curves != null)
                                {
                                    foreach (AnimationWindowCurve curve in curves)
                                    {
                                        if (!this.m_ActiveCurvesCache.Contains(curve))
                                        {
                                            this.m_ActiveCurvesCache.Add(curve);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return this.m_ActiveCurvesCache;
            }
        }

        public List<CurveWrapper> activeCurveWrappers
        {
            get
            {
                if ((this.m_ActiveCurveWrappersCache == null) || (this.m_ActiveCurvesCache == null))
                {
                    List<CurveWrapper> source = new List<CurveWrapper>();
                    foreach (AnimationWindowCurve curve in this.activeCurves)
                    {
                        if (!curve.isPPtrCurve)
                        {
                            source.Add(AnimationWindowUtility.GetCurveWrapper(curve, curve.clip));
                        }
                    }
                    if (!source.Any<CurveWrapper>())
                    {
                        foreach (AnimationWindowCurve curve2 in this.allCurves)
                        {
                            if (!curve2.isPPtrCurve)
                            {
                                source.Add(AnimationWindowUtility.GetCurveWrapper(curve2, curve2.clip));
                            }
                        }
                    }
                    this.m_ActiveCurveWrappersCache = source;
                }
                return this.m_ActiveCurveWrappersCache;
            }
        }

        public GameObject activeGameObject
        {
            get
            {
                if (this.selectedItem != null)
                {
                    return this.selectedItem.gameObject;
                }
                return null;
            }
        }

        public AnimationWindowKeyframe activeKeyframe
        {
            get
            {
                if (this.m_ActiveKeyframeCache == null)
                {
                    foreach (AnimationWindowCurve curve in this.allCurves)
                    {
                        foreach (AnimationWindowKeyframe keyframe in curve.m_Keyframes)
                        {
                            if (keyframe.GetHash() == this.m_ActiveKeyframeHash)
                            {
                                this.m_ActiveKeyframeCache = keyframe;
                            }
                        }
                    }
                }
                return this.m_ActiveKeyframeCache;
            }
            set
            {
                this.m_ActiveKeyframeCache = null;
                this.m_ActiveKeyframeHash = (value == null) ? 0 : value.GetHash();
            }
        }

        public GameObject activeRootGameObject
        {
            get
            {
                if (this.selectedItem != null)
                {
                    return this.selectedItem.rootGameObject;
                }
                return null;
            }
        }

        public List<AnimationWindowCurve> allCurves
        {
            get
            {
                return this.m_Selection.curves;
            }
        }

        public bool animatorIsOptimized
        {
            get
            {
                if (this.activeRootGameObject == null)
                {
                    return false;
                }
                Animator component = this.activeRootGameObject.GetComponent<Animator>();
                return ((component != null) && (component.isOptimizable && !component.hasTransformHierarchy));
            }
        }

        public bool canRecord
        {
            get
            {
                if (Application.isPlaying)
                {
                    return false;
                }
                return ((this.m_Recording != null) && this.m_Recording.canEnable);
            }
        }

        public float clipFrameRate
        {
            get
            {
                if (this.activeAnimationClip == null)
                {
                    return 60f;
                }
                return this.activeAnimationClip.frameRate;
            }
            set
            {
                if (((this.activeAnimationClip != null) && (value > 0f)) && (value <= 10000f))
                {
                    foreach (AnimationWindowCurve curve in this.allCurves)
                    {
                        foreach (AnimationWindowKeyframe keyframe in curve.m_Keyframes)
                        {
                            int frame = AnimationKeyTime.Time(keyframe.time, this.clipFrameRate).frame;
                            keyframe.time = AnimationKeyTime.Frame(frame, value).time;
                        }
                        this.SaveCurve(curve);
                    }
                    AnimationEvent[] animationEvents = AnimationUtility.GetAnimationEvents(this.activeAnimationClip);
                    foreach (AnimationEvent event2 in animationEvents)
                    {
                        int num3 = AnimationKeyTime.Time(event2.time, this.clipFrameRate).frame;
                        event2.time = AnimationKeyTime.Frame(num3, value).time;
                    }
                    AnimationUtility.SetAnimationEvents(this.activeAnimationClip, animationEvents);
                    this.activeAnimationClip.frameRate = value;
                }
            }
        }

        public float currentTime
        {
            get
            {
                return this.m_CurrentTime;
            }
            set
            {
                if (!Mathf.Approximately(this.m_CurrentTime, value))
                {
                    this.m_CurrentTime = Mathf.Max(value, 0f);
                    this.m_CurrentFrame = this.TimeToFrameFloor(this.m_CurrentTime);
                    this.ResampleAnimation();
                }
            }
        }

        public bool disabled
        {
            get
            {
                return this.selection.disabled;
            }
        }

        public List<DopeLine> dopelines
        {
            get
            {
                if (this.m_dopelinesCache == null)
                {
                    this.m_dopelinesCache = new List<DopeLine>();
                    if (this.hierarchyData != null)
                    {
                        foreach (TreeViewItem item in this.hierarchyData.GetRows())
                        {
                            AnimationWindowHierarchyNode node = item as AnimationWindowHierarchyNode;
                            if ((node != null) && !(node is AnimationWindowHierarchyAddButtonNode))
                            {
                                AnimationWindowCurve[] curves = node.curves;
                                if (curves != null)
                                {
                                    DopeLine line = new DopeLine(item.id, curves) {
                                        tallMode = this.hierarchyState.GetTallMode(node),
                                        objectType = node.animatableObjectType,
                                        hasChildren = !(node is AnimationWindowHierarchyPropertyNode),
                                        isMasterDopeline = item is AnimationWindowHierarchyMasterNode
                                    };
                                    this.m_dopelinesCache.Add(line);
                                }
                            }
                        }
                    }
                }
                return this.m_dopelinesCache;
            }
        }

        public int frame
        {
            get
            {
                return this.m_CurrentFrame;
            }
            set
            {
                if (this.m_CurrentFrame != value)
                {
                    this.m_CurrentFrame = Math.Max(value, 0);
                    this.m_CurrentTime = this.FrameToTime((float) this.m_CurrentFrame);
                    this.ResampleAnimation();
                }
            }
        }

        public float frameRate
        {
            get
            {
                return this.m_FrameRate;
            }
            set
            {
                if (this.m_FrameRate != value)
                {
                    this.m_FrameRate = value;
                    if (this.onFrameRateChange != null)
                    {
                        this.onFrameRateChange(this.m_FrameRate);
                    }
                }
            }
        }

        public bool locked
        {
            get
            {
                return this.selection.locked;
            }
            set
            {
                this.selection.locked = value;
            }
        }

        public float maxTime
        {
            get
            {
                return this.timeRange.y;
            }
        }

        public float maxVisibleFrame
        {
            get
            {
                return (this.maxVisibleTime * this.frameRate);
            }
        }

        public float maxVisibleTime
        {
            get
            {
                return this.m_TimeArea.shownArea.xMax;
            }
        }

        public float minTime
        {
            get
            {
                return this.timeRange.x;
            }
        }

        public float minVisibleFrame
        {
            get
            {
                return (this.minVisibleTime * this.frameRate);
            }
        }

        public float minVisibleTime
        {
            get
            {
                return this.m_TimeArea.shownArea.xMin;
            }
        }

        public float pixelPerSecond
        {
            get
            {
                return this.timeArea.m_Scale.x;
            }
        }

        public bool playing
        {
            get
            {
                return AnimationMode.InAnimationPlaybackMode();
            }
            set
            {
                if (!Application.isPlaying)
                {
                    if (value && !AnimationMode.InAnimationPlaybackMode())
                    {
                        AnimationMode.StartAnimationPlaybackMode();
                        this.recording = true;
                    }
                    if (!value && AnimationMode.InAnimationPlaybackMode())
                    {
                        AnimationMode.StopAnimationPlaybackMode();
                        this.currentTime = this.FrameToTime((float) this.frame);
                    }
                }
            }
        }

        public AnimationWindowPolicy policy
        {
            get
            {
                return this.m_Policy;
            }
            set
            {
                this.m_Policy = value;
            }
        }

        public bool recording
        {
            get
            {
                return ((this.m_Recording != null) && this.m_Recording.enable);
            }
            set
            {
                if ((((!value || (this.policy == null)) || this.policy.allowRecording) && !Application.isPlaying) && (this.m_Recording != null))
                {
                    bool enable = this.m_Recording.enable;
                    this.m_Recording.enable = value;
                    bool flag2 = this.m_Recording.enable;
                    if (enable != flag2)
                    {
                        if (flag2)
                        {
                            Undo.postprocessModifications = (Undo.PostprocessModifications) Delegate.Combine(Undo.postprocessModifications, new Undo.PostprocessModifications(this.PostprocessAnimationRecordingModifications));
                        }
                        else
                        {
                            Undo.postprocessModifications = (Undo.PostprocessModifications) Delegate.Remove(Undo.postprocessModifications, new Undo.PostprocessModifications(this.PostprocessAnimationRecordingModifications));
                        }
                    }
                }
            }
        }

        public RefreshType refresh
        {
            get
            {
                return this.m_Refresh;
            }
            set
            {
                if (this.m_Refresh < value)
                {
                    this.m_Refresh = value;
                }
            }
        }

        public List<AnimationWindowHierarchyNode> selectedHierarchyNodes
        {
            get
            {
                List<AnimationWindowHierarchyNode> list = new List<AnimationWindowHierarchyNode>();
                if ((this.activeAnimationClip != null) && (this.hierarchyData != null))
                {
                    foreach (int num in this.hierarchyState.selectedIDs)
                    {
                        AnimationWindowHierarchyNode item = (AnimationWindowHierarchyNode) this.hierarchyData.FindItem(num);
                        if ((item != null) && !(item is AnimationWindowHierarchyAddButtonNode))
                        {
                            list.Add(item);
                        }
                    }
                }
                return list;
            }
        }

        public AnimationWindowSelectionItem selectedItem
        {
            get
            {
                if ((this.m_Selection != null) && (this.m_Selection.count > 0))
                {
                    return this.m_Selection.First();
                }
                return null;
            }
            set
            {
                if (this.m_Selection == null)
                {
                    this.m_Selection = new AnimationWindowSelection();
                }
                if (value == null)
                {
                    this.m_Selection.Clear();
                }
                else
                {
                    this.m_Selection.Set(value);
                }
            }
        }

        private HashSet<int> selectedKeyHashes
        {
            get
            {
                if (this.m_SelectedKeyHashes == null)
                {
                }
                return (this.m_SelectedKeyHashes = new HashSet<int>());
            }
            set
            {
                this.m_SelectedKeyHashes = value;
            }
        }

        public List<AnimationWindowKeyframe> selectedKeys
        {
            get
            {
                if (this.m_SelectedKeysCache == null)
                {
                    this.m_SelectedKeysCache = new List<AnimationWindowKeyframe>();
                    foreach (AnimationWindowCurve curve in this.allCurves)
                    {
                        foreach (AnimationWindowKeyframe keyframe in curve.m_Keyframes)
                        {
                            if (this.KeyIsSelected(keyframe))
                            {
                                this.m_SelectedKeysCache.Add(keyframe);
                            }
                        }
                    }
                }
                return this.m_SelectedKeysCache;
            }
        }

        public AnimationWindowSelection selection
        {
            get
            {
                if (this.m_Selection == null)
                {
                    this.m_Selection = new AnimationWindowSelection();
                }
                return this.m_Selection;
            }
        }

        public bool syncTimeDuringDrag
        {
            get
            {
                return false;
            }
        }

        public AnimationKeyTime time
        {
            get
            {
                return AnimationKeyTime.Frame(this.frame, this.frameRate);
            }
        }

        public TimeArea timeArea
        {
            get
            {
                return this.m_TimeArea;
            }
            set
            {
                this.m_TimeArea = value;
            }
        }

        public Vector2 timeRange
        {
            get
            {
                float a = 0f;
                float minValue = 0f;
                if (this.m_Selection.count > 0)
                {
                    a = float.MaxValue;
                    minValue = float.MinValue;
                    foreach (AnimationWindowSelectionItem item in this.m_Selection.ToArray())
                    {
                        a = Mathf.Min(a, item.animationClip.startTime + item.timeOffset);
                        minValue = Mathf.Max(minValue, item.animationClip.stopTime + item.timeOffset);
                    }
                }
                return new Vector2(a, minValue);
            }
        }

        public float visibleFrameSpan
        {
            get
            {
                return (this.visibleTimeSpan * this.frameRate);
            }
        }

        public float visibleTimeSpan
        {
            get
            {
                return (this.maxVisibleTime - this.minVisibleTime);
            }
        }

        public float zeroTimePixel
        {
            get
            {
                return ((this.timeArea.shownArea.xMin * this.timeArea.m_Scale.x) * -1f);
            }
        }

        [CompilerGenerated]
        private sealed class <CurveWasModified>c__AnonStorey4A
        {
            internal AnimationClip clip;

            internal bool <>m__8D(AnimationWindowSelectionItem item)
            {
                return (item.animationClip == this.clip);
            }
        }

        [CompilerGenerated]
        private sealed class <GetAggregateKeys>c__AnonStorey4B
        {
            internal AnimationWindowHierarchyNode hierarchyNode;

            internal bool <>m__8E(DopeLine e)
            {
                return (e.m_HierarchyNodeID == this.hierarchyNode.id);
            }
        }

        public enum RefreshType
        {
            None,
            CurvesOnly,
            Everything
        }

        public enum SnapMode
        {
            Disabled,
            SnapToFrame,
            SnapToClipFrame
        }
    }
}

