﻿namespace UnityEditorInternal
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal static class WebURLs
    {
        public static string betaLandingPage { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public static string cloudBuildPage { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public static string unity { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public static string unityAnswers { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public static string unityConnect { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public static string unityFeedback { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public static string unityForum { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public static string whatsNewPage { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }
    }
}

