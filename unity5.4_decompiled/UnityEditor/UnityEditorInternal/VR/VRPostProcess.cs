﻿namespace UnityEditorInternal.VR
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEditor.Callbacks;
    using UnityEditorInternal;
    using UnityEngine;

    internal static class VRPostProcess
    {
        [CompilerGenerated]
        private static Func<VRDeviceInfoEditor, bool> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<VRDeviceInfoEditor, string> <>f__am$cache2;
        private static readonly string VR_FOLDER = Path.Combine("VR", "Unity");

        private static string GetPluginExtension(BuildTarget target, bool isSpatializer)
        {
            BuildTarget target2 = target;
            switch (target2)
            {
                case BuildTarget.StandaloneOSXUniversal:
                case BuildTarget.StandaloneOSXIntel:
                    goto Label_004A;

                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                case BuildTarget.WSAPlayer:
                    break;

                default:
                    switch (target2)
                    {
                        case BuildTarget.Android:
                            if (isSpatializer)
                            {
                                return ".so";
                            }
                            return ".aar";

                        case BuildTarget.StandaloneOSXIntel64:
                            goto Label_004A;

                        default:
                            return string.Empty;
                    }
                    break;
            }
            return ".dll";
        Label_004A:
            return ".bundle";
        }

        private static string GetPluginFolder(BuildTarget target)
        {
            string buildTargetName = BuildPipeline.GetBuildTargetName(target);
            string[] paths = new string[] { EditorApplication.applicationContentsPath, VR_FOLDER, buildTargetName };
            string[] strArray2 = new string[] { EditorApplication.applicationContentsPath, VR_FOLDER, buildTargetName.ToLower() };
            string path = FileUtil.CombinePaths(paths);
            if (!Directory.Exists(path))
            {
                path = FileUtil.CombinePaths(strArray2);
                if (!Directory.Exists(path))
                {
                    path = null;
                }
            }
            return path;
        }

        [RegisterPlugins]
        private static IEnumerable<PluginDesc> RegisterSpatializerPlugins(BuildTarget target)
        {
            List<PluginDesc> source = new List<PluginDesc>();
            if (AudioUtil.canUseSpatializerEffect)
            {
                string pluginFolder = GetPluginFolder(target);
                if (pluginFolder == null)
                {
                    return source;
                }
                VRDeviceInfoEditor[] allVRDeviceInfo = VREditor.GetAllVRDeviceInfo(BuildPipeline.GetBuildTargetGroup(target));
                string currentSpatializerEffectName = AudioUtil.GetCurrentSpatializerEffectName();
                for (int i = 0; i < allVRDeviceInfo.Length; i++)
                {
                    if (currentSpatializerEffectName == allVRDeviceInfo[i].spatializerEffectName)
                    {
                        string[] paths = new string[] { pluginFolder, allVRDeviceInfo[i].spatializerPluginName };
                        string path = FileUtil.CombinePaths(paths) + GetPluginExtension(target, true);
                        if (File.Exists(path) || Directory.Exists(path))
                        {
                            PluginDesc item = new PluginDesc {
                                pluginPath = path
                            };
                            source.Add(item);
                            Debug.LogWarning("Native Spatializer Plugin: " + allVRDeviceInfo[i].spatializerPluginName + " was included in build.");
                            break;
                        }
                    }
                }
                if (!source.Any<PluginDesc>())
                {
                    Debug.LogWarning("Spatializer Effect: " + currentSpatializerEffectName + ", is not natively supported for the current build target.");
                }
            }
            return source;
        }

        [RegisterPlugins]
        private static IEnumerable<PluginDesc> RegisterVRPlugins(BuildTarget target)
        {
            List<PluginDesc> source = new List<PluginDesc>();
            if (PlayerSettings.virtualRealitySupported)
            {
                string pluginFolder = GetPluginFolder(target);
                if (pluginFolder != null)
                {
                    string pluginExtension = GetPluginExtension(target, false);
                    if (<>f__am$cache1 == null)
                    {
                        <>f__am$cache1 = d => !string.IsNullOrEmpty(d.externalPluginName);
                    }
                    if (<>f__am$cache2 == null)
                    {
                        <>f__am$cache2 = d => d.externalPluginName;
                    }
                    string[] strArray = VREditor.GetEnabledVRDeviceInfo(BuildPipeline.GetBuildTargetGroup(target)).Where<VRDeviceInfoEditor>(<>f__am$cache1).Select<VRDeviceInfoEditor, string>(<>f__am$cache2).ToArray<string>();
                    for (int i = 0; i < strArray.Length; i++)
                    {
                        string[] paths = new string[] { pluginFolder, strArray[i] };
                        PluginDesc item = new PluginDesc {
                            pluginPath = FileUtil.CombinePaths(paths) + pluginExtension
                        };
                        source.Add(item);
                    }
                }
                if (!source.Any<PluginDesc>())
                {
                    Debug.LogWarning("Unable to find plugins folder " + target + ". Native VR plugins will not be loaded.");
                }
            }
            return source;
        }
    }
}

