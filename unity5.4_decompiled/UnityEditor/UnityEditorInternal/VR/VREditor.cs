﻿namespace UnityEditorInternal.VR
{
    using System;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEngine;

    public sealed class VREditor
    {
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern VRDeviceInfoEditor[] GetAllVRDeviceInfo(BuildTargetGroup targetGroup);
        public static VRDeviceInfoEditor[] GetEnabledVRDeviceInfo(BuildTargetGroup targetGroup)
        {
            <GetEnabledVRDeviceInfo>c__AnonStorey31 storey = new <GetEnabledVRDeviceInfo>c__AnonStorey31 {
                enabledVRDevices = GetVREnabledDevices(targetGroup)
            };
            return GetAllVRDeviceInfo(targetGroup).Where<VRDeviceInfoEditor>(new Func<VRDeviceInfoEditor, bool>(storey.<>m__4E)).ToArray<VRDeviceInfoEditor>();
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern bool GetVREnabled(BuildTargetGroup targetGroup);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern string[] GetVREnabledDevices(BuildTargetGroup targetGroup);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern void SetVREnabled(BuildTargetGroup targetGroup, bool value);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern void SetVREnabledDevices(BuildTargetGroup targetGroup, string[] devices);

        [CompilerGenerated]
        private sealed class <GetEnabledVRDeviceInfo>c__AnonStorey31
        {
            internal string[] enabledVRDevices;

            internal bool <>m__4E(VRDeviceInfoEditor d)
            {
                return this.enabledVRDevices.Contains<string>(d.deviceNameKey);
            }
        }
    }
}

