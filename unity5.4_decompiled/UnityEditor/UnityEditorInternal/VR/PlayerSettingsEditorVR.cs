﻿namespace UnityEditorInternal.VR
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEditorInternal;
    using UnityEngine;

    internal class PlayerSettingsEditorVR
    {
        [CompilerGenerated]
        private static Func<VRDeviceInfoEditor, string> <>f__am$cache4;
        [CompilerGenerated]
        private static ReorderableList.HeaderCallbackDelegate <>f__am$cache5;
        private Dictionary<BuildTargetGroup, VRDeviceInfoEditor[]> m_AllVRDevicesForBuildTarget = new Dictionary<BuildTargetGroup, VRDeviceInfoEditor[]>();
        private Dictionary<string, string> m_MapVRDeviceKeyToUIString = new Dictionary<string, string>();
        private Dictionary<string, string> m_MapVRUIStringToDeviceKey = new Dictionary<string, string>();
        private Dictionary<BuildTargetGroup, ReorderableList> m_VRDeviceActiveUI = new Dictionary<BuildTargetGroup, ReorderableList>();

        private void AddVRDeviceElement(BuildTargetGroup target, Rect rect, ReorderableList list)
        {
            <AddVRDeviceElement>c__AnonStorey2E storeye = new <AddVRDeviceElement>c__AnonStorey2E();
            VRDeviceInfoEditor[] source = this.m_AllVRDevicesForBuildTarget[target];
            storeye.enabledDevices = VREditor.GetVREnabledDevices(target).ToList<string>();
            if (<>f__am$cache4 == null)
            {
                <>f__am$cache4 = d => d.deviceNameUI;
            }
            string[] options = source.Select<VRDeviceInfoEditor, string>(<>f__am$cache4).ToArray<string>();
            bool[] enabled = source.Select<VRDeviceInfoEditor, bool>(new Func<VRDeviceInfoEditor, bool>(storeye.<>m__47)).ToArray<bool>();
            EditorUtility.DisplayCustomMenu(rect, options, enabled, null, new EditorUtility.SelectMenuItemFunction(this.AddVRDeviceMenuSelected), target);
        }

        private void AddVRDeviceMenuSelected(object userData, string[] options, int selected)
        {
            string str;
            BuildTargetGroup targetGroup = (BuildTargetGroup) ((int) userData);
            List<string> list = VREditor.GetVREnabledDevices(targetGroup).ToList<string>();
            if (!this.m_MapVRUIStringToDeviceKey.TryGetValue(options[selected], out str))
            {
                str = options[selected];
            }
            list.Add(str);
            this.ApplyChangedVRDeviceList(targetGroup, list.ToArray());
        }

        private void ApplyChangedVRDeviceList(BuildTargetGroup target, string[] devices)
        {
            if (this.m_VRDeviceActiveUI.ContainsKey(target))
            {
                VREditor.SetVREnabledDevices(target, devices);
                this.m_VRDeviceActiveUI[target].list = devices;
            }
        }

        internal void DevicesGUI(BuildTargetGroup targetGroup)
        {
            if (this.TargetGroupSupportsVirtualReality(targetGroup))
            {
                bool vREnabled = VREditor.GetVREnabled(targetGroup);
                EditorGUI.BeginChangeCheck();
                vREnabled = EditorGUILayout.Toggle(EditorGUIUtility.TextContent("Virtual Reality Supported"), vREnabled, new GUILayoutOption[0]);
                if (EditorGUI.EndChangeCheck())
                {
                    VREditor.SetVREnabled(targetGroup, vREnabled);
                }
                if (vREnabled)
                {
                    this.VRDevicesGUIOneBuildTarget(targetGroup);
                }
            }
        }

        private void DrawVRDeviceElement(BuildTargetGroup target, Rect rect, int index, bool selected, bool focused)
        {
            string str2;
            string key = (string) this.m_VRDeviceActiveUI[target].list[index];
            if (!this.m_MapVRDeviceKeyToUIString.TryGetValue(key, out str2))
            {
                str2 = key + " (missing from build)";
            }
            GUI.Label(rect, str2, EditorStyles.label);
        }

        private void RefreshVRDeviceList(BuildTargetGroup targetGroup)
        {
            VRDeviceInfoEditor[] allVRDeviceInfo = VREditor.GetAllVRDeviceInfo(targetGroup);
            this.m_AllVRDevicesForBuildTarget[targetGroup] = allVRDeviceInfo;
            for (int i = 0; i < allVRDeviceInfo.Length; i++)
            {
                VRDeviceInfoEditor editor = allVRDeviceInfo[i];
                this.m_MapVRDeviceKeyToUIString[editor.deviceNameKey] = editor.deviceNameUI;
                this.m_MapVRUIStringToDeviceKey[editor.deviceNameUI] = editor.deviceNameKey;
            }
        }

        private void RemoveVRDeviceElement(BuildTargetGroup target, ReorderableList list)
        {
            List<string> list2 = VREditor.GetVREnabledDevices(target).ToList<string>();
            list2.RemoveAt(list.index);
            this.ApplyChangedVRDeviceList(target, list2.ToArray());
        }

        private void ReorderVRDeviceElement(BuildTargetGroup target, ReorderableList list)
        {
            string[] devices = list.list.Cast<string>().ToArray<string>();
            this.ApplyChangedVRDeviceList(target, devices);
        }

        internal bool TargetGroupSupportsVirtualReality(BuildTargetGroup targetGroup)
        {
            if (!this.m_AllVRDevicesForBuildTarget.ContainsKey(targetGroup))
            {
                this.RefreshVRDeviceList(targetGroup);
            }
            VRDeviceInfoEditor[] editorArray = this.m_AllVRDevicesForBuildTarget[targetGroup];
            return (editorArray.Length > 0);
        }

        private void VRDevicesGUIOneBuildTarget(BuildTargetGroup targetGroup)
        {
            <VRDevicesGUIOneBuildTarget>c__AnonStorey30 storey = new <VRDevicesGUIOneBuildTarget>c__AnonStorey30 {
                targetGroup = targetGroup,
                <>f__this = this
            };
            if (!this.m_VRDeviceActiveUI.ContainsKey(storey.targetGroup))
            {
                ReorderableList list = new ReorderableList(VREditor.GetVREnabledDevices(storey.targetGroup), typeof(VRDeviceInfoEditor), true, true, true, true) {
                    onAddDropdownCallback = new ReorderableList.AddDropdownCallbackDelegate(storey.<>m__48),
                    onRemoveCallback = new ReorderableList.RemoveCallbackDelegate(storey.<>m__49),
                    onReorderCallback = new ReorderableList.ReorderCallbackDelegate(storey.<>m__4A),
                    drawElementCallback = new ReorderableList.ElementCallbackDelegate(storey.<>m__4B)
                };
                if (<>f__am$cache5 == null)
                {
                    <>f__am$cache5 = rect => GUI.Label(rect, "Virtual Reality SDKs", EditorStyles.label);
                }
                list.drawHeaderCallback = <>f__am$cache5;
                this.m_VRDeviceActiveUI.Add(storey.targetGroup, list);
            }
            this.m_VRDeviceActiveUI[storey.targetGroup].DoLayoutList();
            if (this.m_VRDeviceActiveUI[storey.targetGroup].list.Count == 0)
            {
                EditorGUILayout.HelpBox("Must add at least one Virtual Reality SDK.", MessageType.Warning);
            }
        }

        [CompilerGenerated]
        private sealed class <AddVRDeviceElement>c__AnonStorey2E
        {
            internal List<string> enabledDevices;

            internal bool <>m__47(VRDeviceInfoEditor d)
            {
                <AddVRDeviceElement>c__AnonStorey2F storeyf = new <AddVRDeviceElement>c__AnonStorey2F {
                    <>f__ref$46 = this,
                    d = d
                };
                return !this.enabledDevices.Any<string>(new Func<string, bool>(storeyf.<>m__4D));
            }

            private sealed class <AddVRDeviceElement>c__AnonStorey2F
            {
                internal PlayerSettingsEditorVR.<AddVRDeviceElement>c__AnonStorey2E <>f__ref$46;
                internal VRDeviceInfoEditor d;

                internal bool <>m__4D(string enabledDeviceName)
                {
                    return (this.d.deviceNameKey == enabledDeviceName);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <VRDevicesGUIOneBuildTarget>c__AnonStorey30
        {
            internal PlayerSettingsEditorVR <>f__this;
            internal BuildTargetGroup targetGroup;

            internal void <>m__48(Rect rect, ReorderableList list)
            {
                this.<>f__this.AddVRDeviceElement(this.targetGroup, rect, list);
            }

            internal void <>m__49(ReorderableList list)
            {
                this.<>f__this.RemoveVRDeviceElement(this.targetGroup, list);
            }

            internal void <>m__4A(ReorderableList list)
            {
                this.<>f__this.ReorderVRDeviceElement(this.targetGroup, list);
            }

            internal void <>m__4B(Rect rect, int index, bool isActive, bool isFocused)
            {
                this.<>f__this.DrawVRDeviceElement(this.targetGroup, rect, index, isActive, isFocused);
            }
        }
    }
}

