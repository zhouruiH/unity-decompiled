﻿namespace UnityEditorInternal
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Il2CppNativeCodeBuilderUtils
    {
        public static IEnumerable<string> AddBuilderArguments(Il2CppNativeCodeBuilder builder, string outputRelativePath, IEnumerable<string> includeRelativePaths)
        {
            List<string> list = new List<string> {
                "--compile-cpp",
                "--libil2cpp-static",
                FormatArgument("platform", builder.CompilerPlatform),
                FormatArgument("architecture", builder.CompilerArchitecture),
                FormatArgument("configuration", "Release"),
                FormatArgument("outputpath", builder.ConvertOutputFileToFullPath(outputRelativePath))
            };
            if (!string.IsNullOrEmpty(builder.CacheDirectory))
            {
                list.Add(FormatArgument("cachedirectory", CacheDirectoryPathFor(builder.CacheDirectory)));
            }
            if (!string.IsNullOrEmpty(builder.CompilerFlags))
            {
                list.Add(FormatArgument("compiler-flags", builder.CompilerFlags));
            }
            if (!string.IsNullOrEmpty(builder.LinkerFlags))
            {
                list.Add(FormatArgument("linker-flags", builder.LinkerFlags));
            }
            if (!string.IsNullOrEmpty(builder.PluginPath))
            {
                list.Add(FormatArgument("plugin", builder.PluginPath));
            }
            string[] second = new string[] { outputRelativePath };
            IEnumerator<string> enumerator = builder.ConvertIncludesToFullPaths(includeRelativePaths.Concat<string>(second)).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    string current = enumerator.Current;
                    list.Add(FormatArgument("additional-include-directories", current));
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return list;
        }

        private static string CacheDirectoryPathFor(string builderCacheDirectory)
        {
            return (builderCacheDirectory + "/il2cpp_cache");
        }

        public static void ClearCacheIfEditorVersionDiffers(Il2CppNativeCodeBuilder builder, string currentEditorVersion)
        {
            string path = CacheDirectoryPathFor(builder.CacheDirectory);
            if (Directory.Exists(path) && !File.Exists(Path.Combine(path, currentEditorVersion)))
            {
                Directory.Delete(path, true);
            }
        }

        private static string FormatArgument(string name, string value)
        {
            return string.Format("--{0}=\"{1}\"", name, value);
        }

        public static string ObjectFilePathInCacheDirectoryFor(string builderCacheDirectory)
        {
            return (CacheDirectoryPathFor(builderCacheDirectory) + "/objectfiles");
        }

        public static void PrepareCacheDirectory(Il2CppNativeCodeBuilder builder, string currentEditorVersion)
        {
            string path = CacheDirectoryPathFor(builder.CacheDirectory);
            Directory.CreateDirectory(path);
            string str2 = Path.Combine(path, currentEditorVersion);
            if (!File.Exists(str2))
            {
                File.Create(str2).Dispose();
            }
        }
    }
}

