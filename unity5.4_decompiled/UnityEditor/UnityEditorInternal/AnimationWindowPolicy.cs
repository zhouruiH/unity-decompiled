﻿namespace UnityEditorInternal
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    internal class AnimationWindowPolicy
    {
        [CompilerGenerated]
        private static OnGeometryChangeDelegate <>f__am$cache10;
        [CompilerGenerated]
        private static OnCurrentTimeChangeDelegate <>f__am$cache11;
        [CompilerGenerated]
        private static OnZoomableAreaChangeDelegate <>f__am$cache12;
        [CompilerGenerated]
        private static SynchronizeGeometryDelegate <>f__am$cacheC;
        [CompilerGenerated]
        private static SynchronizeFrameRateDelegate <>f__am$cacheD;
        [CompilerGenerated]
        private static SynchronizeCurrentTimeDelegate <>f__am$cacheE;
        [CompilerGenerated]
        private static SynchronizeZoomableAreaDelegate <>f__am$cacheF;
        [SerializeField]
        public bool allowAddEvent = true;
        [SerializeField]
        public bool allowRecording = true;
        public OnCurrentTimeChangeDelegate OnCurrentTimeChange;
        public OnGeometryChangeDelegate OnGeometryChange;
        public OnZoomableAreaChangeDelegate OnZoomableAreaChange;
        public SynchronizeCurrentTimeDelegate SynchronizeCurrentTime;
        public SynchronizeFrameRateDelegate SynchronizeFrameRate;
        public SynchronizeGeometryDelegate SynchronizeGeometry;
        public SynchronizeZoomableAreaDelegate SynchronizeZoomableArea;
        [SerializeField]
        public bool triggerFramingOnSelection = true;
        [NonSerialized]
        public bool unitialized = true;
        [SerializeField]
        public Views views = Views.All;

        public AnimationWindowPolicy()
        {
            if (<>f__am$cacheC == null)
            {
                <>f__am$cacheC = new SynchronizeGeometryDelegate(AnimationWindowPolicy.<AnimationWindowPolicy>m__85);
            }
            this.SynchronizeGeometry = (SynchronizeGeometryDelegate) Delegate.Combine(this.SynchronizeGeometry, <>f__am$cacheC);
            if (<>f__am$cacheD == null)
            {
                <>f__am$cacheD = new SynchronizeFrameRateDelegate(AnimationWindowPolicy.<AnimationWindowPolicy>m__86);
            }
            this.SynchronizeFrameRate = (SynchronizeFrameRateDelegate) Delegate.Combine(this.SynchronizeFrameRate, <>f__am$cacheD);
            if (<>f__am$cacheE == null)
            {
                <>f__am$cacheE = new SynchronizeCurrentTimeDelegate(AnimationWindowPolicy.<AnimationWindowPolicy>m__87);
            }
            this.SynchronizeCurrentTime = (SynchronizeCurrentTimeDelegate) Delegate.Combine(this.SynchronizeCurrentTime, <>f__am$cacheE);
            if (<>f__am$cacheF == null)
            {
                <>f__am$cacheF = new SynchronizeZoomableAreaDelegate(AnimationWindowPolicy.<AnimationWindowPolicy>m__88);
            }
            this.SynchronizeZoomableArea = (SynchronizeZoomableAreaDelegate) Delegate.Combine(this.SynchronizeZoomableArea, <>f__am$cacheF);
            if (<>f__am$cache10 == null)
            {
                <>f__am$cache10 = new OnGeometryChangeDelegate(AnimationWindowPolicy.<AnimationWindowPolicy>m__89);
            }
            this.OnGeometryChange = (OnGeometryChangeDelegate) Delegate.Combine(this.OnGeometryChange, <>f__am$cache10);
            if (<>f__am$cache11 == null)
            {
                <>f__am$cache11 = new OnCurrentTimeChangeDelegate(AnimationWindowPolicy.<AnimationWindowPolicy>m__8A);
            }
            this.OnCurrentTimeChange = (OnCurrentTimeChangeDelegate) Delegate.Combine(this.OnCurrentTimeChange, <>f__am$cache11);
            if (<>f__am$cache12 == null)
            {
                <>f__am$cache12 = new OnZoomableAreaChangeDelegate(AnimationWindowPolicy.<AnimationWindowPolicy>m__8B);
            }
            this.OnZoomableAreaChange = (OnZoomableAreaChangeDelegate) Delegate.Combine(this.OnZoomableAreaChange, <>f__am$cache12);
        }

        [CompilerGenerated]
        private static bool <AnimationWindowPolicy>m__85(ref int[] sizes, ref int[] minSizes)
        {
            return false;
        }

        [CompilerGenerated]
        private static bool <AnimationWindowPolicy>m__86(ref float frameRate, ref bool timeInFrame)
        {
            return false;
        }

        [CompilerGenerated]
        private static bool <AnimationWindowPolicy>m__87(ref float time)
        {
            return false;
        }

        [CompilerGenerated]
        private static bool <AnimationWindowPolicy>m__88(ref float horizontalScale, ref float horizontalTranslation)
        {
            return false;
        }

        [CompilerGenerated]
        private static void <AnimationWindowPolicy>m__89(int[] sizes)
        {
        }

        [CompilerGenerated]
        private static void <AnimationWindowPolicy>m__8A(float time)
        {
        }

        [CompilerGenerated]
        private static void <AnimationWindowPolicy>m__8B(float horizontalScale, float horizontalTranslation)
        {
        }

        public delegate void OnCurrentTimeChangeDelegate(float time);

        public delegate void OnGeometryChangeDelegate(int[] sizes);

        public delegate void OnZoomableAreaChangeDelegate(float horizontalScale, float horizontalTranslation);

        public delegate bool SynchronizeCurrentTimeDelegate(ref float time);

        public delegate bool SynchronizeFrameRateDelegate(ref float frameRate, ref bool timeInFrame);

        public delegate bool SynchronizeGeometryDelegate(ref int[] sizes, ref int[] minSizes);

        public delegate bool SynchronizeZoomableAreaDelegate(ref float horizontalScale, ref float horizontalTranslation);

        [Flags]
        public enum Views
        {
            None,
            DopeSheet,
            CurveEditor,
            All
        }
    }
}

