﻿namespace UnityEditorInternal
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    public abstract class Il2CppNativeCodeBuilder
    {
        protected Il2CppNativeCodeBuilder()
        {
        }

        public virtual IEnumerable<string> ConvertIncludesToFullPaths(IEnumerable<string> relativeIncludePaths)
        {
            return relativeIncludePaths;
        }

        public virtual string ConvertOutputFileToFullPath(string outputFileRelativePath)
        {
            return outputFileRelativePath;
        }

        protected virtual void SetupEnvironment(ProcessStartInfo startInfo)
        {
        }

        public void SetupStartInfo(ProcessStartInfo startInfo)
        {
            if (this.SetsUpEnvironment)
            {
                this.SetupEnvironment(startInfo);
            }
        }

        public virtual string CacheDirectory
        {
            get
            {
                return string.Empty;
            }
        }

        public abstract string CompilerArchitecture { get; }

        public virtual string CompilerFlags
        {
            get
            {
                return string.Empty;
            }
        }

        public abstract string CompilerPlatform { get; }

        public virtual string LinkerFlags
        {
            get
            {
                return string.Empty;
            }
        }

        public virtual string PluginPath
        {
            get
            {
                return string.Empty;
            }
        }

        public virtual bool SetsUpEnvironment
        {
            get
            {
                return false;
            }
        }
    }
}

