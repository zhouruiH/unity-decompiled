﻿namespace UnityEditorInternal
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEditor;
    using UnityEngine;

    [Serializable]
    internal class DopeSheetEditor : TimeArea, CurveUpdater
    {
        [CompilerGenerated]
        private static Comparison<Object> <>f__am$cache11;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$mapD;
        private List<DrawElement> dragdropKeysDrawBuffer;
        private const float k_KeyframeOffset = -5.5f;
        private const float k_PptrKeyframeOffset = -1f;
        public Bounds m_Bounds;
        private Texture m_DefaultDopeKeyIcon;
        private float m_DragStartTime;
        private bool m_Initialized;
        private bool m_IsDragging;
        private bool m_IsDraggingPlayhead;
        private bool m_IsDraggingPlayheadStarted;
        private bool m_MousedownOnKeyframe;
        [SerializeField]
        public EditorWindow m_Owner;
        private DopeSheetSelectionRect m_SelectionRect;
        public int m_SpritePreviewCacheSize;
        public bool m_SpritePreviewLoading;
        private List<DrawElement> selectedKeysDrawBuffer;
        public AnimationWindowState state;
        private List<DrawElement> unselectedKeysDrawBuffer;

        public DopeSheetEditor(EditorWindow owner) : base(false)
        {
            this.m_Bounds = new Bounds(Vector3.zero, Vector3.zero);
            this.m_Owner = owner;
        }

        private void AddKeyToDopeline(object obj)
        {
            this.AddKeyToDopeline((DopeLine) obj);
        }

        private void AddKeyToDopeline(DopeLine dopeLine)
        {
            this.state.ClearSelections();
            foreach (AnimationWindowCurve curve in dopeLine.m_Curves)
            {
                AnimationWindowKeyframe keyframe = AnimationWindowUtility.AddKeyframeToCurve(this.state, curve, this.state.time);
                this.state.SelectKey(keyframe);
            }
        }

        private bool AnyKeyIsSelectedAtTime(DopeLine dopeLine, int keyIndex)
        {
            AnimationWindowKeyframe keyframe = dopeLine.keys[keyIndex];
            int num = keyframe.m_TimeHash ^ keyframe.curve.timeOffset.GetHashCode();
            int count = dopeLine.keys.Count;
            for (int i = keyIndex; i < count; i++)
            {
                keyframe = dopeLine.keys[i];
                int num4 = keyframe.m_TimeHash ^ keyframe.curve.timeOffset.GetHashCode();
                if (num4 != num)
                {
                    return false;
                }
                if (this.state.KeyIsSelected(keyframe))
                {
                    return true;
                }
            }
            return false;
        }

        private EditorCurveBinding? CreateNewPptrDopeline(GameObject targetGameObject, GameObject rootGameObject, AnimationClip animationClip, Type valueType)
        {
            List<EditorCurveBinding> list = AnimationWindowUtility.GetAnimatableProperties(targetGameObject, rootGameObject, valueType);
            if ((list.Count == 0) && (valueType == typeof(Sprite)))
            {
                return this.CreateNewSpriteRendererDopeline(targetGameObject, rootGameObject);
            }
            if (list.Count == 1)
            {
                return new EditorCurveBinding?(list[0]);
            }
            List<string> list2 = new List<string>();
            foreach (EditorCurveBinding binding in list)
            {
                list2.Add(binding.type.Name);
            }
            List<object> userData = new List<object> {
                animationClip,
                list
            };
            Rect position = new Rect(Event.current.mousePosition.x, Event.current.mousePosition.y, 1f, 1f);
            EditorUtility.DisplayCustomMenu(position, EditorGUIUtility.TempContent(list2.ToArray()), -1, new EditorUtility.SelectMenuItemFunction(this.SelectTypeForCreatingNewPptrDopeline), userData);
            return null;
        }

        private void CreateNewPPtrKeyframe(float time, Object value, AnimationWindowCurve targetCurve)
        {
            ObjectReferenceKeyframe key = new ObjectReferenceKeyframe {
                time = time,
                value = value
            };
            AnimationWindowKeyframe keyframe2 = new AnimationWindowKeyframe(targetCurve, key);
            AnimationKeyTime keyTime = AnimationKeyTime.Time(keyframe2.time, this.state.frameRate);
            targetCurve.AddKeyframe(keyframe2, keyTime);
            this.state.SelectKey(keyframe2);
        }

        private EditorCurveBinding? CreateNewSpriteRendererDopeline(GameObject targetGameObject, GameObject rootGameObject)
        {
            if (targetGameObject.GetComponent<SpriteRenderer>() == null)
            {
                targetGameObject.AddComponent<SpriteRenderer>();
            }
            List<EditorCurveBinding> list = AnimationWindowUtility.GetAnimatableProperties(targetGameObject, rootGameObject, typeof(SpriteRenderer), typeof(Sprite));
            if (list.Count == 1)
            {
                return new EditorCurveBinding?(list[0]);
            }
            Debug.LogError("Unable to create animatable SpriteRenderer component");
            return null;
        }

        private void DeleteSelectedKeys()
        {
            this.state.DeleteSelectedKeys();
        }

        private bool DoDragAndDrop(DopeLine dopeLine, Rect position, bool perform)
        {
            if (!position.Contains(Event.current.mousePosition))
            {
                return false;
            }
            if (!ValidateDragAndDropObjects())
            {
                return false;
            }
            Type type = DragAndDrop.objectReferences[0].GetType();
            AnimationWindowCurve curve = null;
            if (dopeLine.valueType == type)
            {
                curve = dopeLine.m_Curves[0];
            }
            else
            {
                foreach (AnimationWindowCurve curve2 in dopeLine.m_Curves)
                {
                    if (curve2.isPPtrCurve)
                    {
                        if (curve2.m_ValueType == type)
                        {
                            curve = curve2;
                        }
                        Sprite[] spriteFromDraggedPathsOrObjects = SpriteUtility.GetSpriteFromDraggedPathsOrObjects();
                        if (((curve2.m_ValueType == typeof(Sprite)) && (spriteFromDraggedPathsOrObjects != null)) && (spriteFromDraggedPathsOrObjects.Length > 0))
                        {
                            curve = curve2;
                            type = typeof(Sprite);
                        }
                    }
                }
            }
            if (curve == null)
            {
                return false;
            }
            if (!curve.clipIsEditable)
            {
                return false;
            }
            if (perform)
            {
                if (DragAndDrop.objectReferences.Length == 1)
                {
                    Analytics.Event("Sprite Drag and Drop", "Drop single sprite into existing dopeline", "null", 1);
                }
                else
                {
                    Analytics.Event("Sprite Drag and Drop", "Drop multiple sprites into existing dopeline", "null", 1);
                }
                float time = Mathf.Max(this.state.PixelToTime(Event.current.mousePosition.x, AnimationWindowState.SnapMode.SnapToClipFrame), 0f);
                AnimationWindowCurve curveOfType = this.GetCurveOfType(dopeLine, type);
                this.PerformDragAndDrop(curveOfType, time);
            }
            return true;
        }

        private bool DopelineForValueTypeExists(Type valueType)
        {
            <DopelineForValueTypeExists>c__AnonStorey56 storey = new <DopelineForValueTypeExists>c__AnonStorey56 {
                valueType = valueType
            };
            return this.state.allCurves.Exists(new Predicate<AnimationWindowCurve>(storey.<>m__A6));
        }

        private void DopeLineRepaint(DopeLine dopeline)
        {
            Color color = GUI.color;
            AnimationWindowHierarchyNode node = (AnimationWindowHierarchyNode) this.state.hierarchyData.FindItem(dopeline.m_HierarchyNodeID);
            Color color2 = ((node == null) || (node.depth <= 0)) ? Color.gray.AlphaMultiplied(0.16f) : Color.gray.AlphaMultiplied(0.05f);
            if (!dopeline.isMasterDopeline)
            {
                DrawBox(dopeline.position, color2);
            }
            int? nullable = null;
            int count = dopeline.keys.Count;
            for (int i = 0; i < count; i++)
            {
                AnimationWindowKeyframe keyframe = dopeline.keys[i];
                int num3 = keyframe.m_TimeHash ^ keyframe.curve.timeOffset.GetHashCode();
                if ((nullable.GetValueOrDefault() != num3) || !nullable.HasValue)
                {
                    nullable = new int?(num3);
                    Rect keyframeRect = this.GetKeyframeRect(dopeline, keyframe);
                    color2 = !dopeline.isMasterDopeline ? Color.gray.RGBMultiplied((float) 1.2f) : Color.gray.RGBMultiplied((float) 0.85f);
                    Texture2D texture = null;
                    if (keyframe.isPPtrCurve && dopeline.tallMode)
                    {
                        texture = (keyframe.value != null) ? AssetPreview.GetAssetPreview(((Object) keyframe.value).GetInstanceID(), this.assetPreviewManagerID) : null;
                    }
                    if (texture != null)
                    {
                        keyframeRect = this.GetPreviewRectFromKeyFrameRect(keyframeRect);
                        color2 = Color.white.AlphaMultiplied(0.5f);
                    }
                    else if (((keyframe.value != null) && keyframe.isPPtrCurve) && dopeline.tallMode)
                    {
                        this.m_SpritePreviewLoading = true;
                    }
                    if (Mathf.Approximately(keyframe.time, 0f))
                    {
                        keyframeRect.xMin -= 0.01f;
                    }
                    if (this.AnyKeyIsSelectedAtTime(dopeline, i))
                    {
                        color2 = (!dopeline.tallMode || !dopeline.isPptrDopeline) ? new Color(0.34f, 0.52f, 0.85f, 1f) : Color.white;
                        if (dopeline.isMasterDopeline)
                        {
                            color2 = color2.RGBMultiplied((float) 0.85f);
                        }
                        this.selectedKeysDrawBuffer.Add(new DrawElement(keyframeRect, color2, texture));
                    }
                    else
                    {
                        this.unselectedKeysDrawBuffer.Add(new DrawElement(keyframeRect, color2, texture));
                    }
                }
            }
            if (this.DoDragAndDrop(dopeline, dopeline.position, false))
            {
                float time = Mathf.Max(this.state.PixelToTime(Event.current.mousePosition.x, AnimationWindowState.SnapMode.SnapToClipFrame), 0f);
                Color color3 = Color.gray.RGBMultiplied((float) 1.2f);
                Texture2D assetPreview = null;
                foreach (Object obj2 in this.GetSortedDragAndDropObjectReferences())
                {
                    Rect dragAndDropRect = this.GetDragAndDropRect(dopeline, time);
                    if (dopeline.isPptrDopeline && dopeline.tallMode)
                    {
                        assetPreview = AssetPreview.GetAssetPreview(obj2.GetInstanceID(), this.assetPreviewManagerID);
                    }
                    if (assetPreview != null)
                    {
                        dragAndDropRect = this.GetPreviewRectFromKeyFrameRect(dragAndDropRect);
                        color3 = Color.white.AlphaMultiplied(0.5f);
                    }
                    this.dragdropKeysDrawBuffer.Add(new DrawElement(dragAndDropRect, color3, assetPreview));
                    time += 1f / this.state.frameRate;
                }
            }
            GUI.color = color;
        }

        private Rect DopelinesGUI(Rect position, Vector2 scrollPosition)
        {
            Color color = GUI.color;
            Rect rect = position;
            this.selectedKeysDrawBuffer = new List<DrawElement>();
            this.unselectedKeysDrawBuffer = new List<DrawElement>();
            this.dragdropKeysDrawBuffer = new List<DrawElement>();
            if (Event.current.type == EventType.Repaint)
            {
                this.m_SpritePreviewLoading = false;
            }
            if (Event.current.type == EventType.MouseDown)
            {
                this.m_IsDragging = false;
            }
            this.UpdateSpritePreviewCacheSize();
            foreach (DopeLine line in this.state.dopelines)
            {
                line.position = rect;
                line.position.height = !line.tallMode ? 16f : 32f;
                if ((((line.position.yMin + scrollPosition.y) >= position.yMin) && ((line.position.yMin + scrollPosition.y) <= position.yMax)) || (((line.position.yMax + scrollPosition.y) >= position.yMin) && ((line.position.yMax + scrollPosition.y) <= position.yMax)))
                {
                    Event current = Event.current;
                    EventType type = current.type;
                    switch (type)
                    {
                        case EventType.Repaint:
                            this.DopeLineRepaint(line);
                            goto Label_01A7;

                        case EventType.DragUpdated:
                        case EventType.DragPerform:
                            this.HandleDragAndDrop(line);
                            goto Label_01A7;
                    }
                    if (type != EventType.MouseDown)
                    {
                        if ((type == EventType.ContextClick) && !this.m_IsDraggingPlayhead)
                        {
                            this.HandleContextMenu(line);
                        }
                    }
                    else if (current.button == 0)
                    {
                        this.HandleMouseDown(line);
                    }
                }
            Label_01A7:
                rect.y += line.position.height;
            }
            if (Event.current.type == EventType.MouseUp)
            {
                this.m_IsDraggingPlayheadStarted = false;
                this.m_IsDraggingPlayhead = false;
            }
            Rect rect2 = new Rect(position.xMin, position.yMin, position.width, rect.yMax - position.yMin);
            this.DrawElements(this.unselectedKeysDrawBuffer);
            this.DrawElements(this.selectedKeysDrawBuffer);
            this.DrawElements(this.dragdropKeysDrawBuffer);
            GUI.color = color;
            return rect2;
        }

        private void DoSpriteDropAfterGeneratingNewDopeline(AnimationClip animationClip, EditorCurveBinding? spriteBinding)
        {
            if (DragAndDrop.objectReferences.Length == 1)
            {
                Analytics.Event("Sprite Drag and Drop", "Drop single sprite into empty dopesheet", "null", 1);
            }
            else
            {
                Analytics.Event("Sprite Drag and Drop", "Drop multiple sprites into empty dopesheet", "null", 1);
            }
            AnimationWindowCurve curve = new AnimationWindowCurve(animationClip, spriteBinding.Value, typeof(Sprite));
            this.state.SaveCurve(curve);
            this.PerformDragAndDrop(curve, 0f);
        }

        private static void DrawBox(Rect position, Color color)
        {
            Color color2 = GUI.color;
            GUI.color = color;
            DopeLine.dopekeyStyle.Draw(position, GUIContent.none, 0, false);
            GUI.color = color2;
        }

        private void DrawElements(List<DrawElement> elements)
        {
            Color color = GUI.color;
            Color white = Color.white;
            GUI.color = white;
            Texture defaultDopeKeyIcon = this.m_DefaultDopeKeyIcon;
            foreach (DrawElement element in elements)
            {
                if (element.color != white)
                {
                    white = !GUI.enabled ? ((Color) (element.color * 0.8f)) : element.color;
                    GUI.color = white;
                }
                if (element.texture != null)
                {
                    GUI.DrawTexture(element.position, element.texture);
                }
                else
                {
                    Rect position = new Rect(element.position.center.x - (defaultDopeKeyIcon.width / 2), element.position.center.y - (defaultDopeKeyIcon.height / 2), (float) defaultDopeKeyIcon.width, (float) defaultDopeKeyIcon.height);
                    GUI.DrawTexture(position, defaultDopeKeyIcon, ScaleMode.ScaleToFit, true, 1f);
                }
            }
            GUI.color = color;
        }

        private void DrawGrid(Rect position)
        {
            base.TimeRuler(position, this.state.frameRate, false, true, 0.2f);
        }

        public void DrawMasterDopelineBackground(Rect position)
        {
            if (Event.current.type == EventType.Repaint)
            {
                AnimationWindowStyles.eventBackground.Draw(position, false, false, false, false);
            }
        }

        public void FrameClip()
        {
            if (!this.state.disabled)
            {
                Vector2 timeRange = this.state.timeRange;
                timeRange.y = Mathf.Max(timeRange.x + 0.1f, timeRange.y);
                base.SetShownHRangeInsideMargins(timeRange.x, timeRange.y);
            }
        }

        public void FrameSelected()
        {
            Bounds bounds = new Bounds();
            bool flag = true;
            bool flag2 = this.state.selectedKeys.Count > 0;
            if (flag2)
            {
                foreach (AnimationWindowKeyframe keyframe in this.state.selectedKeys)
                {
                    Vector2 vector = new Vector2(keyframe.time + keyframe.curve.timeOffset, 0f);
                    if (flag)
                    {
                        bounds.SetMinMax((Vector3) vector, (Vector3) vector);
                        flag = false;
                    }
                    else
                    {
                        bounds.Encapsulate((Vector3) vector);
                    }
                }
            }
            bool flag3 = !flag2;
            if (!flag2 && (this.state.hierarchyState.selectedIDs.Count > 0))
            {
                foreach (AnimationWindowCurve curve in this.state.activeCurves)
                {
                    int count = curve.m_Keyframes.Count;
                    if (count > 1)
                    {
                        Vector2 vector2 = new Vector2(curve.m_Keyframes[0].time + curve.timeOffset, 0f);
                        Vector2 vector3 = new Vector2(curve.m_Keyframes[count - 1].time + curve.timeOffset, 0f);
                        if (flag)
                        {
                            bounds.SetMinMax((Vector3) vector2, (Vector3) vector3);
                            flag = false;
                        }
                        else
                        {
                            bounds.Encapsulate((Vector3) vector2);
                            bounds.Encapsulate((Vector3) vector3);
                        }
                        flag3 = false;
                    }
                }
            }
            if (flag3)
            {
                this.FrameClip();
            }
            else
            {
                float x = Mathf.Max(bounds.size.x, 0.1f);
                bounds.size = new Vector3(x, Mathf.Max(bounds.size.y, 0.1f), 0f);
                base.SetShownHRangeInsideMargins(bounds.min.x, bounds.max.x);
            }
        }

        private GenericMenu GenerateMenu(DopeLine dopeline, bool clickedEmpty)
        {
            GenericMenu menu = new GenericMenu();
            this.state.recording = true;
            this.state.ResampleAnimation();
            string text = "Add Key";
            if ((dopeline.isEditable && clickedEmpty) && this.state.canRecord)
            {
                menu.AddItem(new GUIContent(text), false, new GenericMenu.MenuFunction2(this.AddKeyToDopeline), dopeline);
            }
            else
            {
                menu.AddDisabledItem(new GUIContent(text));
            }
            text = (this.state.selectedKeys.Count <= 1) ? "Delete Key" : "Delete Keys";
            if ((dopeline.isEditable && (this.state.selectedKeys.Count > 0)) && this.state.canRecord)
            {
                menu.AddItem(new GUIContent(text), false, new GenericMenu.MenuFunction(this.DeleteSelectedKeys));
            }
            else
            {
                menu.AddDisabledItem(new GUIContent(text));
            }
            if (dopeline.isEditable && AnimationWindowUtility.ContainsFloatKeyframes(this.state.selectedKeys))
            {
                menu.AddSeparator(string.Empty);
                List<KeyIdentifier> keyList = new List<KeyIdentifier>();
                Hashtable hashtable = new Hashtable();
                foreach (AnimationWindowKeyframe keyframe in this.state.selectedKeys)
                {
                    if (!keyframe.isPPtrCurve)
                    {
                        int keyframeIndex = keyframe.curve.GetKeyframeIndex(AnimationKeyTime.Time(keyframe.time, this.state.frameRate));
                        if (keyframeIndex != -1)
                        {
                            int curveID = keyframe.curve.GetCurveID();
                            AnimationCurve editorCurve = (AnimationCurve) hashtable[curveID];
                            if (editorCurve == null)
                            {
                                editorCurve = AnimationUtility.GetEditorCurve(keyframe.curve.clip, keyframe.curve.binding);
                                if (editorCurve == null)
                                {
                                    editorCurve = new AnimationCurve();
                                }
                                hashtable.Add(curveID, editorCurve);
                            }
                            keyList.Add(new KeyIdentifier(editorCurve, curveID, keyframeIndex, keyframe.curve.binding));
                        }
                    }
                }
                new CurveMenuManager(this).AddTangentMenuItems(menu, keyList);
            }
            return menu;
        }

        private AnimationWindowCurve GetCurveOfType(DopeLine dopeLine, Type type)
        {
            foreach (AnimationWindowCurve curve in dopeLine.m_Curves)
            {
                if (curve.m_ValueType == type)
                {
                    return curve;
                }
            }
            return null;
        }

        private Rect GetDragAndDropRect(DopeLine dopeline, float time)
        {
            Rect keyframeRect = this.GetKeyframeRect(dopeline, null);
            float keyframeOffset = this.GetKeyframeOffset(dopeline, null);
            keyframeRect.center = new Vector2((this.state.TimeToPixel(time) + (keyframeRect.width * 0.5f)) + keyframeOffset, keyframeRect.center.y);
            return keyframeRect;
        }

        private float GetKeyframeOffset(DopeLine dopeline, AnimationWindowKeyframe keyframe)
        {
            if ((!dopeline.isPptrDopeline || !dopeline.tallMode) || ((keyframe != null) && (keyframe.value == null)))
            {
                return -5.5f;
            }
            return -1f;
        }

        private Rect GetKeyframeRect(DopeLine dopeline, AnimationWindowKeyframe keyframe)
        {
            float time = (keyframe == null) ? 0f : (keyframe.time + keyframe.curve.timeOffset);
            float width = 10f;
            if ((dopeline.isPptrDopeline && dopeline.tallMode) && ((keyframe == null) || (keyframe.value != null)))
            {
                width = dopeline.position.height;
            }
            if (dopeline.isPptrDopeline && dopeline.tallMode)
            {
                return new Rect(this.state.TimeToPixel(this.state.SnapToFrame(time, AnimationWindowState.SnapMode.SnapToClipFrame)) + this.GetKeyframeOffset(dopeline, keyframe), dopeline.position.yMin, width, dopeline.position.height);
            }
            return new Rect(this.state.TimeToPixel(this.state.SnapToFrame(time, AnimationWindowState.SnapMode.SnapToClipFrame)) + this.GetKeyframeOffset(dopeline, keyframe), dopeline.position.yMin, width, dopeline.position.height);
        }

        private Rect GetPreviewRectFromKeyFrameRect(Rect keyframeRect)
        {
            keyframeRect.width -= 2f;
            keyframeRect.height -= 2f;
            keyframeRect.xMin += 2f;
            keyframeRect.yMin += 2f;
            return keyframeRect;
        }

        private Object[] GetSortedDragAndDropObjectReferences()
        {
            Object[] objectReferences = DragAndDrop.objectReferences;
            if (<>f__am$cache11 == null)
            {
                <>f__am$cache11 = (a, b) => EditorUtility.NaturalCompare(a.name, b.name);
            }
            Array.Sort<Object>(objectReferences, <>f__am$cache11);
            return objectReferences;
        }

        private void HandleContextMenu(DopeLine dopeline)
        {
            if (dopeline.position.Contains(Event.current.mousePosition))
            {
                bool clickedEmpty = true;
                foreach (AnimationWindowKeyframe keyframe in dopeline.keys)
                {
                    if (this.GetKeyframeRect(dopeline, keyframe).Contains(Event.current.mousePosition))
                    {
                        clickedEmpty = false;
                        break;
                    }
                }
                this.GenerateMenu(dopeline, clickedEmpty).ShowAsContext();
            }
        }

        private void HandleDelete()
        {
            if (this.state.selectedKeys.Count != 0)
            {
                switch (Event.current.type)
                {
                    case EventType.ValidateCommand:
                    case EventType.ExecuteCommand:
                        if ((Event.current.commandName == "SoftDelete") || (Event.current.commandName == "Delete"))
                        {
                            if (Event.current.type == EventType.ExecuteCommand)
                            {
                                this.state.DeleteSelectedKeys();
                            }
                            Event.current.Use();
                        }
                        break;

                    case EventType.KeyDown:
                        if ((Event.current.keyCode == KeyCode.Backspace) || (Event.current.keyCode == KeyCode.Delete))
                        {
                            this.state.DeleteSelectedKeys();
                            Event.current.Use();
                        }
                        break;
                }
            }
        }

        private void HandleDopelineDoubleclick(DopeLine dopeline)
        {
            this.state.ClearSelections();
            float num = this.state.PixelToTime(Event.current.mousePosition.x, AnimationWindowState.SnapMode.SnapToClipFrame);
            AnimationKeyTime time = AnimationKeyTime.Time(num, this.state.frameRate);
            foreach (AnimationWindowCurve curve in dopeline.m_Curves)
            {
                if (curve.animationIsEditable)
                {
                    AnimationKeyTime time2 = AnimationKeyTime.Time(num - curve.timeOffset, this.state.frameRate);
                    AnimationWindowKeyframe keyframe = AnimationWindowUtility.AddKeyframeToCurve(this.state, curve, time2);
                    this.state.SelectKey(keyframe);
                }
            }
            if (!this.state.playing && this.state.syncTimeDuringDrag)
            {
                this.state.frame = time.frame;
            }
            Event.current.Use();
        }

        private void HandleDragAndDrop(DopeLine dopeline)
        {
            Event current = Event.current;
            if ((current.type == EventType.DragPerform) || (current.type == EventType.DragUpdated))
            {
                if (this.DoDragAndDrop(dopeline, dopeline.position, current.type == EventType.DragPerform))
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    current.Use();
                }
                else
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
                }
            }
        }

        private void HandleDragAndDropToEmptyArea()
        {
            Event current = Event.current;
            if (((current.type == EventType.DragPerform) || (current.type == EventType.DragUpdated)) && ValidateDragAndDropObjects())
            {
                if ((DragAndDrop.objectReferences[0].GetType() == typeof(Sprite)) || (DragAndDrop.objectReferences[0].GetType() == typeof(Texture2D)))
                {
                    foreach (AnimationWindowSelectionItem item in this.state.selection.ToArray())
                    {
                        if (item.clipIsEditable)
                        {
                            AnimationClip animationClip = item.animationClip;
                            GameObject rootGameObject = item.rootGameObject;
                            if ((rootGameObject != null) && !this.DopelineForValueTypeExists(typeof(Sprite)))
                            {
                                if (current.type == EventType.DragPerform)
                                {
                                    EditorCurveBinding? spriteBinding = this.CreateNewPptrDopeline(rootGameObject, rootGameObject, animationClip, typeof(Sprite));
                                    if (spriteBinding.HasValue)
                                    {
                                        this.DoSpriteDropAfterGeneratingNewDopeline(animationClip, spriteBinding);
                                    }
                                }
                                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                                current.Use();
                                return;
                            }
                        }
                    }
                }
                DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
            }
        }

        private void HandleDragging()
        {
            int controlID = GUIUtility.GetControlID("dopesheetdrag".GetHashCode(), FocusType.Passive, new Rect());
            EventType typeForControl = Event.current.GetTypeForControl(controlID);
            if (((typeForControl == EventType.MouseDrag) || (typeForControl == EventType.MouseUp)) && this.m_MousedownOnKeyframe)
            {
                if ((((typeForControl == EventType.MouseDrag) && !EditorGUI.actionKey) && (!Event.current.shift && !this.m_IsDragging)) && (this.state.selectedKeys.Count > 0))
                {
                    this.m_IsDragging = true;
                    this.m_IsDraggingPlayheadStarted = true;
                    GUIUtility.hotControl = controlID;
                    this.m_DragStartTime = this.state.PixelToTime(Event.current.mousePosition.x);
                    Event.current.Use();
                }
                float maxValue = float.MaxValue;
                foreach (AnimationWindowKeyframe keyframe in this.state.selectedKeys)
                {
                    maxValue = Mathf.Min(keyframe.time, maxValue);
                }
                float a = this.state.SnapToFrame(this.state.PixelToTime(Event.current.mousePosition.x), AnimationWindowState.SnapMode.SnapToClipFrame);
                float deltaTime = Mathf.Max((float) (a - this.m_DragStartTime), (float) (maxValue * -1f));
                if (this.m_IsDragging && !Mathf.Approximately(a, this.m_DragStartTime))
                {
                    this.m_DragStartTime = a;
                    this.state.MoveSelectedKeys(deltaTime, true, false);
                    if (((this.state.activeKeyframe != null) && !this.state.playing) && this.state.syncTimeDuringDrag)
                    {
                        this.state.frame = this.state.TimeToFrameFloor(this.state.activeKeyframe.time + this.state.activeKeyframe.curve.timeOffset);
                    }
                    Event.current.Use();
                }
                if (typeForControl == EventType.MouseUp)
                {
                    if (this.m_IsDragging && (GUIUtility.hotControl == controlID))
                    {
                        this.state.MoveSelectedKeys(deltaTime, true, true);
                        Event.current.Use();
                        this.m_IsDragging = false;
                    }
                    this.m_MousedownOnKeyframe = false;
                    GUIUtility.hotControl = 0;
                }
            }
            if ((this.m_IsDraggingPlayheadStarted && (typeForControl == EventType.MouseDrag)) && (Event.current.button == 1))
            {
                this.m_IsDraggingPlayhead = true;
                Event.current.Use();
            }
        }

        private void HandleKeyboard()
        {
            if ((Event.current.type == EventType.ValidateCommand) || (Event.current.type == EventType.ExecuteCommand))
            {
                string commandName = Event.current.commandName;
                if (commandName != null)
                {
                    int num;
                    if (<>f__switch$mapD == null)
                    {
                        Dictionary<string, int> dictionary = new Dictionary<string, int>(2);
                        dictionary.Add("SelectAll", 0);
                        dictionary.Add("FrameSelected", 1);
                        <>f__switch$mapD = dictionary;
                    }
                    if (<>f__switch$mapD.TryGetValue(commandName, out num))
                    {
                        if (num == 0)
                        {
                            if (Event.current.type == EventType.ExecuteCommand)
                            {
                                this.HandleSelectAll();
                            }
                            Event.current.Use();
                        }
                        else if (num == 1)
                        {
                            if (Event.current.type == EventType.ExecuteCommand)
                            {
                                this.FrameSelected();
                            }
                            Event.current.Use();
                        }
                    }
                }
            }
        }

        private void HandleMouseDown(DopeLine dopeline)
        {
            Event current = Event.current;
            if (dopeline.position.Contains(current.mousePosition))
            {
                if (!EditorGUI.actionKey && !current.shift)
                {
                    foreach (AnimationWindowKeyframe keyframe in dopeline.keys)
                    {
                        if (this.GetKeyframeRect(dopeline, keyframe).Contains(current.mousePosition) && !this.state.KeyIsSelected(keyframe))
                        {
                            this.state.ClearSelections();
                            break;
                        }
                    }
                }
                float num = this.state.PixelToTime(Event.current.mousePosition.x);
                float num2 = num;
                if (Event.current.shift)
                {
                    foreach (AnimationWindowKeyframe keyframe2 in dopeline.keys)
                    {
                        if (this.state.KeyIsSelected(keyframe2))
                        {
                            if (keyframe2.time < num)
                            {
                                num = keyframe2.time;
                            }
                            if (keyframe2.time > num2)
                            {
                                num2 = keyframe2.time;
                            }
                        }
                    }
                }
                bool flag = false;
                foreach (AnimationWindowKeyframe keyframe3 in dopeline.keys)
                {
                    if (this.GetKeyframeRect(dopeline, keyframe3).Contains(current.mousePosition))
                    {
                        flag = true;
                        if (!this.state.KeyIsSelected(keyframe3))
                        {
                            if (Event.current.shift)
                            {
                                foreach (AnimationWindowKeyframe keyframe4 in dopeline.keys)
                                {
                                    if ((keyframe4 == keyframe3) || ((keyframe4.time > num) && (keyframe4.time < num2)))
                                    {
                                        this.state.SelectKey(keyframe4);
                                    }
                                }
                            }
                            else
                            {
                                this.state.SelectKey(keyframe3);
                            }
                            if (!dopeline.isMasterDopeline)
                            {
                                this.state.SelectHierarchyItem(dopeline, EditorGUI.actionKey || current.shift);
                            }
                        }
                        else if (EditorGUI.actionKey)
                        {
                            this.state.UnselectKey(keyframe3);
                            if (!this.state.AnyKeyIsSelected(dopeline))
                            {
                                this.state.UnSelectHierarchyItem(dopeline);
                            }
                        }
                        this.state.activeKeyframe = keyframe3;
                        this.m_MousedownOnKeyframe = true;
                        if (!this.state.playing && this.state.syncTimeDuringDrag)
                        {
                            this.state.frame = this.state.TimeToFrameRound(this.state.activeKeyframe.time + this.state.activeKeyframe.curve.timeOffset);
                        }
                        current.Use();
                    }
                }
                if (dopeline.isMasterDopeline)
                {
                    this.state.ClearHierarchySelection();
                    foreach (int num3 in this.state.GetAffectedHierarchyIDs(this.state.selectedKeys))
                    {
                        this.state.SelectHierarchyItem(num3, true, true);
                    }
                }
                if (((current.clickCount == 2) && (current.button == 0)) && (!Event.current.shift && !EditorGUI.actionKey))
                {
                    this.HandleDopelineDoubleclick(dopeline);
                }
                if ((current.button == 1) && !this.state.playing)
                {
                    AnimationKeyTime time = AnimationKeyTime.Time(this.state.PixelToTime(Event.current.mousePosition.x, AnimationWindowState.SnapMode.SnapToClipFrame), this.state.frameRate);
                    if (this.state.syncTimeDuringDrag)
                    {
                        this.state.frame = time.frame;
                    }
                    if (!flag)
                    {
                        this.state.ClearSelections();
                        this.m_IsDraggingPlayheadStarted = true;
                        HandleUtility.Repaint();
                        current.Use();
                    }
                }
            }
        }

        private void HandleSelectAll()
        {
            foreach (DopeLine line in this.state.dopelines)
            {
                foreach (AnimationWindowKeyframe keyframe in line.keys)
                {
                    this.state.SelectKey(keyframe);
                }
                this.state.SelectHierarchyItem(line, true, false);
            }
        }

        private void HandleSelectionRect(Rect rect)
        {
            if (this.m_SelectionRect == null)
            {
                this.m_SelectionRect = new DopeSheetSelectionRect(this);
            }
            if (!this.m_MousedownOnKeyframe)
            {
                this.m_SelectionRect.OnGUI(rect);
            }
        }

        public void Init()
        {
            if (!this.m_Initialized)
            {
                if (this.m_DefaultDopeKeyIcon == null)
                {
                    this.m_DefaultDopeKeyIcon = EditorGUIUtility.LoadIcon("blendKey");
                }
                base.hSlider = true;
                base.vSlider = false;
                base.hRangeLocked = false;
                base.vRangeLocked = true;
                base.hRangeMin = 0f;
                base.margin = 40f;
                base.scaleWithWindow = true;
                base.ignoreScrollWheelUntilClicked = false;
            }
            this.m_Initialized = true;
        }

        internal void OnDestroy()
        {
            AssetPreview.DeletePreviewTextureManagerByID(this.assetPreviewManagerID);
        }

        public void OnGUI(Rect position, Vector2 scrollPosition)
        {
            this.Init();
            this.HandleDragAndDropToEmptyArea();
            GUIClip.Push(position, scrollPosition, Vector2.zero, false);
            Rect rect = new Rect(0f, 0f, position.width, position.height);
            Rect rect2 = this.DopelinesGUI(rect, scrollPosition);
            this.HandleKeyboard();
            this.HandleDragging();
            this.HandleSelectionRect(rect2);
            this.HandleDelete();
            GUIClip.Pop();
        }

        private void PerformDragAndDrop(AnimationWindowCurve targetCurve, float time)
        {
            if ((DragAndDrop.objectReferences.Length != 0) && (targetCurve != null))
            {
                this.state.ClearSelections();
                foreach (Object obj2 in this.GetSortedDragAndDropObjectReferences())
                {
                    Object obj3 = obj2;
                    if (obj3 is Texture2D)
                    {
                        obj3 = SpriteUtility.TextureToSprite(obj2 as Texture2D);
                    }
                    this.CreateNewPPtrKeyframe(time, obj3, targetCurve);
                    time += 1f / targetCurve.clip.frameRate;
                }
                this.state.SaveCurve(targetCurve);
                DragAndDrop.AcceptDrag();
            }
        }

        public void RecalculateBounds()
        {
            if (!this.state.disabled)
            {
                Vector2 timeRange = this.state.timeRange;
                this.m_Bounds.SetMinMax(new Vector3(timeRange.x, 0f, 0f), new Vector3(timeRange.y, 0f, 0f));
            }
        }

        private void SelectTypeForCreatingNewPptrDopeline(object userData, string[] options, int selected)
        {
            List<object> list = userData as List<object>;
            AnimationClip animationClip = list[0] as AnimationClip;
            List<EditorCurveBinding> list2 = list[1] as List<EditorCurveBinding>;
            if (list2.Count > selected)
            {
                this.DoSpriteDropAfterGeneratingNewDopeline(animationClip, new EditorCurveBinding?(list2[selected]));
            }
        }

        public void UpdateCurves(List<int> curveIds, string undoText)
        {
        }

        public void UpdateCurves(List<ChangedCurve> changedCurves, string undoText)
        {
            Undo.RegisterCompleteObjectUndo(this.state.activeAnimationClip, undoText);
            <UpdateCurves>c__AnonStorey57 storey = new <UpdateCurves>c__AnonStorey57();
            using (List<ChangedCurve>.Enumerator enumerator = changedCurves.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    storey.changedCurve = enumerator.Current;
                    AnimationWindowCurve curve = this.state.allCurves.Find(new Predicate<AnimationWindowCurve>(storey.<>m__A8));
                    if (curve != null)
                    {
                        AnimationUtility.SetEditorCurve(curve.clip, storey.changedCurve.binding, storey.changedCurve.curve);
                    }
                    else
                    {
                        Debug.LogError("Could not match ChangedCurve data to destination curves.");
                    }
                }
            }
        }

        private void UpdateSpritePreviewCacheSize()
        {
            int size = 1;
            foreach (DopeLine line in this.state.dopelines)
            {
                if (line.tallMode && line.isPptrDopeline)
                {
                    size += line.keys.Count;
                }
            }
            size += DragAndDrop.objectReferences.Length;
            if (size > this.m_SpritePreviewCacheSize)
            {
                AssetPreview.SetPreviewTextureCacheSize(size, this.assetPreviewManagerID);
                this.m_SpritePreviewCacheSize = size;
            }
        }

        private static bool ValidateDragAndDropObjects()
        {
            if (DragAndDrop.objectReferences.Length == 0)
            {
                return false;
            }
            for (int i = 0; i < DragAndDrop.objectReferences.Length; i++)
            {
                Object obj2 = DragAndDrop.objectReferences[i];
                if (obj2 == null)
                {
                    return false;
                }
                if (i < (DragAndDrop.objectReferences.Length - 1))
                {
                    Object obj3 = DragAndDrop.objectReferences[i + 1];
                    bool flag = ((obj2 is Texture2D) || (obj2 is Sprite)) && ((obj3 is Texture2D) || (obj3 is Sprite));
                    if ((obj2.GetType() != obj3.GetType()) && !flag)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        internal int assetPreviewManagerID
        {
            get
            {
                return ((this.m_Owner == null) ? 0 : this.m_Owner.GetInstanceID());
            }
        }

        public float contentHeight
        {
            get
            {
                float num = 0f;
                foreach (DopeLine line in this.state.dopelines)
                {
                    num += !line.tallMode ? 16f : 32f;
                }
                return (num + 40f);
            }
        }

        public override Bounds drawingBounds
        {
            get
            {
                return this.m_Bounds;
            }
        }

        [CompilerGenerated]
        private sealed class <DopelineForValueTypeExists>c__AnonStorey56
        {
            internal Type valueType;

            internal bool <>m__A6(AnimationWindowCurve curve)
            {
                return (curve.m_ValueType == this.valueType);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateCurves>c__AnonStorey57
        {
            internal ChangedCurve changedCurve;

            internal bool <>m__A8(AnimationWindowCurve c)
            {
                return (this.changedCurve.curveId == c.GetCurveID());
            }
        }

        internal class DopeSheetPopup
        {
            private Rect backgroundRect;
            private Rect position;
            private static int s_height = 0x70;
            private static int s_width = 0x60;

            public DopeSheetPopup(Rect position)
            {
                this.position = position;
            }

            public void OnGUI(AnimationWindowState state, AnimationWindowKeyframe keyframe)
            {
                if (!keyframe.isPPtrCurve)
                {
                    this.backgroundRect = this.position;
                    this.backgroundRect.x = (state.TimeToPixel(keyframe.time) + this.position.x) - (s_width / 2);
                    this.backgroundRect.y += 16f;
                    this.backgroundRect.width = s_width;
                    this.backgroundRect.height = s_height;
                    Rect backgroundRect = this.backgroundRect;
                    backgroundRect.height = 16f;
                    Rect position = this.backgroundRect;
                    position.y += 16f;
                    position.height = s_width;
                    GUI.Box(this.backgroundRect, string.Empty);
                    GUI.Box(position, AssetPreview.GetAssetPreview((Object) keyframe.value));
                    EditorGUI.BeginChangeCheck();
                    Object obj2 = EditorGUI.ObjectField(backgroundRect, (Object) keyframe.value, keyframe.curve.m_ValueType, false);
                    if (EditorGUI.EndChangeCheck())
                    {
                        keyframe.value = obj2;
                        state.SaveCurve(keyframe.curve);
                    }
                }
            }
        }

        internal class DopeSheetSelectionRect
        {
            public readonly GUIStyle createRect = "U2D.createRect";
            private Vector2 m_SelectMousePoint;
            private Vector2 m_SelectStartPoint;
            private bool m_ValidRect;
            private DopeSheetEditor owner;
            private static int s_RectSelectionID = GUIUtility.GetPermanentControlID();

            public DopeSheetSelectionRect(DopeSheetEditor owner)
            {
                this.owner = owner;
            }

            public Rect GetCurrentPixelRect()
            {
                float num = 16f;
                Rect rect = AnimationWindowUtility.FromToRect(this.m_SelectStartPoint, this.m_SelectMousePoint);
                rect.xMin = this.owner.state.TimeToPixel(this.owner.state.PixelToTime(rect.xMin, AnimationWindowState.SnapMode.SnapToClipFrame), AnimationWindowState.SnapMode.SnapToClipFrame);
                rect.xMax = this.owner.state.TimeToPixel(this.owner.state.PixelToTime(rect.xMax, AnimationWindowState.SnapMode.SnapToClipFrame), AnimationWindowState.SnapMode.SnapToClipFrame);
                rect.yMin = Mathf.Floor(rect.yMin / num) * num;
                rect.yMax = (Mathf.Floor(rect.yMax / num) + 1f) * num;
                return rect;
            }

            public Rect GetCurrentTimeRect()
            {
                float num = 16f;
                Rect rect = AnimationWindowUtility.FromToRect(this.m_SelectStartPoint, this.m_SelectMousePoint);
                rect.xMin = this.owner.state.PixelToTime(rect.xMin, AnimationWindowState.SnapMode.SnapToClipFrame);
                rect.xMax = this.owner.state.PixelToTime(rect.xMax, AnimationWindowState.SnapMode.SnapToClipFrame);
                rect.yMin = Mathf.Floor(rect.yMin / num) * num;
                rect.yMax = (Mathf.Floor(rect.yMax / num) + 1f) * num;
                return rect;
            }

            public void OnGUI(Rect position)
            {
                Event current = Event.current;
                Vector2 mousePosition = current.mousePosition;
                int controlID = s_RectSelectionID;
                switch (current.GetTypeForControl(controlID))
                {
                    case EventType.MouseDown:
                        if ((current.button == 0) && position.Contains(mousePosition))
                        {
                            GUIUtility.hotControl = controlID;
                            this.m_SelectStartPoint = mousePosition;
                            this.m_ValidRect = false;
                            current.Use();
                        }
                        return;

                    case EventType.MouseUp:
                    {
                        if ((GUIUtility.hotControl != controlID) || (current.button != 0))
                        {
                            return;
                        }
                        if (!this.m_ValidRect)
                        {
                            this.owner.state.ClearSelections();
                            break;
                        }
                        if (!EditorGUI.actionKey)
                        {
                            this.owner.state.ClearSelections();
                        }
                        float frameRate = this.owner.state.frameRate;
                        Rect currentTimeRect = this.GetCurrentTimeRect();
                        GUI.changed = true;
                        this.owner.state.ClearHierarchySelection();
                        List<AnimationWindowKeyframe> list = new List<AnimationWindowKeyframe>();
                        List<AnimationWindowKeyframe> list2 = new List<AnimationWindowKeyframe>();
                        foreach (DopeLine line in this.owner.state.dopelines)
                        {
                            if ((line.position.yMin >= currentTimeRect.yMin) && (line.position.yMax <= currentTimeRect.yMax))
                            {
                                foreach (AnimationWindowKeyframe keyframe in line.keys)
                                {
                                    AnimationKeyTime time = AnimationKeyTime.Time(currentTimeRect.xMin - keyframe.curve.timeOffset, frameRate);
                                    AnimationKeyTime time2 = AnimationKeyTime.Time(currentTimeRect.xMax - keyframe.curve.timeOffset, frameRate);
                                    AnimationKeyTime time3 = AnimationKeyTime.Time(keyframe.time, frameRate);
                                    if ((((!line.tallMode && (time3.frame >= time.frame)) && (time3.frame <= time2.frame)) || ((line.tallMode && (time3.frame >= time.frame)) && (time3.frame < time2.frame))) && (!list2.Contains(keyframe) && !list.Contains(keyframe)))
                                    {
                                        if (!this.owner.state.KeyIsSelected(keyframe))
                                        {
                                            list2.Add(keyframe);
                                        }
                                        else if (this.owner.state.KeyIsSelected(keyframe))
                                        {
                                            list.Add(keyframe);
                                        }
                                    }
                                }
                            }
                        }
                        if (list2.Count == 0)
                        {
                            foreach (AnimationWindowKeyframe keyframe2 in list)
                            {
                                this.owner.state.UnselectKey(keyframe2);
                            }
                        }
                        foreach (AnimationWindowKeyframe keyframe3 in list2)
                        {
                            this.owner.state.SelectKey(keyframe3);
                        }
                        foreach (DopeLine line2 in this.owner.state.dopelines)
                        {
                            if (this.owner.state.AnyKeyIsSelected(line2))
                            {
                                this.owner.state.SelectHierarchyItem(line2, true, false);
                            }
                        }
                        break;
                    }
                    case EventType.MouseMove:
                    case EventType.KeyDown:
                    case EventType.KeyUp:
                    case EventType.ScrollWheel:
                        return;

                    case EventType.MouseDrag:
                        if (GUIUtility.hotControl == controlID)
                        {
                            Vector2 vector2 = mousePosition - this.m_SelectStartPoint;
                            this.m_ValidRect = Mathf.Abs(vector2.x) > 1f;
                            if (this.m_ValidRect)
                            {
                                this.m_SelectMousePoint = new Vector2(mousePosition.x, mousePosition.y);
                            }
                            current.Use();
                        }
                        return;

                    case EventType.Repaint:
                        if ((GUIUtility.hotControl == controlID) && this.m_ValidRect)
                        {
                            EditorStyles.selectionRect.Draw(this.GetCurrentPixelRect(), GUIContent.none, false, false, false, false);
                        }
                        return;

                    default:
                        return;
                }
                current.Use();
                GUIUtility.hotControl = 0;
            }

            private enum SelectionType
            {
                Normal,
                Additive,
                Subtractive
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct DrawElement
        {
            public Rect position;
            public Color color;
            public Texture2D texture;
            public DrawElement(Rect position, Color color, Texture2D texture)
            {
                this.position = position;
                this.color = color;
                this.texture = texture;
            }
        }
    }
}

