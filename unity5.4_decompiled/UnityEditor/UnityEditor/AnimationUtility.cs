﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Internal;
    using UnityEngine.Scripting;

    public sealed class AnimationUtility
    {
        private static int kBrokenMask = 1;
        private static int kLeftTangentMask = 6;
        private static int kRightTangentMask = 0x18;
        public static OnCurveWasModified onCurveWasModified;

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern bool AmbiguousBinding(string path, int classID, Transform root);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern string CalculateTransformPath(Transform targetTransform, Transform root);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern void ConstrainToPolynomialCurve(AnimationCurve curve);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern bool CurveSupportsProcedural(AnimationCurve curve);
        [Obsolete("GetAllCurves is deprecated. Use GetCurveBindings and GetObjectReferenceCurveBindings instead."), ExcludeFromDocs]
        public static AnimationClipCurveData[] GetAllCurves(AnimationClip clip)
        {
            bool includeCurveData = true;
            return GetAllCurves(clip, includeCurveData);
        }

        [Obsolete("GetAllCurves is deprecated. Use GetCurveBindings and GetObjectReferenceCurveBindings instead.")]
        public static AnimationClipCurveData[] GetAllCurves(AnimationClip clip, [DefaultValue("true")] bool includeCurveData)
        {
            EditorCurveBinding[] curveBindings = GetCurveBindings(clip);
            AnimationClipCurveData[] dataArray = new AnimationClipCurveData[curveBindings.Length];
            for (int i = 0; i < dataArray.Length; i++)
            {
                dataArray[i] = new AnimationClipCurveData(curveBindings[i]);
                if (includeCurveData)
                {
                    dataArray[i].curve = GetEditorCurve(clip, curveBindings[i]);
                }
            }
            return dataArray;
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern EditorCurveBinding[] GetAnimatableBindings(GameObject targetObject, GameObject root);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern Object GetAnimatedObject(GameObject root, EditorCurveBinding binding);
        [Obsolete("GetAnimationClips(Animation) is deprecated. Use GetAnimationClips(GameObject) instead.")]
        public static AnimationClip[] GetAnimationClips(Animation component)
        {
            return GetAnimationClips(component.gameObject);
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern AnimationClip[] GetAnimationClips(GameObject gameObject);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern AnimationClipSettings GetAnimationClipSettings(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern AnimationClipStats GetAnimationClipStats(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern AnimationEvent[] GetAnimationEvents(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern EditorCurveBinding[] GetCurveBindings(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern AnimationCurve GetEditorCurve(AnimationClip clip, EditorCurveBinding binding);
        [Obsolete("This overload is deprecated. Use the one with EditorCurveBinding instead.")]
        public static AnimationCurve GetEditorCurve(AnimationClip clip, string relativePath, Type type, string propertyName)
        {
            return GetEditorCurve(clip, EditorCurveBinding.FloatCurve(relativePath, type, propertyName));
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern Type GetEditorCurveValueType(GameObject root, EditorCurveBinding binding);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern bool GetFloatValue(GameObject root, EditorCurveBinding binding, out float data);
        [Obsolete("This overload is deprecated. Use the one with EditorCurveBinding instead.")]
        public static bool GetFloatValue(GameObject root, string relativePath, Type type, string propertyName, out float data)
        {
            return GetFloatValue(root, EditorCurveBinding.FloatCurve(relativePath, type, propertyName), out data);
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern bool GetGenerateMotionCurves(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern ObjectReferenceKeyframe[] GetObjectReferenceCurve(AnimationClip clip, EditorCurveBinding binding);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern EditorCurveBinding[] GetObjectReferenceCurveBindings(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern bool GetObjectReferenceValue(GameObject root, EditorCurveBinding binding, out Object targetObject);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern bool HasGenericRootTransform(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern bool HasMotionCurves(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern bool HasMotionFloatCurves(AnimationClip clip);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern bool HasRootCurves(AnimationClip clip);
        [Obsolete("Use AnimationMode.InAnimationMode instead")]
        public static bool InAnimationMode()
        {
            return AnimationMode.InAnimationMode();
        }

        private static float Internal_CalculateLinearTangent(AnimationCurve curve, int index, int toIndex)
        {
            Keyframe keyframe = curve[index];
            Keyframe keyframe2 = curve[toIndex];
            float a = keyframe.time - keyframe2.time;
            if (Mathf.Approximately(a, 0f))
            {
                return 0f;
            }
            Keyframe keyframe3 = curve[index];
            Keyframe keyframe4 = curve[toIndex];
            return ((keyframe3.value - keyframe4.value) / a);
        }

        [RequiredByNativeCode]
        private static void Internal_CallAnimationClipAwake(AnimationClip clip)
        {
            if (onCurveWasModified != null)
            {
                onCurveWasModified(clip, new EditorCurveBinding(), CurveModifiedType.ClipModified);
            }
        }

        private static bool Internal_GetKeyBroken(Keyframe key)
        {
            return ((key.tangentMode & kBrokenMask) != 0);
        }

        private static TangentMode Internal_GetKeyLeftTangentMode(Keyframe key)
        {
            return (TangentMode) ((key.tangentMode & kLeftTangentMask) >> 1);
        }

        private static TangentMode Internal_GetKeyRightTangentMode(Keyframe key)
        {
            return (TangentMode) ((key.tangentMode & kRightTangentMask) >> 3);
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        private static extern void Internal_SetAnimationEvents(AnimationClip clip, AnimationEvent[] events);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        private static extern void Internal_SetEditorCurve(AnimationClip clip, EditorCurveBinding binding, AnimationCurve curve);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        private static extern void Internal_SetObjectReferenceCurve(AnimationClip clip, EditorCurveBinding binding, ObjectReferenceKeyframe[] keyframes);
        private static void Internal_UpdateTangents(AnimationCurve curve, int index)
        {
            if ((index < 0) || (index >= curve.length))
            {
                throw new ArgumentException("Index out of bounds.");
            }
            Keyframe key = curve[index];
            if ((Internal_GetKeyLeftTangentMode(key) == TangentMode.Linear) && (index >= 1))
            {
                key.inTangent = Internal_CalculateLinearTangent(curve, index, index - 1);
                curve.MoveKey(index, key);
            }
            if ((Internal_GetKeyRightTangentMode(key) == TangentMode.Linear) && ((index + 1) < curve.length))
            {
                key.outTangent = Internal_CalculateLinearTangent(curve, index, index + 1);
                curve.MoveKey(index, key);
            }
            if ((Internal_GetKeyLeftTangentMode(key) == TangentMode.Auto) || (Internal_GetKeyRightTangentMode(key) == TangentMode.Auto))
            {
                curve.SmoothTangents(index, 0f);
            }
            if (((Internal_GetKeyLeftTangentMode(key) == TangentMode.Free) && (Internal_GetKeyRightTangentMode(key) == TangentMode.Free)) && !Internal_GetKeyBroken(key))
            {
                key.outTangent = key.inTangent;
                curve.MoveKey(index, key);
            }
            if (Internal_GetKeyLeftTangentMode(key) == TangentMode.Constant)
            {
                key.inTangent = float.PositiveInfinity;
                curve.MoveKey(index, key);
            }
            if (Internal_GetKeyRightTangentMode(key) == TangentMode.Constant)
            {
                key.outTangent = float.PositiveInfinity;
                curve.MoveKey(index, key);
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern bool IsValidPolynomialCurve(AnimationCurve curve);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern Type PropertyModificationToEditorCurveBinding(PropertyModification modification, GameObject gameObject, out EditorCurveBinding binding);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern void SetAdditiveReferencePose(AnimationClip clip, AnimationClip referenceClip, float time);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern void SetAnimationClips(Animation animation, AnimationClip[] clips);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern void SetAnimationClipSettings(AnimationClip clip, AnimationClipSettings srcClipInfo);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern void SetAnimationClipSettingsNoDirty(AnimationClip clip, AnimationClipSettings srcClipInfo);
        public static void SetAnimationEvents(AnimationClip clip, AnimationEvent[] events)
        {
            if (clip == null)
            {
                throw new ArgumentNullException("clip");
            }
            if (events == null)
            {
                throw new ArgumentNullException("events");
            }
            Internal_SetAnimationEvents(clip, events);
            if (onCurveWasModified != null)
            {
                onCurveWasModified(clip, new EditorCurveBinding(), CurveModifiedType.ClipModified);
            }
        }

        [Obsolete("SetAnimationType is no longer supported", true)]
        public static void SetAnimationType(AnimationClip clip, ModelImporterAnimationType type)
        {
        }

        public static void SetEditorCurve(AnimationClip clip, EditorCurveBinding binding, AnimationCurve curve)
        {
            Internal_SetEditorCurve(clip, binding, curve);
            if (onCurveWasModified != null)
            {
                onCurveWasModified(clip, binding, (curve == null) ? CurveModifiedType.CurveDeleted : CurveModifiedType.CurveModified);
            }
        }

        [Obsolete("This overload is deprecated. Use the one with EditorCurveBinding instead.")]
        public static void SetEditorCurve(AnimationClip clip, string relativePath, Type type, string propertyName, AnimationCurve curve)
        {
            SetEditorCurve(clip, EditorCurveBinding.FloatCurve(relativePath, type, propertyName), curve);
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern void SetGenerateMotionCurves(AnimationClip clip, bool value);
        public static void SetKeyBroken(AnimationCurve curve, int index, bool broken)
        {
            if (curve == null)
            {
                throw new ArgumentNullException("curve");
            }
            if ((index >= 0) && (index < curve.length))
            {
                Keyframe key = curve[index];
                if (broken)
                {
                    key.tangentMode |= kBrokenMask;
                }
                else
                {
                    key.tangentMode &= ~kBrokenMask;
                }
                curve.MoveKey(index, key);
                Internal_UpdateTangents(curve, index);
            }
        }

        public static void SetKeyLeftTangentMode(AnimationCurve curve, int index, TangentMode tangentMode)
        {
            if (curve == null)
            {
                throw new ArgumentNullException("curve");
            }
            if ((index >= 0) && (index < curve.length))
            {
                Keyframe key = curve[index];
                if (tangentMode != TangentMode.Free)
                {
                    key.tangentMode |= kBrokenMask;
                }
                key.tangentMode &= ~kLeftTangentMask;
                key.tangentMode |= ((int) tangentMode) << 1;
                curve.MoveKey(index, key);
                Internal_UpdateTangents(curve, index);
            }
        }

        public static void SetKeyRightTangentMode(AnimationCurve curve, int index, TangentMode tangentMode)
        {
            if (curve == null)
            {
                throw new ArgumentNullException("curve");
            }
            if ((index >= 0) && (index < curve.length))
            {
                Keyframe key = curve[index];
                if (tangentMode != TangentMode.Free)
                {
                    key.tangentMode |= kBrokenMask;
                }
                key.tangentMode &= ~kRightTangentMask;
                key.tangentMode |= ((int) tangentMode) << 3;
                curve.MoveKey(index, key);
                Internal_UpdateTangents(curve, index);
            }
        }

        public static void SetObjectReferenceCurve(AnimationClip clip, EditorCurveBinding binding, ObjectReferenceKeyframe[] keyframes)
        {
            Internal_SetObjectReferenceCurve(clip, binding, keyframes);
            if (onCurveWasModified != null)
            {
                onCurveWasModified(clip, binding, (keyframes == null) ? CurveModifiedType.CurveDeleted : CurveModifiedType.CurveModified);
            }
        }

        [Obsolete("Use AnimationMode.StartAnimationmode instead")]
        public static void StartAnimationMode(Object[] objects)
        {
            Debug.LogWarning("AnimationUtility.StartAnimationMode is deprecated. Use AnimationMode.StartAnimationMode with the new APIs. The objects passed to this function will no longer be reverted automatically. See AnimationMode.AddPropertyModification");
            AnimationMode.StartAnimationMode();
        }

        [Obsolete("Use AnimationMode.StopAnimationMode instead")]
        public static void StopAnimationMode()
        {
            AnimationMode.StopAnimationMode();
        }

        public enum CurveModifiedType
        {
            CurveDeleted,
            CurveModified,
            ClipModified
        }

        public delegate void OnCurveWasModified(AnimationClip clip, EditorCurveBinding binding, AnimationUtility.CurveModifiedType deleted);

        public enum TangentMode
        {
            Free,
            Auto,
            Linear,
            Constant
        }
    }
}

