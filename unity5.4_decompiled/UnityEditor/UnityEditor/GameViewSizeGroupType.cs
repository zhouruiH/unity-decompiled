﻿namespace UnityEditor
{
    using System;

    public enum GameViewSizeGroupType
    {
        Android = 3,
        iOS = 2,
        Nintendo3DS = 9,
        PS3 = 4,
        Standalone = 0,
        Tizen = 7,
        [Obsolete("WebPlayer has been removed in 5.4", false)]
        WebPlayer = 1,
        WiiU = 6,
        WP8 = 8,
        Xbox360 = 5
    }
}

