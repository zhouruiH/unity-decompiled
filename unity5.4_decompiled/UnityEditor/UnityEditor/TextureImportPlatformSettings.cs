﻿namespace UnityEditor
{
    using System;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    internal class TextureImportPlatformSettings
    {
        [CompilerGenerated]
        private static Func<Object, TextureImporter> <>f__am$cache15;
        public static readonly int[] kNormalFormatsValueWeb = new int[] { 12, 0x1d, 2, 5 };
        public static readonly int[] kTextureFormatsValueAndroid = new int[] { 
            10, 12, 0x1c, 0x1d, 0x22, 0x2d, 0x2e, 0x2f, 30, 0x1f, 0x20, 0x21, 0x23, 0x24, 0x30, 0x31, 
            50, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 7, 3, 1, 13, 4
         };
        public static readonly int[] kTextureFormatsValueiPhone = new int[] { 
            30, 0x1f, 0x20, 0x21, 0x30, 0x31, 50, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 
            7, 3, 1, 13, 4
         };
        public static readonly int[] kTextureFormatsValueSTV = new int[] { 0x22, 7, 3, 1, 13, 4 };
        public static readonly int[] kTextureFormatsValueTizen = new int[] { 0x22, 7, 3, 1, 13, 4 };
        private static readonly int[] kTextureFormatsValuetvOS = new int[] { 
            30, 0x1f, 0x20, 0x21, 0x30, 0x31, 50, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 
            7, 3, 1, 13, 4
         };
        public static readonly int[] kTextureFormatsValueWeb = new int[] { 10, 12, 0x1c, 0x1d, 7, 3, 1, 2, 5 };
        public static readonly int[] kTextureFormatsValueWiiU = new int[] { 10, 12, 7, 1, 4, 13 };
        [SerializeField]
        private int m_CompressionQuality;
        [SerializeField]
        private bool m_CompressionQualityIsDifferent;
        [SerializeField]
        private bool m_HasChanged;
        [SerializeField]
        private TextureImporter[] m_Importers;
        [SerializeField]
        private TextureImporterInspector m_Inspector;
        [SerializeField]
        private int m_MaxTextureSize;
        [SerializeField]
        private bool m_MaxTextureSizeIsDifferent;
        [SerializeField]
        private bool m_Overridden;
        [SerializeField]
        private bool m_OverriddenIsDifferent;
        [SerializeField]
        public BuildTarget m_Target;
        [SerializeField]
        private TextureImporterFormat[] m_TextureFormatArray;
        [SerializeField]
        private bool m_TextureFormatIsDifferent;
        [SerializeField]
        public string name;

        public TextureImportPlatformSettings(string name, BuildTarget target, TextureImporterInspector inspector)
        {
            this.name = name;
            this.m_Target = target;
            this.m_Inspector = inspector;
            this.m_Overridden = false;
            if (<>f__am$cache15 == null)
            {
                <>f__am$cache15 = new Func<Object, TextureImporter>(TextureImportPlatformSettings.<TextureImportPlatformSettings>m__15C);
            }
            this.m_Importers = inspector.targets.Select<Object, TextureImporter>(<>f__am$cache15).ToArray<TextureImporter>();
            this.m_TextureFormatArray = new TextureImporterFormat[this.importers.Length];
            for (int i = 0; i < this.importers.Length; i++)
            {
                TextureImporterFormat textureFormat;
                int maxTextureSize;
                int compressionQuality;
                bool flag;
                TextureImporter importer = this.importers[i];
                if (!this.isDefault)
                {
                    flag = importer.GetPlatformTextureSettings(name, out maxTextureSize, out textureFormat, out compressionQuality);
                }
                else
                {
                    flag = true;
                    maxTextureSize = importer.maxTextureSize;
                    textureFormat = importer.textureFormat;
                    compressionQuality = importer.compressionQuality;
                }
                this.m_TextureFormatArray[i] = textureFormat;
                if (i == 0)
                {
                    this.m_Overridden = flag;
                    this.m_MaxTextureSize = maxTextureSize;
                    this.m_CompressionQuality = compressionQuality;
                }
                else
                {
                    if (flag != this.m_Overridden)
                    {
                        this.m_OverriddenIsDifferent = true;
                    }
                    if (maxTextureSize != this.m_MaxTextureSize)
                    {
                        this.m_MaxTextureSizeIsDifferent = true;
                    }
                    if (compressionQuality != this.m_CompressionQuality)
                    {
                        this.m_CompressionQualityIsDifferent = true;
                    }
                    if (textureFormat != this.m_TextureFormatArray[0])
                    {
                        this.m_TextureFormatIsDifferent = true;
                    }
                }
            }
            this.Sync();
        }

        [CompilerGenerated]
        private static TextureImporter <TextureImportPlatformSettings>m__15C(Object x)
        {
            return (x as TextureImporter);
        }

        public void Apply()
        {
            for (int i = 0; i < this.importers.Length; i++)
            {
                int maxTextureSize;
                TextureImporter importer = this.importers[i];
                int compressionQuality = -1;
                bool overridden = false;
                if (this.isDefault)
                {
                    maxTextureSize = importer.maxTextureSize;
                }
                else
                {
                    TextureImporterFormat format;
                    overridden = importer.GetPlatformTextureSettings(this.name, out maxTextureSize, out format, out compressionQuality);
                }
                if (!overridden)
                {
                    maxTextureSize = importer.maxTextureSize;
                }
                if (!this.m_MaxTextureSizeIsDifferent)
                {
                    maxTextureSize = this.m_MaxTextureSize;
                }
                if (!this.m_CompressionQualityIsDifferent)
                {
                    compressionQuality = this.m_CompressionQuality;
                }
                if (!this.isDefault)
                {
                    if (!this.m_OverriddenIsDifferent)
                    {
                        overridden = this.m_Overridden;
                    }
                    bool allowsAlphaSplit = false;
                    allowsAlphaSplit = importer.GetAllowsAlphaSplitting();
                    if (overridden)
                    {
                        importer.SetPlatformTextureSettings(this.name, maxTextureSize, this.m_TextureFormatArray[i], compressionQuality, allowsAlphaSplit);
                    }
                    else
                    {
                        importer.ClearPlatformTextureSettings(this.name);
                    }
                }
                else
                {
                    importer.maxTextureSize = maxTextureSize;
                    importer.textureFormat = this.m_TextureFormatArray[i];
                    importer.compressionQuality = compressionQuality;
                }
            }
        }

        private bool GetOverridden(TextureImporter importer)
        {
            int num;
            TextureImporterFormat format;
            if (!this.m_OverriddenIsDifferent)
            {
                return this.m_Overridden;
            }
            return importer.GetPlatformTextureSettings(this.name, out num, out format);
        }

        public TextureImporterSettings GetSettings(TextureImporter importer)
        {
            TextureImporterSettings dest = new TextureImporterSettings();
            importer.ReadTextureSettings(dest);
            this.m_Inspector.GetSerializedPropertySettings(dest);
            return dest;
        }

        public virtual bool HasChanged()
        {
            return this.m_HasChanged;
        }

        public virtual void SetChanged()
        {
            this.m_HasChanged = true;
        }

        public void SetCompressionQualityForAll(int quality)
        {
            this.m_CompressionQuality = quality;
            this.m_CompressionQualityIsDifferent = false;
            this.m_HasChanged = true;
        }

        public void SetMaxTextureSizeForAll(int maxTextureSize)
        {
            this.m_MaxTextureSize = maxTextureSize;
            this.m_MaxTextureSizeIsDifferent = false;
            this.m_HasChanged = true;
        }

        public void SetOverriddenForAll(bool overridden)
        {
            this.m_Overridden = overridden;
            this.m_OverriddenIsDifferent = false;
            this.m_HasChanged = true;
        }

        public void SetTextureFormatForAll(TextureImporterFormat format)
        {
            for (int i = 0; i < this.m_TextureFormatArray.Length; i++)
            {
                this.m_TextureFormatArray[i] = format;
            }
            this.m_TextureFormatIsDifferent = false;
            this.m_HasChanged = true;
        }

        public bool SupportsFormat(TextureImporterFormat format, TextureImporter importer)
        {
            int[] kTextureFormatsValueSTV;
            TextureImporterSettings settings = this.GetSettings(importer);
            BuildTarget target = this.m_Target;
            switch (target)
            {
                case BuildTarget.SamsungTV:
                    kTextureFormatsValueSTV = TextureImportPlatformSettings.kTextureFormatsValueSTV;
                    break;

                case BuildTarget.WiiU:
                    kTextureFormatsValueSTV = kTextureFormatsValueWiiU;
                    break;

                case BuildTarget.tvOS:
                    kTextureFormatsValueSTV = kTextureFormatsValuetvOS;
                    break;

                default:
                    if (target == BuildTarget.iOS)
                    {
                        kTextureFormatsValueSTV = kTextureFormatsValueiPhone;
                    }
                    else if (target == BuildTarget.Android)
                    {
                        kTextureFormatsValueSTV = kTextureFormatsValueAndroid;
                    }
                    else if (target == BuildTarget.Tizen)
                    {
                        kTextureFormatsValueSTV = kTextureFormatsValueTizen;
                    }
                    else
                    {
                        kTextureFormatsValueSTV = !settings.normalMap ? kTextureFormatsValueWeb : kNormalFormatsValueWeb;
                    }
                    break;
            }
            return kTextureFormatsValueSTV.Contains((int) format);
        }

        public void Sync()
        {
            if (!this.isDefault && (!this.m_Overridden || this.m_OverriddenIsDifferent))
            {
                TextureImportPlatformSettings settings = this.m_Inspector.m_PlatformSettings[0];
                this.m_MaxTextureSize = settings.m_MaxTextureSize;
                this.m_MaxTextureSizeIsDifferent = settings.m_MaxTextureSizeIsDifferent;
                this.m_TextureFormatArray = (TextureImporterFormat[]) settings.m_TextureFormatArray.Clone();
                this.m_TextureFormatIsDifferent = settings.m_TextureFormatIsDifferent;
                this.m_CompressionQuality = settings.m_CompressionQuality;
                this.m_CompressionQualityIsDifferent = settings.m_CompressionQualityIsDifferent;
            }
            TextureImporterType textureType = this.m_Inspector.textureType;
            for (int i = 0; i < this.importers.Length; i++)
            {
                TextureImporter importer = this.importers[i];
                TextureImporterSettings settings2 = this.GetSettings(importer);
                if (textureType == TextureImporterType.Advanced)
                {
                    if (this.isDefault)
                    {
                        continue;
                    }
                    if (!this.SupportsFormat(this.m_TextureFormatArray[i], importer))
                    {
                        this.m_TextureFormatArray[i] = TextureImporter.FullToSimpleTextureFormat(this.m_TextureFormatArray[i]);
                    }
                    if (this.m_TextureFormatArray[i] < ~TextureImporterFormat.AutomaticCompressed)
                    {
                        this.m_TextureFormatArray[i] = TextureImporter.SimpleToFullTextureFormat2(this.m_TextureFormatArray[i], textureType, settings2, importer.DoesSourceTextureHaveAlpha(), importer.IsSourceTextureHDR(), this.m_Target);
                    }
                }
                else if (this.m_TextureFormatArray[i] >= ~TextureImporterFormat.AutomaticCompressed)
                {
                    this.m_TextureFormatArray[i] = TextureImporter.FullToSimpleTextureFormat(this.m_TextureFormatArray[i]);
                }
                if (settings2.normalMap && !TextureImporterInspector.IsGLESMobileTargetPlatform(this.m_Target))
                {
                    this.m_TextureFormatArray[i] = TextureImporterInspector.MakeTextureFormatHaveAlpha(this.m_TextureFormatArray[i]);
                }
            }
            this.m_TextureFormatIsDifferent = false;
            foreach (TextureImporterFormat format in this.m_TextureFormatArray)
            {
                if (format != this.m_TextureFormatArray[0])
                {
                    this.m_TextureFormatIsDifferent = true;
                }
            }
        }

        public bool allAreOverridden
        {
            get
            {
                return (this.isDefault || (this.m_Overridden && !this.m_OverriddenIsDifferent));
            }
        }

        public int compressionQuality
        {
            get
            {
                return this.m_CompressionQuality;
            }
        }

        public bool compressionQualityIsDifferent
        {
            get
            {
                return this.m_CompressionQualityIsDifferent;
            }
        }

        public TextureImporter[] importers
        {
            get
            {
                return this.m_Importers;
            }
        }

        public bool isDefault
        {
            get
            {
                return (this.name == string.Empty);
            }
        }

        public int maxTextureSize
        {
            get
            {
                return this.m_MaxTextureSize;
            }
        }

        public bool maxTextureSizeIsDifferent
        {
            get
            {
                return this.m_MaxTextureSizeIsDifferent;
            }
        }

        public bool overridden
        {
            get
            {
                return this.m_Overridden;
            }
        }

        public bool overriddenIsDifferent
        {
            get
            {
                return this.m_OverriddenIsDifferent;
            }
        }

        public bool textureFormatIsDifferent
        {
            get
            {
                return this.m_TextureFormatIsDifferent;
            }
        }

        public TextureImporterFormat[] textureFormats
        {
            get
            {
                return this.m_TextureFormatArray;
            }
        }
    }
}

