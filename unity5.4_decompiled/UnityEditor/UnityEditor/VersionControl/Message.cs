﻿namespace UnityEditor.VersionControl
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class Message
    {
        private IntPtr m_thisDummy;

        internal Message()
        {
        }

        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe, WrapperlessIcall]
        public extern void Dispose();
        ~Message()
        {
            this.Dispose();
        }

        private static void Info(string message)
        {
            Debug.Log("Version control:\n" + message);
        }

        public void Show()
        {
            Info(this.message);
        }

        [ThreadAndSerializationSafe]
        public string message { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        [ThreadAndSerializationSafe]
        public Severity severity { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        [Flags]
        public enum Severity
        {
            Data,
            Verbose,
            Info,
            Warning,
            Error
        }
    }
}

