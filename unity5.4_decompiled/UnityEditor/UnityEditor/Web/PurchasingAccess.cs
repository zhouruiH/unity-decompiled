﻿namespace UnityEditor.Web
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Net;
    using System.Net.Security;
    using System.Runtime.CompilerServices;
    using System.Security.Cryptography.X509Certificates;
    using UnityEditor;
    using UnityEditor.Connect;
    using UnityEngine;
    using UnityEngine.Connect;

    [InitializeOnLoad]
    internal class PurchasingAccess : CloudServiceAccess
    {
        [CompilerGenerated]
        private static RemoteCertificateValidationCallback <>f__am$cache2;
        private const string kETagPath = "Assets/Plugins/UnityPurchasing/ETag";
        private static readonly Uri kPackageUri = new Uri("https://public-cdn.cloud.unity3d.com/UnityEngine.Cloud.Purchasing.unitypackage");
        private const string kServiceDisplayName = "In App Purchasing";
        private const string kServiceName = "Purchasing";
        private const string kServiceUrl = "https://public-cdn.cloud.unity3d.com/editor/5.4/production/cloud/purchasing";
        private const string kUnknownPackageETag = "unknown";
        private bool m_InstallInProgress;

        static PurchasingAccess()
        {
            UnityConnectServiceData cloudService = new UnityConnectServiceData("Purchasing", "https://public-cdn.cloud.unity3d.com/editor/5.4/production/cloud/purchasing", new PurchasingAccess(), "unity/project/cloud/purchasing");
            UnityConnectServiceCollection.instance.AddService(cloudService);
        }

        public override void EnableService(bool enabled)
        {
            UnityPurchasingSettings.enabled = enabled;
        }

        public string GetInstalledETag()
        {
            if (File.Exists("Assets/Plugins/UnityPurchasing/ETag"))
            {
                return File.ReadAllText("Assets/Plugins/UnityPurchasing/ETag");
            }
            if (Directory.Exists(Path.GetDirectoryName("Assets/Plugins/UnityPurchasing/ETag")))
            {
                return "unknown";
            }
            return null;
        }

        public override string GetServiceDisplayName()
        {
            return "In App Purchasing";
        }

        public override string GetServiceName()
        {
            return "Purchasing";
        }

        public void InstallUnityPackage()
        {
            <InstallUnityPackage>c__AnonStoreyC5 yc = new <InstallUnityPackage>c__AnonStoreyC5 {
                <>f__this = this
            };
            if (!this.m_InstallInProgress)
            {
                yc.originalCallback = ServicePointManager.ServerCertificateValidationCallback;
                if (Application.platform != RuntimePlatform.OSXEditor)
                {
                    if (<>f__am$cache2 == null)
                    {
                        <>f__am$cache2 = (a, b, c, d) => true;
                    }
                    ServicePointManager.ServerCertificateValidationCallback = <>f__am$cache2;
                }
                this.m_InstallInProgress = true;
                yc.location = FileUtil.GetUniqueTempPathInProject();
                yc.location = Path.ChangeExtension(yc.location, ".unitypackage");
                yc.client = new WebClient();
                yc.client.DownloadFileCompleted += new AsyncCompletedEventHandler(yc.<>m__257);
                yc.client.DownloadFileAsync(kPackageUri, yc.location);
            }
        }

        public override bool IsServiceEnabled()
        {
            return UnityPurchasingSettings.enabled;
        }

        private void SaveETag(WebClient client)
        {
            string contents = client.ResponseHeaders.Get("ETag");
            if (contents != null)
            {
                Directory.CreateDirectory(Path.GetDirectoryName("Assets/Plugins/UnityPurchasing/ETag"));
                File.WriteAllText("Assets/Plugins/UnityPurchasing/ETag", contents);
            }
        }

        [CompilerGenerated]
        private sealed class <InstallUnityPackage>c__AnonStoreyC5
        {
            internal PurchasingAccess <>f__this;
            internal WebClient client;
            internal string location;
            internal RemoteCertificateValidationCallback originalCallback;

            internal void <>m__257(object sender, AsyncCompletedEventArgs args)
            {
                <InstallUnityPackage>c__AnonStoreyC6 yc;
                yc = new <InstallUnityPackage>c__AnonStoreyC6 {
                    <>f__ref$197 = this,
                    args = args,
                    handler = null,
                    handler = new EditorApplication.CallbackFunction(yc.<>m__258)
                };
                EditorApplication.update = (EditorApplication.CallbackFunction) Delegate.Combine(EditorApplication.update, yc.handler);
            }

            private sealed class <InstallUnityPackage>c__AnonStoreyC6
            {
                internal PurchasingAccess.<InstallUnityPackage>c__AnonStoreyC5 <>f__ref$197;
                internal AsyncCompletedEventArgs args;
                internal EditorApplication.CallbackFunction handler;

                internal void <>m__258()
                {
                    ServicePointManager.ServerCertificateValidationCallback = this.<>f__ref$197.originalCallback;
                    EditorApplication.update = (EditorApplication.CallbackFunction) Delegate.Remove(EditorApplication.update, this.handler);
                    this.<>f__ref$197.<>f__this.m_InstallInProgress = false;
                    if (this.args.Error == null)
                    {
                        this.<>f__ref$197.<>f__this.SaveETag(this.<>f__ref$197.client);
                        AssetDatabase.ImportPackage(this.<>f__ref$197.location, false);
                    }
                    else
                    {
                        Debug.LogWarning("Failed to download IAP package. Please check connectivity and retry.");
                        Debug.LogException(this.args.Error);
                    }
                }
            }
        }
    }
}

