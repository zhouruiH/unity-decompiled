﻿namespace UnityEditor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class CustomEditorAttributes
    {
        private static readonly List<MonoEditorType> kSCustomEditors = new List<MonoEditorType>();
        private static readonly List<MonoEditorType> kSCustomMultiEditors = new List<MonoEditorType>();
        private static bool s_Initialized;

        internal static Type FindCustomEditorType(Object o, bool multiEdit)
        {
            return FindCustomEditorTypeByType(o.GetType(), multiEdit);
        }

        internal static Type FindCustomEditorTypeByType(Type type, bool multiEdit)
        {
            <FindCustomEditorTypeByType>c__AnonStorey3A storeya = new <FindCustomEditorTypeByType>c__AnonStorey3A {
                type = type
            };
            if (!s_Initialized)
            {
                Assembly[] loadedAssemblies = EditorAssemblies.loadedAssemblies;
                for (int i = loadedAssemblies.Length - 1; i >= 0; i--)
                {
                    Rebuild(loadedAssemblies[i]);
                }
                s_Initialized = true;
            }
            List<MonoEditorType> source = !multiEdit ? kSCustomEditors : kSCustomMultiEditors;
            <FindCustomEditorTypeByType>c__AnonStorey3C storeyc = new <FindCustomEditorTypeByType>c__AnonStorey3C {
                pass = 0
            };
            while (storeyc.pass < 2)
            {
                <FindCustomEditorTypeByType>c__AnonStorey3B storeyb = new <FindCustomEditorTypeByType>c__AnonStorey3B {
                    <>f__ref$58 = storeya,
                    <>f__ref$60 = storeyc,
                    inspected = storeya.type
                };
                while (storeyb.inspected != null)
                {
                    MonoEditorType type2 = source.FirstOrDefault<MonoEditorType>(new Func<MonoEditorType, bool>(storeyb.<>m__5B));
                    if (type2 != null)
                    {
                        return type2.m_InspectorType;
                    }
                    storeyb.inspected = storeyb.inspected.BaseType;
                }
                storeyc.pass++;
            }
            return null;
        }

        internal static void Rebuild(Assembly assembly)
        {
            foreach (Type type in AssemblyHelper.GetTypesFromAssembly(assembly))
            {
                foreach (CustomEditor editor in type.GetCustomAttributes(typeof(CustomEditor), false))
                {
                    MonoEditorType item = new MonoEditorType();
                    if (editor.m_InspectedType == null)
                    {
                        Debug.Log("Can't load custom inspector " + type.Name + " because the inspected type is null.");
                    }
                    else if (!type.IsSubclassOf(typeof(Editor)))
                    {
                        if (((type.FullName != "TweakMode") || !type.IsEnum) || (editor.m_InspectedType.FullName != "BloomAndFlares"))
                        {
                            Debug.LogWarning(type.Name + " uses the CustomEditor attribute but does not inherit from Editor.\nYou must inherit from Editor. See the Editor class script documentation.");
                        }
                    }
                    else
                    {
                        item.m_InspectedType = editor.m_InspectedType;
                        item.m_InspectorType = type;
                        item.m_EditorForChildClasses = editor.m_EditorForChildClasses;
                        item.m_IsFallback = editor.isFallback;
                        kSCustomEditors.Add(item);
                        if (type.GetCustomAttributes(typeof(CanEditMultipleObjects), false).Length > 0)
                        {
                            kSCustomMultiEditors.Add(item);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <FindCustomEditorTypeByType>c__AnonStorey3A
        {
            internal Type type;
        }

        [CompilerGenerated]
        private sealed class <FindCustomEditorTypeByType>c__AnonStorey3B
        {
            internal CustomEditorAttributes.<FindCustomEditorTypeByType>c__AnonStorey3A <>f__ref$58;
            internal CustomEditorAttributes.<FindCustomEditorTypeByType>c__AnonStorey3C <>f__ref$60;
            internal Type inspected;

            internal bool <>m__5B(CustomEditorAttributes.MonoEditorType x)
            {
                if ((this.<>f__ref$58.type != this.inspected) && !x.m_EditorForChildClasses)
                {
                    return false;
                }
                if ((this.<>f__ref$60.pass == 1) != x.m_IsFallback)
                {
                    return false;
                }
                return (this.inspected == x.m_InspectedType);
            }
        }

        [CompilerGenerated]
        private sealed class <FindCustomEditorTypeByType>c__AnonStorey3C
        {
            internal int pass;
        }

        private class MonoEditorType
        {
            public bool m_EditorForChildClasses;
            public Type m_InspectedType;
            public Type m_InspectorType;
            public bool m_IsFallback;
        }
    }
}

