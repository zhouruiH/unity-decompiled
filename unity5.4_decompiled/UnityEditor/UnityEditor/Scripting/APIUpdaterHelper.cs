﻿namespace UnityEditor.Scripting
{
    using Mono.Cecil;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Text.RegularExpressions;
    using Unity.DataContract;
    using UnityEditor;
    using UnityEditor.Modules;
    using UnityEditor.Scripting.Compilers;
    using UnityEditor.Utils;
    using UnityEngine;

    internal class APIUpdaterHelper
    {
        [CompilerGenerated]
        private static Func<Assembly, IEnumerable<Type>> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<string, Exception, string> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<KeyValuePair<string, PackageFileData>, bool> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<KeyValuePair<string, PackageFileData>, string> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<CustomAttribute, bool> <>f__am$cache4;

        private static string APIVersionArgument()
        {
            return (" --api-version " + Application.unityVersion + " ");
        }

        private static string AssemblySearchPathArgument()
        {
            string[] textArray1 = new string[] { Path.Combine(MonoInstallationFinder.GetFrameWorksFolder(), "Managed"), ",+", Path.Combine(EditorApplication.applicationContentsPath, "UnityExtensions/Unity"), ",+", Application.dataPath };
            string str = string.Concat(textArray1);
            return (" -s \"" + str + "\"");
        }

        private static string ConfigurationProviderAssembliesPathArgument()
        {
            StringBuilder builder = new StringBuilder();
            IEnumerator<PackageInfo> enumerator = ModuleManager.packageManager.unityExtensions.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PackageInfo current = enumerator.Current;
                    if (<>f__am$cache2 == null)
                    {
                        <>f__am$cache2 = f => f.Value.type == PackageFileType.Dll;
                    }
                    if (<>f__am$cache3 == null)
                    {
                        <>f__am$cache3 = pi => pi.Key;
                    }
                    IEnumerator<string> enumerator2 = current.files.Where<KeyValuePair<string, PackageFileData>>(<>f__am$cache2).Select<KeyValuePair<string, PackageFileData>, string>(<>f__am$cache3).GetEnumerator();
                    try
                    {
                        while (enumerator2.MoveNext())
                        {
                            string str = enumerator2.Current;
                            builder.AppendFormat(" {0}", CommandLineFormatter.PrepareFileName(Path.Combine(current.basePath, str)));
                        }
                        continue;
                    }
                    finally
                    {
                        if (enumerator2 == null)
                        {
                        }
                        enumerator2.Dispose();
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            string unityEditorManagedPath = GetUnityEditorManagedPath();
            builder.AppendFormat(" {0}", CommandLineFormatter.PrepareFileName(Path.Combine(unityEditorManagedPath, "UnityEngine.dll")));
            builder.AppendFormat(" {0}", CommandLineFormatter.PrepareFileName(Path.Combine(unityEditorManagedPath, "UnityEditor.dll")));
            return builder.ToString();
        }

        public static bool DoesAssemblyRequireUpgrade(string assemblyFullPath)
        {
            if (File.Exists(assemblyFullPath))
            {
                string str;
                string str2;
                if (!AssemblyHelper.IsManagedAssembly(assemblyFullPath))
                {
                    return false;
                }
                if (!MayContainUpdatableReferences(assemblyFullPath))
                {
                    return false;
                }
                int num = RunUpdatingProgram("AssemblyUpdater.exe", TimeStampArgument() + APIVersionArgument() + "--check-update-required -a " + CommandLineFormatter.PrepareFileName(assemblyFullPath) + AssemblySearchPathArgument() + ConfigurationProviderAssembliesPathArgument(), out str, out str2);
                Console.WriteLine("{0}{1}", str, str2);
                switch (num)
                {
                    case 0:
                    case 1:
                        return false;

                    case 2:
                        return true;
                }
                Debug.LogError(str + Environment.NewLine + str2);
            }
            return false;
        }

        private static string GetUnityEditorManagedPath()
        {
            return Path.Combine(MonoInstallationFinder.GetFrameWorksFolder(), "Managed");
        }

        public static bool IsReferenceToMissingObsoleteMember(string namespaceName, string className)
        {
            bool flag;
            <IsReferenceToMissingObsoleteMember>c__AnonStoreyBE ybe = new <IsReferenceToMissingObsoleteMember>c__AnonStoreyBE {
                className = className,
                namespaceName = namespaceName
            };
            try
            {
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = (Func<Assembly, IEnumerable<Type>>) (a => a.GetTypes());
                }
                flag = AppDomain.CurrentDomain.GetAssemblies().SelectMany<Assembly, Type>(<>f__am$cache0).FirstOrDefault<Type>(new Func<Type, bool>(ybe.<>m__22F)) != null;
            }
            catch (ReflectionTypeLoadException exception)
            {
                if (<>f__am$cache1 == null)
                {
                    <>f__am$cache1 = (acc, curr) => acc + "\r\n\t" + curr.Message;
                }
                throw new Exception(exception.Message + exception.LoaderExceptions.Aggregate<Exception, string>(string.Empty, <>f__am$cache1));
            }
            return flag;
        }

        private static bool IsTargetFrameworkValidOnCurrentOS(AssemblyDefinition assembly)
        {
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                return true;
            }
            if (assembly.HasCustomAttributes)
            {
            }
            return !((<>f__am$cache4 == null) && assembly.CustomAttributes.Any<CustomAttribute>(<>f__am$cache4));
        }

        private static bool IsUpdateable(Type type)
        {
            object[] customAttributes = type.GetCustomAttributes(typeof(ObsoleteAttribute), false);
            if (customAttributes.Length != 1)
            {
                return false;
            }
            ObsoleteAttribute attribute = (ObsoleteAttribute) customAttributes[0];
            return attribute.Message.Contains("UnityUpgradable");
        }

        internal static bool MayContainUpdatableReferences(string assemblyPath)
        {
            using (FileStream stream = File.Open(assemblyPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                AssemblyDefinition assembly = AssemblyDefinition.ReadAssembly(stream);
                if (assembly.Name.IsWindowsRuntime)
                {
                    return false;
                }
                if (!IsTargetFrameworkValidOnCurrentOS(assembly))
                {
                    return false;
                }
            }
            return true;
        }

        private static string ResolveAssemblyPath(string assemblyPath)
        {
            return CommandLineFormatter.PrepareFileName(assemblyPath);
        }

        public static void Run(string commaSeparatedListOfAssemblies)
        {
            char[] separator = new char[] { ',' };
            string[] source = commaSeparatedListOfAssemblies.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            object[] args = new object[] { source.Count<string>() };
            APIUpdaterLogger.WriteToFile("Started to update {0} assemblie(s)", args);
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            foreach (string str in source)
            {
                if (AssemblyHelper.IsManagedAssembly(str))
                {
                    string str2;
                    string str3;
                    string str4 = ResolveAssemblyPath(str);
                    int num2 = RunUpdatingProgram("AssemblyUpdater.exe", "-u -a " + str4 + APIVersionArgument() + AssemblySearchPathArgument() + ConfigurationProviderAssembliesPathArgument(), out str2, out str3);
                    if (str2.Length > 0)
                    {
                        object[] objArray2 = new object[] { str4, str2 };
                        APIUpdaterLogger.WriteToFile("Assembly update output ({0})\r\n{1}", objArray2);
                    }
                    if (num2 < 0)
                    {
                        object[] objArray3 = new object[] { num2, str3 };
                        APIUpdaterLogger.WriteErrorToConsole("Error {0} running AssemblyUpdater. Its output is: `{1}`", objArray3);
                    }
                }
            }
            object[] objArray4 = new object[] { stopwatch.Elapsed.TotalSeconds };
            APIUpdaterLogger.WriteToFile("Update finished in {0}s", objArray4);
        }

        private static int RunUpdatingProgram(string executable, string arguments, out string stdOut, out string stdErr)
        {
            string str = EditorApplication.applicationContentsPath + "/Tools/ScriptUpdater/" + executable;
            ManagedProgram program = new ManagedProgram(MonoInstallationFinder.GetMonoInstallation("MonoBleedingEdge"), "4.0", str, arguments, null);
            program.LogProcessStartInfo();
            program.Start();
            program.WaitForExit();
            stdOut = program.GetStandardOutputAsString();
            stdErr = string.Join("\r\n", program.GetErrorOutput());
            return program.ExitCode;
        }

        private static bool TargetsWindowsSpecificFramework(CustomAttribute targetFrameworkAttr)
        {
            <TargetsWindowsSpecificFramework>c__AnonStoreyBF ybf = new <TargetsWindowsSpecificFramework>c__AnonStoreyBF();
            if (!targetFrameworkAttr.AttributeType.FullName.Contains("System.Runtime.Versioning.TargetFrameworkAttribute"))
            {
                return false;
            }
            ybf.regex = new Regex(@"\.NETCore|\.NETPortable");
            return targetFrameworkAttr.ConstructorArguments.Any<CustomAttributeArgument>(new Func<CustomAttributeArgument, bool>(ybf.<>m__234));
        }

        private static string TimeStampArgument()
        {
            return (" --timestamp " + DateTime.Now.Ticks + " ");
        }

        [CompilerGenerated]
        private sealed class <IsReferenceToMissingObsoleteMember>c__AnonStoreyBE
        {
            internal string className;
            internal string namespaceName;

            internal bool <>m__22F(Type t)
            {
                return (((t.Name == this.className) && (t.Namespace == this.namespaceName)) && APIUpdaterHelper.IsUpdateable(t));
            }
        }

        [CompilerGenerated]
        private sealed class <TargetsWindowsSpecificFramework>c__AnonStoreyBF
        {
            internal Regex regex;

            internal bool <>m__234(CustomAttributeArgument arg)
            {
                return ((arg.Type.FullName == typeof(string).FullName) && this.regex.IsMatch((string) arg.Value));
            }
        }
    }
}

