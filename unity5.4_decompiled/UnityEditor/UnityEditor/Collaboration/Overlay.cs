﻿namespace UnityEditor.Collaboration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using UnityEditor;
    using UnityEngine;

    internal class Overlay
    {
        private static readonly Dictionary<Collab.CollabStates, Texture2D> s_Overlays = new Dictionary<Collab.CollabStates, Texture2D>();

        protected static bool AreOverlaysLoaded()
        {
            if (s_Overlays.Count == 0)
            {
                return false;
            }
            foreach (Texture2D textured in s_Overlays.Values)
            {
                if (textured == null)
                {
                    return false;
                }
            }
            return true;
        }

        protected static void DrawOverlayElement(Collab.CollabStates singleState, Rect itemRect)
        {
            Texture2D textured;
            if (s_Overlays.TryGetValue(singleState, out textured) && (textured != null))
            {
                GUI.DrawTexture(itemRect, textured);
            }
        }

        public static void DrawOverlays(Collab.CollabStates assetState, Rect itemRect)
        {
            if (((assetState != Collab.CollabStates.kCollabInvalidState) && (assetState != Collab.CollabStates.kCollabNone)) && (Event.current.type == EventType.Repaint))
            {
                if (!AreOverlaysLoaded())
                {
                    LoadOverlays();
                }
                DrawOverlayElement(GetOverlayStateForAsset(assetState), itemRect);
            }
        }

        protected static Collab.CollabStates GetOverlayStateForAsset(Collab.CollabStates assetStates)
        {
            foreach (Collab.CollabStates states in s_Overlays.Keys)
            {
                if (HasState(assetStates, states))
                {
                    return states;
                }
            }
            return Collab.CollabStates.kCollabNone;
        }

        protected static bool HasState(Collab.CollabStates assetStates, Collab.CollabStates includesState)
        {
            return ((assetStates & includesState) == includesState);
        }

        protected static void LoadOverlays()
        {
            s_Overlays.Clear();
            s_Overlays.Add(Collab.CollabStates.kCollabConflicted, LoadTextureFromApplicationContents("conflict.png"));
            s_Overlays.Add(Collab.CollabStates.kCollabNone | Collab.CollabStates.kCollabPendingMerge, LoadTextureFromApplicationContents("conflict.png"));
            s_Overlays.Add(Collab.CollabStates.kCollabChanges, LoadTextureFromApplicationContents("changes.png"));
            s_Overlays.Add(Collab.CollabStates.kCollabCheckedOutLocal | Collab.CollabStates.kCollabMovedLocal, LoadTextureFromApplicationContents("modif-local.png"));
            s_Overlays.Add(Collab.CollabStates.kCollabAddedLocal, LoadTextureFromApplicationContents("added-local.png"));
            s_Overlays.Add(Collab.CollabStates.kCollabCheckedOutLocal, LoadTextureFromApplicationContents("modif-local.png"));
            s_Overlays.Add(Collab.CollabStates.kCollabDeletedLocal, LoadTextureFromApplicationContents("deleted-local.png"));
            s_Overlays.Add(Collab.CollabStates.kCollabMovedLocal, LoadTextureFromApplicationContents("modif-local.png"));
        }

        protected static Texture2D LoadTextureFromApplicationContents(string path)
        {
            Texture2D textured = new Texture2D(2, 2);
            path = Path.Combine(Path.Combine(Path.Combine(Path.Combine(EditorApplication.applicationContentsPath, "Resources"), "Collab"), "overlays"), path);
            try
            {
                FileStream stream = File.OpenRead(path);
                byte[] array = new byte[stream.Length];
                stream.Read(array, 0, (int) stream.Length);
                if (!textured.LoadImage(array))
                {
                    return null;
                }
            }
            catch (Exception)
            {
                Debug.LogWarning("Collab Overlay Texture load fail, path: " + path);
                return null;
            }
            return textured;
        }
    }
}

