﻿namespace UnityEditor
{
    using System;
    using UnityEditor.Collaboration;
    using UnityEditorInternal;
    using UnityEngine;

    internal class CacheServerPreferences
    {
        private static bool s_CacheServerEnabled;
        private static string s_CacheServerIPAddress;
        private static bool s_CollabCacheEnabled;
        private static string s_CollabCacheIPAddress;
        private static ConnectionState s_ConnectionState;
        private static bool s_PrefsLoaded;

        [PreferenceItem("Cache Server")]
        public static void OnGUI()
        {
            GUILayout.Space(10f);
            if (!InternalEditorUtility.HasTeamLicense())
            {
                GUILayout.Label(EditorGUIUtility.TempContent("You need to have a Pro or Team license to use the cache server.", EditorGUIUtility.GetHelpIcon(MessageType.Warning)), EditorStyles.helpBox, new GUILayoutOption[0]);
            }
            using (new EditorGUI.DisabledScope(!InternalEditorUtility.HasTeamLicense()))
            {
                if (!s_PrefsLoaded)
                {
                    ReadPreferences();
                    if (s_CacheServerEnabled && (s_ConnectionState == ConnectionState.Unknown))
                    {
                        if (InternalEditorUtility.CanConnectToCacheServer())
                        {
                            s_ConnectionState = ConnectionState.Success;
                        }
                        else
                        {
                            s_ConnectionState = ConnectionState.Failure;
                        }
                    }
                    s_PrefsLoaded = true;
                }
                EditorGUI.BeginChangeCheck();
                if (Collab.instance.collabInfo.whitelisted)
                {
                    s_CollabCacheEnabled = EditorGUILayout.Toggle("Use Collab Cache", s_CollabCacheEnabled, new GUILayoutOption[0]);
                    using (new EditorGUI.DisabledScope(!s_CollabCacheEnabled))
                    {
                        s_CollabCacheIPAddress = EditorGUILayout.TextField("Collab Cache IP Address", s_CollabCacheIPAddress, new GUILayoutOption[0]);
                    }
                }
                s_CacheServerEnabled = EditorGUILayout.Toggle("Use Cache Server", s_CacheServerEnabled, new GUILayoutOption[0]);
                using (new EditorGUI.DisabledScope(!s_CacheServerEnabled))
                {
                    Rect controlRect = EditorGUILayout.GetControlRect(true, new GUILayoutOption[0]);
                    int controlID = GUIUtility.GetControlID(FocusType.Keyboard, controlRect);
                    s_CacheServerIPAddress = EditorGUI.DelayedTextFieldInternal(controlRect, controlID, GUIContent.Temp("IP Address"), s_CacheServerIPAddress, null, EditorStyles.textField);
                    if (GUI.changed)
                    {
                        s_ConnectionState = ConnectionState.Unknown;
                        WritePreferences();
                    }
                    GUILayout.Space(5f);
                    GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Width(150f) };
                    if (GUILayout.Button("Check Connection", options))
                    {
                        if (EditorGUI.s_DelayedTextEditor.IsEditingControl(controlID))
                        {
                            string text = EditorGUI.s_DelayedTextEditor.text;
                            EditorGUI.s_DelayedTextEditor.EndEditing();
                            if (text != s_CacheServerIPAddress)
                            {
                                s_CacheServerIPAddress = text;
                                WritePreferences();
                            }
                        }
                        if (InternalEditorUtility.CanConnectToCacheServer())
                        {
                            s_ConnectionState = ConnectionState.Success;
                        }
                        else
                        {
                            s_ConnectionState = ConnectionState.Failure;
                        }
                    }
                    GUILayout.Space(-25f);
                    switch (s_ConnectionState)
                    {
                        case ConnectionState.Unknown:
                            GUILayout.Space(44f);
                            goto Label_025C;

                        case ConnectionState.Success:
                            EditorGUILayout.HelpBox("Connection successful.", MessageType.Info, false);
                            goto Label_025C;

                        case ConnectionState.Failure:
                            EditorGUILayout.HelpBox("Connection failed.", MessageType.Warning, false);
                            goto Label_025C;
                    }
                }
            Label_025C:
                if (EditorGUI.EndChangeCheck())
                {
                    WritePreferences();
                    ReadPreferences();
                }
            }
        }

        public static void ReadPreferences()
        {
            s_CacheServerIPAddress = EditorPrefs.GetString("CacheServerIPAddress", s_CacheServerIPAddress);
            s_CacheServerEnabled = EditorPrefs.GetBool("CacheServerEnabled");
            s_CollabCacheIPAddress = EditorPrefs.GetString("CollabCacheIPAddress", s_CollabCacheIPAddress);
            s_CollabCacheEnabled = EditorPrefs.GetBool("CollabCacheEnabled");
        }

        public static void WritePreferences()
        {
            EditorPrefs.SetString("CacheServerIPAddress", s_CacheServerIPAddress);
            EditorPrefs.SetBool("CacheServerEnabled", s_CacheServerEnabled);
            EditorPrefs.SetString("CollabCacheIPAddress", s_CollabCacheIPAddress);
            EditorPrefs.SetBool("CollabCacheEnabled", s_CollabCacheEnabled);
        }

        private enum ConnectionState
        {
            Unknown,
            Success,
            Failure
        }
    }
}

