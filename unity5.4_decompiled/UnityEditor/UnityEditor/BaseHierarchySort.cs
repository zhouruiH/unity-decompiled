﻿namespace UnityEditor
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Obsolete("BaseHierarchySort is no longer supported because of performance reasons")]
    public abstract class BaseHierarchySort : IComparer<GameObject>
    {
        protected BaseHierarchySort()
        {
        }

        public virtual int Compare(GameObject lhs, GameObject rhs)
        {
            return 0;
        }

        public virtual GUIContent content
        {
            get
            {
                return null;
            }
        }
    }
}

