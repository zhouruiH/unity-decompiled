﻿namespace UnityEditor
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEditorInternal;
    using UnityEngine;

    [EditorWindowTitle(title="Animation", useTypeNameAsIconName=true)]
    internal class AnimationWindow : EditorWindow
    {
        [SerializeField]
        private AnimEditor m_AnimEditor;
        private GUIStyle m_LockButtonStyle;
        [SerializeField]
        private AnimationWindowPolicy m_Policy;
        private static List<AnimationWindow> s_AnimationWindows = new List<AnimationWindow>();

        private AnimationWindowSelectionItem BuildSelection(GameObject gameObject)
        {
            <BuildSelection>c__AnonStorey48 storey = new <BuildSelection>c__AnonStorey48 {
                selectedItem = StandaloneSelectionItem.Create()
            };
            storey.selectedItem.gameObject = gameObject;
            storey.selectedItem.animationClip = null;
            storey.selectedItem.timeOffset = 0f;
            storey.selectedItem.id = 0;
            if (storey.selectedItem.rootGameObject != null)
            {
                AnimationClip[] animationClips = AnimationUtility.GetAnimationClips(storey.selectedItem.rootGameObject);
                if ((storey.selectedItem.animationClip == null) && (storey.selectedItem.gameObject != null))
                {
                    storey.selectedItem.animationClip = (animationClips.Length <= 0) ? null : animationClips[0];
                }
                else if (!Array.Exists<AnimationClip>(animationClips, new Predicate<AnimationClip>(storey.<>m__80)))
                {
                    storey.selectedItem.animationClip = (animationClips.Length <= 0) ? null : animationClips[0];
                }
            }
            return storey.selectedItem;
        }

        public void ForceRefresh()
        {
            if (this.m_AnimEditor != null)
            {
                this.m_AnimEditor.state.ForceRefresh();
            }
        }

        public static List<AnimationWindow> GetAllAnimationWindows()
        {
            return s_AnimationWindows;
        }

        public void OnControllerChange()
        {
            this.OnSelectionChange();
        }

        public void OnDestroy()
        {
            Object.DestroyImmediate(this.m_AnimEditor);
        }

        public void OnDisable()
        {
            s_AnimationWindows.Remove(this);
            this.m_AnimEditor.OnDisable();
        }

        public void OnEnable()
        {
            if (this.m_AnimEditor == null)
            {
                this.m_AnimEditor = ScriptableObject.CreateInstance(typeof(AnimEditor)) as AnimEditor;
                this.m_AnimEditor.hideFlags = HideFlags.HideAndDontSave;
            }
            s_AnimationWindows.Add(this);
            base.titleContent = base.GetLocalizedTitleContent();
            this.OnSelectionChange();
        }

        public void OnFocus()
        {
            this.OnSelectionChange();
        }

        public void OnGUI()
        {
            this.m_AnimEditor.OnAnimEditorGUI(this, base.position);
        }

        public void OnLostFocus()
        {
            if (this.m_AnimEditor != null)
            {
                this.m_AnimEditor.OnLostFocus();
            }
        }

        public void OnSelectionChange()
        {
            if (this.m_AnimEditor != null)
            {
                GameObject activeGameObject = Selection.activeGameObject;
                if (((activeGameObject != null) && !EditorUtility.IsPersistent(activeGameObject)) && ((activeGameObject.hideFlags & HideFlags.NotEditable) == HideFlags.None))
                {
                    this.SynchronizePolicy();
                    AnimationWindowSelectionItem selectedItem = this.BuildSelection(activeGameObject);
                    if (!this.m_AnimEditor.locked && this.ShouldUpdateSelection(selectedItem))
                    {
                        this.m_AnimEditor.state.recording = false;
                        this.m_AnimEditor.selectedItem = selectedItem;
                    }
                }
            }
        }

        private bool ShouldUpdateSelection(AnimationWindowSelectionItem selectedItem)
        {
            <ShouldUpdateSelection>c__AnonStorey49 storey = new <ShouldUpdateSelection>c__AnonStorey49();
            if (selectedItem.rootGameObject == null)
            {
                return true;
            }
            storey.currentlySelectedItem = this.m_AnimEditor.selectedItem;
            return ((storey.currentlySelectedItem == null) || ((selectedItem.rootGameObject != storey.currentlySelectedItem.rootGameObject) || ((storey.currentlySelectedItem.animationClip == null) || ((storey.currentlySelectedItem.rootGameObject != null) && !Array.Exists<AnimationClip>(AnimationUtility.GetAnimationClips(storey.currentlySelectedItem.rootGameObject), new Predicate<AnimationClip>(storey.<>m__81))))));
        }

        protected virtual void ShowButton(Rect r)
        {
            if (this.m_LockButtonStyle == null)
            {
                this.m_LockButtonStyle = "IN LockButton";
            }
            EditorGUI.BeginChangeCheck();
            using (new EditorGUI.DisabledScope(this.m_AnimEditor.stateDisabled))
            {
                this.m_AnimEditor.locked = GUI.Toggle(r, this.m_AnimEditor.locked, GUIContent.none, this.m_LockButtonStyle);
            }
            if (EditorGUI.EndChangeCheck())
            {
                this.OnSelectionChange();
            }
        }

        private void SynchronizePolicy()
        {
            if (this.m_AnimEditor != null)
            {
                if (this.m_Policy == null)
                {
                    this.m_Policy = new AnimationWindowPolicy();
                    this.m_Policy.allowRecording = true;
                }
                if (this.m_Policy.unitialized)
                {
                    this.m_Policy.SynchronizeFrameRate = delegate (ref float frameRate, ref bool timeInFrames) {
                        AnimationWindowSelectionItem selectedItem = this.m_AnimEditor.selectedItem;
                        if ((selectedItem != null) && (selectedItem.animationClip != null))
                        {
                            frameRate = selectedItem.animationClip.frameRate;
                        }
                        else
                        {
                            frameRate = 60f;
                        }
                        timeInFrames = false;
                        return true;
                    };
                    this.m_Policy.unitialized = false;
                }
                this.m_AnimEditor.policy = this.m_Policy;
            }
        }

        public void Update()
        {
            this.m_AnimEditor.Update();
        }

        [CompilerGenerated]
        private sealed class <BuildSelection>c__AnonStorey48
        {
            internal StandaloneSelectionItem selectedItem;

            internal bool <>m__80(AnimationClip x)
            {
                return (x == this.selectedItem.animationClip);
            }
        }

        [CompilerGenerated]
        private sealed class <ShouldUpdateSelection>c__AnonStorey49
        {
            internal AnimationWindowSelectionItem currentlySelectedItem;

            internal bool <>m__81(AnimationClip x)
            {
                return (x == this.currentlySelectedItem.animationClip);
            }
        }
    }
}

