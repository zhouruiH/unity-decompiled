﻿namespace UnityEditor.Rendering
{
    using System;

    public enum ShaderQuality
    {
        Low,
        Medium,
        High
    }
}

