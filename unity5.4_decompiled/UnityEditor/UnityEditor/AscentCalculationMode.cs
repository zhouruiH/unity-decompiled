﻿namespace UnityEditor
{
    using System;

    public enum AscentCalculationMode
    {
        Legacy2x,
        FaceAscender,
        FaceBoundingBox
    }
}

