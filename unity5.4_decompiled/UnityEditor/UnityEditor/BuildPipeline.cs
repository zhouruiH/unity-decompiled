﻿namespace UnityEditor
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEditor.BuildReporting;
    using UnityEngine;
    using UnityEngine.Internal;

    public sealed class BuildPipeline
    {
        [CompilerGenerated]
        private static Func<EditorBuildSettingsScene, bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<EditorBuildSettingsScene, string> <>f__am$cache1;

        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundle(Object mainAsset, Object[] assets, string pathName)
        {
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundle(Object mainAsset, Object[] assets, string pathName, BuildAssetBundleOptions assetBundleOptions)
        {
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundle(Object mainAsset, Object[] assets, string pathName, out uint crc)
        {
            crc = 0;
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundle(Object mainAsset, Object[] assets, string pathName, out uint crc, BuildAssetBundleOptions assetBundleOptions)
        {
            crc = 0;
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static bool BuildAssetBundle(Object mainAsset, Object[] assets, string pathName, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            uint num;
            return BuildAssetBundle(mainAsset, assets, pathName, out num, assetBundleOptions, targetPlatform);
        }

        [Obsolete("BuildAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static bool BuildAssetBundle(Object mainAsset, Object[] assets, string pathName, out uint crc, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            crc = 0;
            try
            {
                return BuildAssetBundleInternal(mainAsset, assets, null, pathName, assetBundleOptions, targetPlatform, out crc);
            }
            catch (Exception exception)
            {
                LogBuildExceptionAndExit("BuildPipeline.BuildAssetBundle", exception);
                return false;
            }
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundleExplicitAssetNames(Object[] assets, string[] assetNames, string pathName)
        {
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundleExplicitAssetNames(Object[] assets, string[] assetNames, string pathName, BuildAssetBundleOptions assetBundleOptions)
        {
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundleExplicitAssetNames(Object[] assets, string[] assetNames, string pathName, out uint crc)
        {
            crc = 0;
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.", true)]
        public static bool BuildAssetBundleExplicitAssetNames(Object[] assets, string[] assetNames, string pathName, out uint crc, BuildAssetBundleOptions assetBundleOptions)
        {
            crc = 0;
            return WebPlayerAssetBundlesAreNoLongerSupported();
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static bool BuildAssetBundleExplicitAssetNames(Object[] assets, string[] assetNames, string pathName, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            uint num;
            return BuildAssetBundleExplicitAssetNames(assets, assetNames, pathName, out num, assetBundleOptions, targetPlatform);
        }

        [Obsolete("BuildAssetBundleExplicitAssetNames has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static bool BuildAssetBundleExplicitAssetNames(Object[] assets, string[] assetNames, string pathName, out uint crc, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            crc = 0;
            try
            {
                return BuildAssetBundleInternal(null, assets, assetNames, pathName, assetBundleOptions, targetPlatform, out crc);
            }
            catch (Exception exception)
            {
                LogBuildExceptionAndExit("BuildPipeline.BuildAssetBundleExplicitAssetNames", exception);
                return false;
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        private static extern bool BuildAssetBundleInternal(Object mainAsset, Object[] assets, string[] assetNames, string pathName, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform, out uint crc);
        [Obsolete("BuildAssetBundles signature has changed. Please specify the targetPlatform parameter", true), ExcludeFromDocs]
        public static AssetBundleManifest BuildAssetBundles(string outputPath)
        {
            BuildAssetBundleOptions none = BuildAssetBundleOptions.None;
            return BuildAssetBundles(outputPath, none);
        }

        [Obsolete("BuildAssetBundles signature has changed. Please specify the targetPlatform parameter", true)]
        public static AssetBundleManifest BuildAssetBundles(string outputPath, [DefaultValue("BuildAssetBundleOptions.None")] BuildAssetBundleOptions assetBundleOptions)
        {
            WebPlayerAssetBundlesAreNoLongerSupported();
            return null;
        }

        [ExcludeFromDocs, Obsolete("BuildAssetBundles signature has changed. Please specify the targetPlatform parameter", true)]
        public static AssetBundleManifest BuildAssetBundles(string outputPath, AssetBundleBuild[] builds)
        {
            BuildAssetBundleOptions none = BuildAssetBundleOptions.None;
            return BuildAssetBundles(outputPath, builds, none);
        }

        [Obsolete("BuildAssetBundles signature has changed. Please specify the targetPlatform parameter", true)]
        public static AssetBundleManifest BuildAssetBundles(string outputPath, AssetBundleBuild[] builds, [DefaultValue("BuildAssetBundleOptions.None")] BuildAssetBundleOptions assetBundleOptions)
        {
            WebPlayerAssetBundlesAreNoLongerSupported();
            return null;
        }

        public static AssetBundleManifest BuildAssetBundles(string outputPath, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            if (!Directory.Exists(outputPath))
            {
                Debug.LogError("The output path \"" + outputPath + "\" doesn't exist");
                return null;
            }
            try
            {
                return BuildAssetBundlesInternal(outputPath, assetBundleOptions, targetPlatform);
            }
            catch (Exception exception)
            {
                LogBuildExceptionAndExit("BuildPipeline.BuildAssetBundles", exception);
                return null;
            }
        }

        public static AssetBundleManifest BuildAssetBundles(string outputPath, AssetBundleBuild[] builds, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform)
        {
            if (!Directory.Exists(outputPath))
            {
                Debug.LogError("The output path \"" + outputPath + "\" doesn't exist");
                return null;
            }
            if (builds == null)
            {
                Debug.LogError("AssetBundleBuild cannot be null.");
                return null;
            }
            try
            {
                return BuildAssetBundlesWithInfoInternal(outputPath, builds, assetBundleOptions, targetPlatform);
            }
            catch (Exception exception)
            {
                LogBuildExceptionAndExit("BuildPipeline.BuildAssetBundles", exception);
                return null;
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        private static extern AssetBundleManifest BuildAssetBundlesInternal(string outputPath, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        private static extern AssetBundleManifest BuildAssetBundlesWithInfoInternal(string outputPath, AssetBundleBuild[] builds, BuildAssetBundleOptions assetBundleOptions, BuildTarget targetPlatform);
        public static string BuildPlayer(string[] levels, string locationPathName, BuildTarget target, BuildOptions options)
        {
            if (string.IsNullOrEmpty(locationPathName))
            {
                if (((options & BuildOptions.InstallInBuildFolder) == BuildOptions.CompressTextures) || !PostprocessBuildPlayer.SupportsInstallInBuildFolder(target))
                {
                    return "The 'locationPathName' parameter for BuildPipeline.BuildPlayer should not be null or empty.";
                }
            }
            else if (string.IsNullOrEmpty(Path.GetFileName(locationPathName)))
            {
                string extensionForBuildTarget = PostprocessBuildPlayer.GetExtensionForBuildTarget(target, options);
                if (!string.IsNullOrEmpty(extensionForBuildTarget))
                {
                    return string.Format("For the '{0}' target the 'locationPathName' parameter for BuildPipeline.BuildPlayer should not end with a directory separator.\nProvided path: '{1}', expected a path with the extension '.{2}'.", target, locationPathName, extensionForBuildTarget);
                }
            }
            try
            {
                return BuildPlayerInternal(levels, locationPathName, target, options).SummarizeErrors();
            }
            catch (Exception exception)
            {
                LogBuildExceptionAndExit("BuildPipeline.BuildPlayer", exception);
                return string.Empty;
            }
        }

        public static string BuildPlayer(EditorBuildSettingsScene[] levels, string locationPathName, BuildTarget target, BuildOptions options)
        {
            string[] strArray = null;
            if (levels != null)
            {
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = l => l.enabled;
                }
                if (<>f__am$cache1 == null)
                {
                    <>f__am$cache1 = l => l.path;
                }
                strArray = levels.Where<EditorBuildSettingsScene>(<>f__am$cache0).Select<EditorBuildSettingsScene, string>(<>f__am$cache1).ToArray<string>();
            }
            return BuildPlayer(strArray, locationPathName, target, options);
        }

        private static BuildReport BuildPlayerInternal(string[] levels, string locationPathName, BuildTarget target, BuildOptions options)
        {
            if (((BuildOptions.EnableHeadlessMode & options) != BuildOptions.CompressTextures) && ((BuildOptions.Development & options) != BuildOptions.CompressTextures))
            {
                throw new Exception("Unsupported build setting: cannot build headless development player");
            }
            if ((target == BuildTarget.WSAPlayer) && (EditorUserBuildSettings.wsaSDK == WSASDK.SDK80))
            {
                throw new Exception("Windows SDK 8.0 is no longer supported, please switch to Windows SDK 8.1");
            }
            return BuildPlayerInternalNoCheck(levels, locationPathName, target, options, false);
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern BuildReport BuildPlayerInternalNoCheck(string[] levels, string locationPathName, BuildTarget target, BuildOptions options, bool delayToAfterScriptReload);
        [Obsolete("BuildStreamedSceneAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static string BuildStreamedSceneAssetBundle(string[] levels, string locationPath, BuildTarget target)
        {
            return BuildPlayer(levels, locationPath, target, BuildOptions.BuildAdditionalStreamedScenes);
        }

        [Obsolete("BuildStreamedSceneAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static string BuildStreamedSceneAssetBundle(string[] levels, string locationPath, BuildTarget target, BuildOptions options)
        {
            return BuildPlayer(levels, locationPath, target, options | BuildOptions.BuildAdditionalStreamedScenes);
        }

        [Obsolete("BuildStreamedSceneAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static string BuildStreamedSceneAssetBundle(string[] levels, string locationPath, BuildTarget target, out uint crc)
        {
            return BuildStreamedSceneAssetBundle(levels, locationPath, target, out crc, BuildOptions.CompressTextures);
        }

        [Obsolete("BuildStreamedSceneAssetBundle has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static string BuildStreamedSceneAssetBundle(string[] levels, string locationPath, BuildTarget target, out uint crc, BuildOptions options)
        {
            crc = 0;
            try
            {
                BuildReport report = BuildPlayerInternal(levels, locationPath, target, (options | BuildOptions.BuildAdditionalStreamedScenes) | BuildOptions.ComputeCRC);
                crc = report.crc;
                string str = report.SummarizeErrors();
                Object.DestroyImmediate(report, true);
                return str;
            }
            catch (Exception exception)
            {
                LogBuildExceptionAndExit("BuildPipeline.BuildStreamedSceneAssetBundle", exception);
                return string.Empty;
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern string GetBuildTargetAdvancedLicenseName(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern BuildTarget GetBuildTargetByName(string platform);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern BuildTargetGroup GetBuildTargetGroup(BuildTarget platform);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern BuildTargetGroup GetBuildTargetGroupByName(string platform);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern string GetBuildTargetGroupDisplayName(BuildTargetGroup targetPlatformGroup);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern string GetBuildTargetGroupName(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall, ThreadAndSerializationSafe]
        internal static extern string GetBuildTargetName(BuildTarget targetPlatform);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern string GetBuildToolsDirectory(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern bool GetCRCForAssetBundle(string targetPath, out uint crc);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall, ThreadAndSerializationSafe]
        internal static extern string GetEditorTargetName();
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern bool GetHashForAssetBundle(string targetPath, out Hash128 hash);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern string GetMonoBinDirectory(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern string GetMonoLibDirectory(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern string GetMonoProfileLibDirectory(BuildTarget target, string profile);
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe, WrapperlessIcall]
        internal static extern string GetPlaybackEngineDirectory(BuildTarget target, BuildOptions options);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern bool IsBuildTargetSupported(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern bool IsUnityScriptEvalSupported(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall, ThreadAndSerializationSafe]
        internal static extern bool LicenseCheck(BuildTarget target);
        private static void LogBuildExceptionAndExit(string buildFunctionName, Exception exception)
        {
            object[] args = new object[] { buildFunctionName };
            Debug.LogErrorFormat("Internal Error in {0}:", args);
            Debug.LogException(exception);
            EditorApplication.Exit(1);
        }

        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("PopAssetDependencies has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details."), WrapperlessIcall]
        public static extern void PopAssetDependencies();
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall, Obsolete("PushAssetDependencies has been made obsolete. Please use the new AssetBundle build system introduced in 5.0 and check BuildAssetBundles documentation for details.")]
        public static extern void PushAssetDependencies();
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern void SetPlaybackEngineDirectory(BuildTarget target, BuildOptions options, string playbackEngineDirectory);
        [Obsolete("WebPlayer has been removed in 5.4", true)]
        private static bool WebPlayerAssetBundlesAreNoLongerSupported()
        {
            throw new InvalidOperationException("WebPlayer asset bundles can no longer be built in 5.4+");
        }

        public static bool isBuildingPlayer { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }
    }
}

