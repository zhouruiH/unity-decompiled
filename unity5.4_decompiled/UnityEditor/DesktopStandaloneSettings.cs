﻿using System;
using UnityEditor;

internal static class DesktopStandaloneSettings
{
    private static readonly string kSettingCopyPDBFiles = "CopyPDBFiles";

    internal static bool CopyPDBFiles
    {
        get
        {
            return (EditorUserBuildSettings.GetPlatformSettings(PlatformName, kSettingCopyPDBFiles).ToLower() == "true");
        }
        set
        {
            EditorUserBuildSettings.SetPlatformSettings(PlatformName, kSettingCopyPDBFiles, value.ToString().ToLower());
        }
    }

    internal static string PlatformName
    {
        get
        {
            return "Standalone";
        }
    }
}

